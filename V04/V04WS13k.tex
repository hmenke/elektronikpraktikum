\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V04: Signalausbreitung auf Leitungen}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Torsten Rendler}\noaffiliation
\date{11.\ November 2013}

\begin{abstract}
	%Im Versuch werden die Signalausbreitungseigenschaften in Abhängigkeit der Leitung betrachtet.
	Die Signalaubreitungseigenschaften in einem Koxialkabel (BNC, Typ RG-58) werden betrechtet.
	Zunächst wird eine Impedanzanpassung durch manuelle Einstellung des Abschlusswiderstandes durchgeführt.
	Danach wird die Dämpfung durch ein Kabel in Abhängigkeit der Frequenz des induzierten Signals betrachtet.
	Im darauf folgenden Teil wird die Ausbreitungsgeschwindigkeit in Abhängigkeit der Frequenz (Dispersion) untersucht.
	Dann werden eine Pulsformanalyse und eine Spektralanalyse mittels FFT angewandt.
	Zuletzt wird die Pulsausbreitung im Kabel und der Einfluss des Abschlusswiderstandes auf die Pulsform untersucht.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Leitungstheorie homogener Leitungen}

Eine reale Spannungsquelle ist eine Spannungsquelle, bei der ihr eigener Innenwiderstand $R_i$ berücksichtigt wird. Diese liefert die elektrische Leistung $P_\mathrm{el}$, welche vom Strom $I$ zum Verbraucher in Form eines Lastwiderstandes $R_L$ transportiert wird. Eine entsprechende Skizze ist in Abbildung~\ref{fig:reale Spannungsquelle} abgebildet.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw[dotted] (-1,-0.5) rectangle (3.8,3);
		\path (-1,-0.5) -- node[below] {reale Spannungsquelle} (3.8,-0.5);
		\draw (0,0) to[I=$I$]
		(0,2) to[R=$R_i$,-o]
		(4,2) to[short,i=$I$]
		(5,2) to[R=$R_L$]
		(5,0) to[short,-o]
		(4,0) to[short]
		(0,0);
		\draw[->] (1,1.8) -- node[right] {$U_0 = \mathrm{const}$} (1,0.2);
		\draw[->] (4,1.8) -- node[right] {$U_\mathrm{kl}$} (4,0.2);
	\end{tikzpicture}
	\caption{Schaltbild einer realen Spannungsquelle. Im gepunkteten Kasten ist die Spannungsquelle abgebildet, außerhalb ein Lastwiderstand.}
	\label{fig:reale Spannungsquelle}
\end{figure}

%Nun stellt sich die Frage, wann von der realen Spannungsquelle die meiste Leistung an den Verbraucher abgegeben wird. Dies lässt sich berechnen durch \cite[S.~9]{EP:Skript}
Die abgegebene Leistung entspricht einer realen Spannungsquelle und ist gegeben durch
%
\begin{align*}
	P_L = U_\mathrm{kl} \cdot I = \frac{U_0^2}{R_L} \frac{1}{\left( 1 + \frac{R_i}{R_L} \right)^2}
\end{align*}
%
Wie man leicht sieht, besitzt $P_L$ ein Maximum für $R_i = R_L$ mit dem Wert $P_L = 1/4 \cdot U_0^2/R_L$. In diesem Fall spricht man von einer \emph{Leistungsanpassung}.

Zur Übertragung von Signalen wird meist ein \emph{Koaxialkabel} verwendet. Ein solches Kabel schirmt den Leiter von der Umgebung ab und umgekehrt. Ein Koaxialkabel besteht aus einem Außenleiter und einem Innenleiter, die durch ein Dielektrikum voneinander getrennt sind. Das Kabel ist zudem mit einem Schutzmantel umgeben. % um Beschädigungen zu verhindern und Verletzungen zu vermeiden.

Ein Koaxialkabel kann aus infinitesimalen Bauelementen der Länge $\diff x$ aufgebaut betrachtet werden. Ein Ersatzschaltbild eines solchen Bauteils ist in Abbildung~\ref{fig:Koaxkabel} zu sehen.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw[dotted]
		(-1,2) -- (0,2)
		(-1,0) -- (0,0)
		(6,2) -- (7,2)
		(6,0) -- (7,0);
		\draw (4,2) to[C,l_=$C$] (4,0);
		\draw (5,2) to[R=$G$] (5,0);
		\draw (0,2) to[L=$L$,i>^=$I$,o-]
		(2,2) to[R=$R$,-*]
		(4,2) to[short,-*]
		(5,2) to[short,-o]
		(6,2);
		\draw (0,0) to[short,o-*]
		(4,0) to[short,-*]
		(5,0) to[short,-o]
		(6,0);
		\draw[->] (0,1.8) -- node[right] {$U$} (0,0.2);
		\draw[<->] (0,-0.5) -- node[below] {$\diff x$} (6,-0.5);
	\end{tikzpicture}
	\caption{Ersatzschaltbild eines Leitungselements eines Koaxialkabels mit ohmschem Widerstand $R$, Induktivität $L$, Kapazität $C$ und Leckwiderstand $G$.}
	\label{fig:Koaxkabel}
\end{figure}

Um die Signalausbreitung mathematisch beschreiben zu können betrachten wir dieses als Funktion des Ortes $x$. Dies führt uns auf die sogenannten Beläge
%
\begin{align*}
	\diff L &= L' \diff x && \text{Induktivität von $\diff x$} \\
	\diff C &= C' \diff x && \text{Kapazität von $\diff x$} \\
	\diff R &= R' \diff x && \text{Gleichstromwiderstand von $\diff x$} \\
	\diff G &= G' \diff x && \text{Isolationsleitwert von $\diff x$}
\end{align*}
%
Die gestrichenen Größen heißen Induktivitäts-, Kapazitäts-, Widerstands- und Leitwertsbelag. Sie geben die entsprechende Eigenschaft pro Meter Leitungslänge an.
Die Leckwiderstände der Isolierung werden im Leitwertsbelag $G'$ zusammengefasst um die Beschreibung zu vereinfachen.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (2,0) to[C] (2,2);
		\draw (4,0) to[C] (4,2);
		\draw (7.5,0) to[C] (7.5,2);
		\draw (0,0) to[L,o-*]
		(2,0) to[L,-*]
		(4,0) to[short]
		(4.5,0)
		(5.5,0) to[L,-*]
		(7.5,0) to[short,-o]
		(8,0);
		\draw (0,2) to[L,o-*]
		(2,2) to[L,-*]
		(4,2) to[short]
		(4.5,2)
		(5.5,2) to[L,-*]
		(7.5,2) to[short,-o]
		(8,2);
		\draw[dotted] (4.5,0) -- (5.5,0);
		\draw[dotted] (4.5,2) -- (5.5,2);
	\end{tikzpicture}
	\caption{Ersatzschaltbild eines gesamten Koaxialkabels (ohne Leckwiderstände).}
\end{figure}

\subsubsection{Wellenwiderstand}

Wendet man nun die Kirchhoffschen Gesetze an, so erhält man ein Differentialgleichungssystem für $U$ und $I$ welches mit einem Ansatz der Form einer ebenen Welle gelöst werden kann.
%
\begin{equation}
	\begin{aligned}
		\frac{\diff U}{\diff x} &= - (R' \cdot I + L' \cdot \dot{I}) \\
		\frac{\diff I}{\diff x} &= - (G' \cdot U + C' \cdot \dot{U})
	\end{aligned}
	\label{eq:leitungsgleichungen}
\end{equation}
%
Seien nun $R' = G' = 0$, dann genügt zur Lösung der Wellenansatz
%
\begin{align*}
	U(x,t) = U_0 \, \mathrm{e}^{-\mathrm{j}(k x - \omega t)} \\
	I(x,t) = I_0 \, \mathrm{e}^{-\mathrm{j}(k x - \omega t)}
\end{align*}
%
Setzt man diesen Ansatz ein, so erhält man die Dispersionsrelation
%
\begin{align*}
	k = \pm \omega \sqrt{L' C'}
\end{align*}
%
sowie den Wellenwiderstand der Leitung
%
\begin{align*}
	Z = \frac{U_0}{I_0} = \sqrt{\frac{L'}{C'}}
\end{align*}

\subsubsection{Phasen- und Gruppengeschwindigkeiten}

Die Phasengeschwindigkeit ist die Ausbreitungsgeschwindigkeit eines Wellenpakets und ist gegeben durch die Dispersionsrelation als
%
\begin{align*}
	c_\mathrm{Ph} = \frac{\omega}{k} = \frac{1}{\sqrt{L'C'}}
\end{align*}

Die Gruppengeschwindigkeit gibt die Ausbreitungsgeschwindigkeit der Einhüllenden des Wellenpakets an und wird berechnet durch
%
\begin{align*}
	c_\mathrm{Gr} = \frac{\partial \omega}{\partial k} = \frac{1}{\sqrt{L'C'}}
\end{align*}

%Man sieht, dass hier $c_\mathrm{Ph} = c_\mathrm{Gr}$ ist --- die Welle zeigt also keine Dispersion.

\subsubsection{Dämpfung}

%Mit Hilfe der \emph{Dezibel-Skala} ($\SI{1}{\deci\bel} = \SI{0.1}{\bel}$) können Verhältnisse beschrieben werden, bei denen unterschiedliche Größenordnungen im Spiel sind.
Um Verhätlnisse mit der \emph{Dezibel-Skala} zu beschreiben bildet man den Logarithmus zur Basis zehn des Verhältnisses und multipliziert dies mit zehn.
\SI{10}{\deci\bel} entspricht also einer Verzehnfachung, \SI{3}{\deci\bel} ungefähr einer Verdoppelung, \SI{-3}{\deci\bel} einer Halbierung, usw.

Die elektrische Leistung ist proportional zum Quadrat der Spannung, was in der Dezibel-Skala einen Faktor $2$ zur Folge hat. Folglich entsprechen \SI{6}{\deci\bel} einer Verdopplung. In Formeln drückt sich dies aus als
%
\begin{align*}
	L(U_1,U_2)
	&= 10 \log_{10}\left( \frac{U_1^2}{U_2^2} \right) \si{\deci\bel} \\
	&= 20 \log_{10}\left( \frac{U_1}{U_2} \right) \si{\deci\bel} \\
	&= 2 \log_{10}\left( \frac{U_1}{U_2} \right) \si{\bel}
\end{align*}
%
Da die Dämpfung über die gesamte Länge der Leitung stattfindet wird sie auch in Relation zu dieser angegeben. Meist in $\si{\deci\bel}/\SI{10}{m}$ oder $\si{\deci\bel}/\SI{100}{m}$.

\subsubsection{Reflexion}

Die Lösungen der Differentialgleichungen~\eqref{eq:leitungsgleichungen} sind ebene Wellen. Dies legt nahe, dass sich Spannungs- und Stromwellen wie andere Wellen verhalten, also auch reflektiert werden können, z.\,B.\ am Leitungsende. Dazu gibt es den \emph{Reflexionsfaktor}
%
\begin{align*}
	p = \frac{U^{(r)}}{U^{(e)}} = \frac{R_L - Z}{R_L + Z}
\end{align*}
%
Die Indizes ${}^{(r)}$ und ${}^{(e)}$ stehen für reflektiert bzw.\ einfallend. Der Reflexionsfaktor ist $p=0$, wenn $R_L = Z$. Aus diesem Grund werden offene Leitungen mit einem Abschlusswiderstand versehen.

\subsection{Fouriertransformation}

Ist $f(t) \in L^1(\mathbb{R}^n)$, dann gilt für die zeitkontinuierliche Fouriertransformierte
%
\begin{align*}
	\mathcal{F}[f](\omega) = \frac{1}{\sqrt{(2 \pi)^n}} \int_{\mathbb{R}^n} f(t) \, \mathrm{e}^{-\mathrm{j} \omega t} \diff t
\end{align*}

Ist nun auch $\mathcal{F}[f](\omega) \in L^1(\mathbb{R}^n)$, so existiert die zeitkontinuierliche Fourierrücktransformierte
%
\begin{align*}
	f(t) = \mathcal{F}^{-1}\left[ \mathcal{F}[f] \right](t) = \frac{1}{\sqrt{(2 \pi)^n}} \int_{\mathbb{R}^n} \mathcal{F}[f](\omega) \, \mathrm{e}^{-\mathrm{j} \omega t} \diff \omega
\end{align*}

Bei Messreihen handelt es sich jedoch meist um zeitdiskrete Signale, die nicht so transformiert werden können. Deshalb bedient man sich der diskreten Fouriertransformation (DFT). Sei dazu $f_k = f(t_k) \in \mathbb{C}$ an den diskreten Zeitpunkten $t_k$ \cite[S.~39]{PhaC:Skript}.
%
\begin{align*}
	\mathrm{DFT}(f_k)_n &= g_n = \sum_{k=0}^{N-1} f_k \mathrm{e}^{-\mathrm{j} \frac{2\pi}{N} n k} \\
	\mathrm{iDFT}(g_n)_k &= f(t_k) = \sum_{n=0}^{N-1} \frac{g_n}{N} \mathrm{e}^{\mathrm{j} \frac{2\pi}{N} n k}
\end{align*}
%
Dieses Verfahren ist von der Ordnung $\mathcal{O}(N^2)$, was für viele Datenpunkte von Nachteil ist.

Deshalb bedient man sich meist der \emph{Fast Fourier Transform} (FFT) nach Cooley und Tukey, welche für eine Anzahl von Datenpunkten $N = 2 M$ das Verfahren auf die Ordnung $\mathcal{O}(N \log N)$ reduzieren kann.

\section{Versuchsaufbau und -durchführung}

In dem heutigen Versuch wird die Signalausbreitung auf Leitungen untersucht. Im ersten Versuchsteil führt man eine Impedanzanpassung durch. Anschließend wird die Dämpfung in Abhängigkeit der Frequenz auf einem \SI{10}{\m} Koaxialkabel gemessen. Im dritten Versuchsteil geht es den Zusammenhang zwischen Singnalgeschwindigkeit und Frequenz. Im darauf folgenden Teil wird mit Hilfe des Oszilloskops eine Pulsanalyse durchgeführt. Abschließend wird der Einfluss des Abschlusswiderstandes auf die Pulsform untersucht.
\\
\subsection{Impedanzanpassung}

Als Erstes wird der Ausgang des Frequenzgenerators über ein \SI{1}{\m}-Koaxialkabel mit der ersten Eingang des Oszilloskops verbunden. Der Sync-Ausgang wird, ebenfalls über ein 1 m-Koaxialkabel, mit dem Oszilloskop verbunden. Im Oszilloskop wird die Triggerquelle entsprechend eingestellt.

Auf dem Frequenzgenerator wird ein Sinussignal mit \SI{100}{\kilo \Hz} und $\SI{3}{\volt}_{\textrm{rms}}$ eingestellt.

Nun wird über ein BNC-T-Stück ein variabler Abschlusswiderstand angeschlossen. Der Widerstand wird so eingestellt, dass keine Reflexion auftritt. Anschließend wird der Widerstand mit einem Handmultimeter ausgelesen.

\subsection{Dämpfung}

Das \SI{10}{\m}-Koaxialkabel wird zwischen BNC-T-Stück und dem zweiten Eingang des Oszilloskops eingefügt, vor den Eingang kommt noch ein weiteres BNC-T-Stück an das der variable Widerstand angeschlossen wird. Nun wird für Frequenzen von \SI{100}{\kilo \Hz} und \SI{50}{\mega \Hz} die Spannung an Kanal 1 und 2 gemessen und notiert. Die Frequenz wird dabei in 1-2-5-Schritten hochgeregelt.

\subsection{Dispersion}

Um die Dispersion, also die Laufzeit zwischen Kanal 1 und Kanal 2 am Oszilloskop zu messen, wird ein \SI{500}{\kilo\Hz} Signal angelegt. Die Eingangsempfindlichkeit beider Eingänge wird auf \SI{10}{\milli\volt\per{Div}}, die Zeitauflösung auf \SI{10}{\nano\s\per{Div}} eingestellt. Mit Hilfe der Curserfunktion des Oszilloskops wird die Laufzeit ermittelt. Die Messung wird bei Frequenzen bis \SI{50}{\mega\Hz} durchgeführt.

\subsection{Pulsformanalyse}

Das \SI{10}{\m}-Koaxialkabel wird entfernt und die Leitung wird mit \SI{50}{\ohm}
abgeschlossen. Am Frequenzgenerator wird ein Pulssignal mit folgenden Eigenschaften generiert:
\begin{itemize}
	\item Pulsbreite: \SI{20}{\nano\s}
	\item Anstiegszeit: \SI{5}{\nano\s}
	\item Wiederholfrequenz: \SI{100}{\kilo\Hz}
	\item Low Level: \SI{0}{\volt}
	\item High Level: \SI{5}{\volt}
\end{itemize}

Als erstes wird die \SI{10}{\percent}-\SI{90}{\percent}-Signalanstiegszeit des Pulses mit der Curserfunktion des Oszilloskops gemessen. Nun wird der Puls auf dem Oszilloskop mittig und symmetrisch zur y-Achse positioniert und die interne \emph{Fast Fourier Transformation} aktiviert. Das erhaltene Spektrum, sowie der Pulsverlauf wird über LabVIEW gesichert. Es werden Pulsbreite und Anstiegszeit variiert und die veränderten Spektren ebenfalls abgespeichert.
Dann werden Pulsbreite und Anstiegszeit wieder auf die Anfangswerte eingestellt und die Bandbreite des Oszilloskops von \SI{100}{\mega\Hz} auf \SI{100}{\kilo \Hz} begrenzt. Das Zeitsignal und das dazugehörige Spektrum werden abgespeichert.

\subsection{Pulsausbreitung}
Nun wird das \SI{10}{\m}-Koaxialkabel vor dem Abschlusswiderstand angebracht. Das Signal wird nun bei offenem Kabelende (variabler Abschlussiwiderstand $R_a$ wird komplett hoch gestellt), bei Kurzschluss ($R_a = 0$) und bei angepasstem Abschusswiderstand gemessen und gespeichert.



\section{Formeln}

Die folgenden Zeichen und Einheiten wurden im Weiteren verwendet
%
\begin{itemize}
	\item $U$ in Volt (\si{\V}): Spannung
		\begin{itemize}
			\item Index $a$: Eingangsspannung
			\item Index $e$: Ausgangsspannung
			\item Index $i$: Einlaufende Spannung
			\item Index $r$: Reflektierte Spannung
		\end{itemize}
	\item $a$ in Dezibel (\si{\deci\bel}): Dämpfung des Kabels
	\item $A$ in Dezibel pro Meter (\si{\deci\bel\per\m}): relative Dämpfung des Kabels
	\item $c_s$ in Meter pro Sekunde (\si{\m\per\s}): Signalgeschwindigkeit
	\item $\tau$ in Sekunden (\si{\s}): Zeitdifferenz
		\begin{itemize}
			\item Index $a$: Signalanstiegszeit
			\item Index $b$: Pulsbreite
		\end{itemize}
	\item $\mathcal{B}$ in \si{\per\s}: Frequenzbandbreite des Oszilloskops
	\item $p$ ohne Einheit: Reflexionsfaktor
	\item $R$ in Ohm (\si{\ohm}): Ohmscher Widerstand
	\item $|Z|$ in Ohm (\si{\ohm}): Wellenwiderstand des Koaxialkalbels
\end{itemize}

\begin{description}
	\item[Dämpfung] Die absolute Kabeldämpfung $a$ und relative Kabeldämpfung $A$ lassen sich bestimmen mittels
		\begin{equation}
			a = 20 \log \frac{U_e}{U_a}\,\si{\deci\bel} \; , \quad A = \frac{a}{\ell} = \frac{a}{\SI{10}{\m}} \label{eq:Deampfung}
		\end{equation}
		Die $\ell = \SI{10}{\m}$ wurden eingesetzt, da das im Versuch verwendete Kabel gerade \SI{10}{\m} lang ist.

	\item[Dispersion] Wie aus der Mechanik bekannt, kann die Signalgeschwindigkeit berechnet werden mit
		\begin{equation}
			c_s = \frac{\Delta s}{\Delta t} = \frac{\ell}{\Delta t} = \frac{\SI{10}{\m}}{\Delta t} \label{eq:Dispersion}
		\end{equation}

	\item[Pulsformanalyse] Die wahre \SI{10}{\percent}-\SI{90}{\percent}-Signal"|an"|stiegs"|zeit $\hat{\tau}_a$ kann berechnet werden mit
		\begin{equation}
			\hat{\tau}_a = \sqrt{\tau_a^2 - \left( \frac{\num{0.35}}{\mathcal{B}} \right)^2} \label{eq:Anstiegszeit}
		\end{equation}

	\item[Pulsausbreitung] Der Reflexionsfaktor einer Welle im Koaxialkabel ist gegeben durch
		\begin{equation}
			p = \frac{U_r}{U_i} = \frac{R_L - Z}{R_L + Z} \label{eq:Reflexion}
		\end{equation}
		Durch Umformen ergibt sich für $R_L$
		\begin{equation}
			R_L = \frac{U_i + U_r}{U_i - U_r} Z = \frac{U_i + U_r}{U_i - U_r} \cdot \SI{50}{\ohm} \label{eq:Impendanz}
		\end{equation}
		Die \SI{50}{\ohm} wurden eingesetzt, da dies gerade die Impendanz des eingesetzten RG-58 Koaxialkabels ist.
\end{description}

\section{Messwerte}

%Die handschriftlich aufgenommenen Messwerte sind nochmals in den Tabellen~\ref{tab:daempfung} und~\ref{tab:dispersion} dargelegt.
Ein handschriftliches Messprotkoll befindet sich im Anhang.

%\begin{table}
%	\centering
%	\begin{tabular}{SSS}
%		\toprule
%		{Freq.\ $\nu$ $[\si{\Hz}]$} & {$U_e^{\textrm{eff}}$ $[\si{\volt}]$} & {$U_a^{\textrm{eff}}$ $[\si{\volt}]$} \\
%		\midrule
%		1e5 & 3.03 & 2.96 \\
%		2e5 & 3.01 & 2.94 \\
%		5e5 & 3.04 & 2.95 \\
%		1e6 & 3.06 & 2.92 \\
%		2e6 & 3.08 & 2.87 \\
%		5e6 & 3.10 & 2.82 \\
%		1e7 & 3.00 & 2.78 \\
%		2e7 & 2.96 & 2.60 \\
%		5e7 & 2.89 & 2.34 \\
%		\bottomrule
%	\end{tabular}
%	\caption{Messwerte zur Ermittlung der Kabeldämpfung.}
%	\label{tab:daempfung}
%\end{table}
%
%\begin{table}
%	\centering
%	\begin{tabular}{SSS}
%		\toprule
%		{Freq.\ $\nu$ $[\si{\Hz}]$} & {Zeitdiff.\ $\tau$ $[\si{\s}]$} \\
%		\midrule
%		5e5 & 58.80e-9 \\
%		1e6 & 56.40e-9 \\
%		2e6 & 54.40e-9 \\
%		5e6 & 51.60e-9 \\
%		1e7 & 51.20e-9 \\
%		2e7 & 50.80e-9 \\
%		5e7 & 50.20e-9 \\
%		\bottomrule
%	\end{tabular}
%	\caption{Gemessene Laufzeitdifferenzen zur Ermittlung der Dispersion.}
%	\label{tab:dispersion}
%\end{table}

\section{Auswertung}

\subsection{Impedanzanpassung}

\begin{description}
	\item[Ohne Leistungsanpassung] Zunächst werden am Frequenzgenerator \SI{3}{\volt_{RMS}} eingestellt, bei einer Lastimpedanz von \SI{50}{\ohm}. Die vom Generator loslaufende Welle trifft auf CH1 des Oszilloskops auf einen Widerstand von \SI{1}{\mega\ohm}, was einem offenen Kabelende entspricht. Die rücklaufende Welle interferiert also konstriktiv mit der einlaufenden und führt zu einem Effektivwert von \SI{6}{\volt} am Oszilloskop. Stellt man die Lastimpedanz am Frequenzgenerator auf \texttt{High-Z} ein, so erscheint der korrekte Effektivwert auf der Anzeige des Frequenzgenerators.
	
	\item[Mit Leistungsanpassung] Nun wird über ein BNC-T-Stück ein variabler Abschlusswiderstand (Potentiometer) angeschlossen. Durch Drehen des Reglers lässt sich ein Effektivwert von \SI{3.00}{\volt} erreichen. Zieht man das Potentiometer vom Aufbau ab und misst den Widerstand mit dem Handmultimeter, so ergibt sich dieser zu $R_L = \SI{48.3}{\ohm}$. Dieser Wert kommt nicht ganz an die Impedanz des RG-58-Kabels von $Z = \SI{50}{\ohm}$ heran.
\end{description}

Die Impedanzanpassung ist grafisch in Abbildung~\ref{fig:Impedanzanpassung} dargestellt.
\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Impedanzanpassung/}{Abschlusswiderstand.tex}
	\caption{Durch Reflexion am unangepassten Kabelende entsteht Interferenz und der Effektivwert der Spannung am Oszilloskop ist erhöht. Durch die \SI{48.3}{\ohm}-Leistungsanpassung wird der richtige Spannungseffektivwert erreicht.}
	\label{fig:Impedanzanpassung}
\end{figure}

\subsection{Dämpfung}
Es wird die Dämpfung auf einem \SI{10}{\m}-Koaxialkabel untersucht. Das Kabel befindet sich zwischen Kanal 1 und Kanal 2. Die absolute Kabeldämpfung $a$ lässt sich mit \eqref{eq:Deampfung} aus den Spannungen am Kabelanfang und Kabelende berechnen.
%\[ a = 20 \log \left( \frac{U_e}{U_a}\right) \, \si{\deci\bel} \]
Um die relative Kabeldämpfung $A$ zu erhalten, teilt man die absolute Dämpfung durch die Kabellänge $\ell$.
\[A = \frac{a}{\ell} =\frac{a}{\SI{10}{\m}}\]
Die sich so ergebende absolute Dämpfung ist in Abbildung~\ref{fig:Daempfung} dargestellt.

%\begin{table}
%	\centering
%	\begin{tabular}{SSSSS}
%		\toprule
%		{Freq.\ $\nu$ $[\si{\Hz}]$} & {$U_e^{\textrm{eff}}$ $[\si{\volt}]$} & {$U_a^{\textrm{eff}}$ $[\si{\volt}]$} & {$a$ $[\si{\dB}]$} & {$A$ $[\si{\dB\per{100}\m}]$} \\
%		\midrule
%		1e5 & 3.03 & 2.96 & 0.203 & 0.0203 \\
%		2e5 & 3.01 & 2.94 & 0.204 & 0.0204 \\
%		5e5 & 3.04 & 2.95 & 0.261 & 0.0261 \\
%		1e6 & 3.06 & 2.92 & 0.407 & 0.0407 \\
%		2e6 & 3.08 & 2.87 & 0.613 & 0.0613 \\
%		5e6 & 3.10 & 2.82 & 0.822 & 0.0822 \\
%		1e7 & 3.00 & 2.78 & 0.662 & 0.0662 \\
%		2e7 & 2.96 & 2.60 & 1.126 & 0.1126 \\
%		5e7 & 2.89 & 2.34 & 1.834 & 0.1834 \\
%		\bottomrule
%	\end{tabular}
%	\caption{Relative und absolute Dämpfung des Koaxialkabels RG-58.}
%	\label{tab:daempfung2}
%\end{table}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Daempfung/}{Daempfung.tex}
	\caption{Doppellogarithmischer Polt der relativen Dämpfung $A$ gegen die Frequenz $\nu$.}
	\label{fig:Daempfung}
\end{figure}

\subsection{Dispersion}
Um die frequenzabhängige Dispersion, die auf dem \SI{10}{\m}-Koaxialkabel auftritt, zu messen, werden die Spannungsverläufe vor und nach dem Kabel auf dem Oszilloskop übereinandergelegt. Bei einer hohen Eingangsempfindlichkeit und Zeitauflösung kann dann die Laufzeitdifferenz mit Hilfe der Curserfunktion ermittelt werden. Die Frequenz wird dabei in 1-2-5-Schritten von $\SI{50}{\kilo \Hz}$ bis $\SI{50}{\mega \Hz}$ hochgeregelt. Nach folgender Formel kann dann die Signaldifferenz $c_s$ berechnet werden:

\[ c_s = \frac{\Delta s}{\Delta t} = \frac{\ell}{\Delta t} = \frac{\SI{10}{\m}}{\Delta t} \]

Die Ergebnisse sind in Abbildung~\ref{fig:Dispersion} zu sehen.

%\begin{table}
%	\centering
%	\begin{tabular}{SSS}
%		\toprule
%		{Freq.\ $\nu$ $[\si{\Hz}]$} & {Zeitdiff.\ $\tau$ $[\si{\nano \s}]$} & {Signaldiff.\ $c_s$ $[\SI{e8}{\m\per\s}]$} \\
%		\midrule
%		5e5 & 58.80 & 1.70 \\
%		1e6 & 56.40 & 1.77 \\
%		2e6 & 54.40 & 1.84 \\
%		5e6 & 51.60 & 1.94 \\
%		1e7 & 51.20 & 1.95 \\
%		2e7 & 50.80 & 1.97 \\
%		5e7 & 50.20 & 1.99 \\
%		\bottomrule
%	\end{tabular}
%	\caption{Gemessene Laufzeitdifferenzen zur Ermittlung der Dispersion.}
%	\label{tab:dispersion2}
%\end{table}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Dispersion/}{Dispersion.tex}
	\caption{Halblogarithmischer Plot der Signalausbreitung $c_s$ gegen die Frequenz $\nu$.}
	\label{fig:Dispersion}
\end{figure}

In dem halblogarithmischen Plot steigt die Dispersion linear an, somit gilt für die Abhängigkeit von der Frequenz \[ c_s \propto \log\left(\frac{\nu}{\nu_0}\right)\]. Bei einer Frequenz von ca. \SI{4}{\mega \Hz} hat die Kurve einen leichten Knick und die Dispersion steigt für größere Frequenzen weniger stark an.

\subsection{Pulsformanalyse}
In ABB.~\ref{fig:Signalanstieg} ist das am Oszilloskop gemessene Pulssignal zu sehen. Mit Hilfe der Curserfunktion wurde die \SI{10}{\percent}-\SI{90}{\percent}-Anstiegszeit ermittelt zu $\tau_a = \SI{5.8}{\nano \s}$.  Da die Bandbreite $\mathcal{B}$ des Oszilloskops beachtet werden muss, ergibt sich für die wahre Anstiegszeit:
\[ \hat{\tau}_a = \sqrt{\tau_a^2 - \left( \frac{\num{0.35}}{\mathcal{B}} \right)^2} = \sqrt{( \SI{5.8}{\nano \s} )^2 - \left( \frac{\num{0.35}}{\SI{100}{\mega \Hz}} \right)^2} = \SI{4.6}{\nano \s}\]

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Pulsform/}{Signalanstieg.tex}
	\caption{Das am Oszilloskop gemessene Pulssignal, mit sichtbaren Cursorlinien zur Bestimmung der \SI{10}{\percent}-\SI{90}{\percent}-Anstiegszeit.}
	\label{fig:Signalanstieg}
\end{figure}

Ein Rechtecksignal besteht aus einer Überlagerung vieler Sinussignale. Je eckiger der Puls, desto mehr Sinussignale mit hohen Frequenzen sind Teil der Überlagerung. Die Fouriertransformation zeigt die Gewichtung der verschiedenen Frequenzen der Schwingungen. Ein Puls mit niedrigerer Anstiegszeit hat also einen größeren Anteil hochfrequenter Schwingungen. Die erste Nullstelle einer Fouriertransformation ist genau die Frequenz der Schwingung, deren Periodenlänge mit der Breite des Pulses übereinstimmt. Eine solche Schwingung und Schwingungen mit einem Vielfachen dieser Frequenz können nichts zu dem Puls beitragen und kommen somit nicht vor.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Pulsform/}{Pulsbreite.tex}
	\caption{FFT verschiedener Pulse mit variablen Breiten und Anstiegszeiten. Die Breite $\tau_b$ und die Ansteigszeit $\tau_a$ wurden der Übersichtlichkeit halber in Paaren $\langle \tau_a | \tau_b \rangle$ angegeben. Die Kurven wurden farblich nach Pulsbreite gruppiert.}
	\label{fig:Pulsbreite}
\end{figure}

Hier sind die erwähnten Eigenschaften gut zu erkennen. Je breiter der Puls, desto später kommt die erste Nullstelle der Fouriertransformation. Außerdem ist erkennbar, das bei kürzeren Anstiegszeiten ein größerer Anteil an hochfrequenten Schwingungen miteinfließt. Dieser Aspekt ist auch in dem folgenden Plot noch einmal deutlich zu sehen.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Pulsform/}{Anstiegszeit.tex}
	\caption{FFT des Signals eines Pulses mit einer Breite von \SI{50}{\ns} und variabler Anstiegszeit.}
	\label{fig:Anstiegszeit}
\end{figure}

Verringert man am Oszilloskop die Bandbreite von \SI{100}{\mega \Hz} auf \SI{20}{\mega \Hz}, so gibt es weniger hochfrequente Schwingungen, der Puls läuft also auseinander. Zu sehen ist dieser Effekt in der Abbildung~\ref{fig:Bandbreitenbeschraenkung}.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Pulsform/}{Bandbreitenbeschraenkung.tex}
	\caption{Der Plot zeigt anschaulich, wie der Spannungspuls bei einer geringeren Bandbreite auseinander fließt.}
	\label{fig:Bandbreitenbeschraenkung}
\end{figure}


\subsection{Pulsausbreitung}
In diesem Versuchsteil wird die Reflexion von Pulsen untersucht. Der Puls wird dabei am Ende des \SI{10}{\m}-Koaxialkabel reflektiert. Einmal bei offenem Kabelende, bei kurzgeschlossenem Kabelende und mit angepasstem Abschlusswiderstand. In ABB.~\ref{fig:Abschlusswiderstand} sind die 
Signalverläufe zu sehen.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Pulsausbreitung/}{Abschlusswiderstand.tex}
	\caption{Am Oszilloskop gemessenes Pulssignal und Pulsecho für ein offenes und kurzgeschlossenes, sowie leistungsangepasstem Kabelende.}
	\label{fig:Abschlusswiderstand}
\end{figure}

Wie man sieht, kommt es durch kurzschließen des Kabelendes zu einer Reflexion am festen Ende. Ist das Kabelende dagegen offen, so klappt der Puls um. Befindet sich am Kabelende dagegen ein angepasster Widerstand, so tritt quasi keine Reflexion auf. Der Widerstand wird mit dem Tischmultimeter ausgelesen zu $ R = \SI{49.8}{\ohm}$.
Da die Impedanz frequenzabhängig ist und der Puls aus vielen verschiedenen Frequenzen besteht, lässt sich die Reflexion nicht komplett verhindern.

Nun wird das Ende des \SI{10}{\m}-Kabels auf Kanal 2 des Oszilloskops gesteckt. Das Oszilloskop hat einen \glqq unendlich\grqq\ hohen Eingangswiderstand, es kommt also zur Reflexion am festen Ende.


Um die Impedanz des Generators zu erhöhen wird nun ein unbekannter Widerstand auf dessen Ausgangsbuchse gesteckt. Damit kommt es zu einer Mehrfachreflexion im Kabel. Durch positive Interferenz kommt es an Kanal 2 zu einer erhöhten Amplitude.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/Pulsausbreitung/}{Reflexionsverhalten.tex}
	\caption{Signalverlauf am Oszilloskop mit unbekanntem Vorwiderstand.}
	\label{fig:Reflexionsverhalten}
\end{figure}

Um den unbekannten Widerstand $R_u$ zu bestimmen wird zuerst der 
Reflexionsfaktor $p$ nach \eqref{eq:Reflexion} berechnet.
%\[ p = \frac{U_r}{U_i} = \frac{R_L - Z}{R_L + Z} \]
Für $R_L$ ergibt sich somit
\[ R_L = \frac{U_i + U_r}{U_i - U_r} Z = \frac{U_i + U_r}{U_i - U_r} \cdot \SI{50}{\ohm} \]
Die so errechneten Werte sind in Tabelle~\ref{tab:reflexionsfaktoren} aufgeführt.

\begin{table}
	\centering
	\begin{tabular}{SSSS}
		\toprule
		{$U_i$ $[\si{\volt}]$} & {$U_r$ $[\si{\volt}]$} & {$p$ $[1]$} & {$R_L$ $[\si{\ohm}]$} \\
		\midrule
		3.20 & 1.88 & 0.59 & 192.42 \\
		1.88 & 1.12 & 0.60 & 197.37 \\
		1.12 & 0.64 & 0.57 & 183.33 \\
		0.64 & 0.36 & 0.56 & 178.57 \\
		\midrule
		\multicolumn{2}{r}{Mittelwerte} & 0.58 & 187.92 \\
		\bottomrule
	\end{tabular}
	\caption{Reflexionsfaktoren und Abschlusswiderstände aus der Amplitude der Echopulse berechnet.}
	\label{tab:reflexionsfaktoren}
\end{table}

Der Eingangswiderstand des Funktionsgenerators beträgt \SI{50}{\ohm}, dieser ist mit dem unbekannten Widerstand in Reihe geschaltet und muss somit noch von $R_L$ abgezogen werden. Wir führen dies für den Mittelwert durch
\[ R_u = \langle R_L \rangle - \SI{50}{\ohm} = \SI{137.92}{\ohm} \]

\section{Fehlerbetrachtung}
\subsection{Leistungsanpassung}
Als Fehler wird die relative Abweichung des angepassten Abschlusswiderstandes $R_L$ gegenüber dem theoretisch zu erreichenden Wert der Kabelimpedanz $Z$ berechnet.
\[ Q[R_L,Z] = \frac{R_L - z}{Z} = \frac{\SI{48.3}{\ohm} - \SI{50}{\ohm}}{\SI{50}{\ohm}} = \SI{3.4}{\percent} \]
Mögliche Fehlerquellen sind:
%
\begin{itemize}
	\item Die (unbekannte) Ungenauigkeit der Berechnung des Effektivwertes durch das Oszilloskop.
	\item Die ungenaue Einstellmöglichkeit des Abschlusswiderstandes.
\end{itemize}

\subsection{Pulsformanalyse}
Die am Frequenzgenerator eingestellte Anstiegszeit beträgt $\tau_a^* = \SI{5}{\nano\s}$. Am Oszilloskop wurde mit der Cursorfunktion $\tau_a = \SI{5.8}{\nano\s}$ abgelesen und eine wahre Anstiegszeit von $\hat{\tau}_a = \SI{4.6}{\nano\s}$ berechnet.
Die relative Abweichung beträgt
\[ Q[\hat{\tau}_a,\tau_a^*] = \frac{\hat{\tau}_a - \tau_a^*}{\tau_a^*} = \frac{\SI{4.6}{\nano\s} - \SI{5}{\nano\s}}{\SI{5}{\nano\s}} = \SI{8.0}{\percent} \]

\subsection{Pulsausbreitung}
Aus der Höhe der reflektierten Pulse relativ zueinander wurde der Abschlusswiderstand bestimmt. Es wird die Abweichung vom Mittelwert (Standardabweichung) bestimmt.
\[ \sigma[R_L,\langle R_L \rangle] = \sqrt{\frac{1}{4 - 1} \sum_{i = 1}^{4} (R_{L,i} - \langle R_L \rangle)^2} = \SI{7.38}{\ohm} \]
Für den Mittelwert $\langle R_L \rangle = \SI{187.92}{\ohm}$ bedeutet dies einen Fehler von ca.\ \SI{8}{\ohm}. Der relative Fehler beträgt also
\[ Q[\langle R_L \rangle, \langle R_L \rangle \pm \SI{8}{\ohm}] = \frac{\langle R_L \rangle - \langle R_L \rangle \pm \SI{8}{\ohm}}{\langle R_L \rangle \pm \SI{8}{\ohm}} = \SI{4.1}{\percent} \]


\section{Zusammenfassung}

In diesem Versuch wurden Signalübertragungseigenschaften, wie z.\,B.\ Dispersion und Dämpfung untersucht. Außerdem wurde eine Pulsformanalyse mit Hilfe von Fouriertransformationen durchgeführt und der Einfluss des Kabelabschlusses auf die gemessene Pulsform untersucht.

Zu Beginn des Versuchs wurde über einen variablen Abschusswiderstand eine Impedanzanpassung durchgeführt. Es ergab sich ein Wert von $R_L = \SI{48.3}{\ohm}$, dies entspricht nicht ganz der Impedanz des Koaxialkabels von $Z = \SI{50}{\ohm}$.
 Anschließend wurde die Dämpfung auf dem \SI{10}{\m}-Koaxialkabel untersucht. Durch einen doppellogarithmischen Plot der Dämpfung $A$ über die Frequenz $\nu$ konnte folgende Abhängigkeit erkannt werden:
 \[ A \propto \nu^k \; , \quad k \in \mathbb{N} \; , \quad k>1 \]
Auch die Dispersion ist frequenzabhängig, hier wurde folgende Abhängigkeit bestimmt:
\[ c_s \propto \log \left( \frac{\nu}{\nu_0} \right) \]
Dies führt dazu, dass der Puls beim Übertragen auseinanderfließen.

Im nächsten Versuchsteil wurde der Einfluss von den Eigenschaften eines Pulses, Anstiegszeit und Pulsbreite auf die Fouriertransformation des Signals untersucht. Je geringer die Anstiegszeit, desto größer der Anteil an hochfrequenten Schwingungen. Schwingungen deren Wellenlänge mit der Breite des Pulses übereinstimmen, sind nicht Teil der Überlagerung. Ebenso Schwingungen mit einem Vielfachen dieser Wellenlänge.

Als Letztes wurde die Reflexion von Pulsen am Kabelende untersucht. Am offenen Kabelende findet eine Reflexion ohne Phasensprung statt, am kurzgeschlossenen Ende gibt es eine Phasenverschiebung von \SI{180}{\degree}, der Puls klappt also um. Mit Hilfe eines angepassten Abschlusswiderstandes kann die Reflexion zum Großteil verhindert werden. Da die Impedanz jedoch frequenzabhängig ist, wird trotzdem ein kleiner Teil reflektiert.

Schlussendlich wurde noch die neue Quellimpedanz, die durch einen unbekannter Widerstand, welcher an den Frequenzgenerator angebracht wurde, entstanden ist, bestimmt:
\[ R_L = \SI{187.92}{\ohm} \pm \SI{8}{\ohm} \]

\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
