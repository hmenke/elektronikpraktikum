#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Dispersion.tex'

set xlabel 'Frequenz $\nu$ $[\si{\mega\Hz}]$'
set ylabel 'Signalgeschw.\ $c_s$ $[\SI{e8}{\m\per\s}]$'
set format '\num[detect-all]{%g}'
set format x '\num[detect-all]{e%L}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale x
set ytics 0.1

d=10

plot \
'Dispersion.txt' using ($1/1e6):(d/($2*1e8)):(d/($2**2 * 1e8)*$3) with yerrorbars pointtype 5 linecolor 3 title '$c_s(\nu)$'
