#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Abschlusswiderstand.tex'

set xlabel 'Zeit $t$ $[\si{\micro\s}]$'
set ylabel 'Spannung $U$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables
set ytics 2

set xrange [0.4:0.8]

plot \
'kurzschluss.txt' using ($1*1e6):4 with lines linetype 1 linecolor 1 title 'Kurzschluss' , \
'offenesKabelende.txt' using ($1*1e6):4 with lines linetype 1 linecolor 2 title 'Offenes Kabelende' , \
'keineReflexion.txt' using ($1*1e6):4 with lines linetype 1 linecolor 3 title 'Keine Reflexion'
