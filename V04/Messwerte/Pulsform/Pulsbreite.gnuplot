#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,3
set pointsize 0.5
set output 'Pulsbreite.tex'

set xlabel 'Frequenz $\nu$ $[\si{\mega\Hz}]$'
set ylabel 'Amplitude $\Gamma$ $[\mathrm{a.\,u.}]$'
set format '\num[detect-all]{%g}'

set key below width -5
set grid linetype 3 linecolor 0
set fit errorvariables
set ytics ( \
	'\num[detect-all]{0}' -60+0, \
	'\num[detect-all]{0}' -60+50, \
	'\num[detect-all]{0}' -60+100 \
)
set yrange [-65:90]

set xrange [0:100]

plot \
'FFT_w=20,e=5.txt' using ($1/1e6):4 with lines linetype 1 linecolor 1 title '$\langle \SI{5}{\nano\s} | \SI{20}{\nano\s} \rangle$' , \
'FFT_w=20,e=10.txt' using ($1/1e6):4 with lines linetype 2 linecolor 1 title '$\langle \SI{10}{\nano\s} | \SI{20}{\nano\s} \rangle$' , \
'FFT_w=50,e=5.txt' using ($1/1e6):($4+50) with lines linetype 1 linecolor 2 title '$\langle \SI{5}{\nano\s} | \SI{50}{\nano\s} \rangle$' , \
'FFT_w=50,e=10.txt' using ($1/1e6):($4+50) with lines linetype 2 linecolor 2 title '$\langle \SI{10}{\nano\s} | \SI{50}{\nano\s} \rangle$' , \
'FFT_w=50,e=20.txt' using ($1/1e6):($4+50) with lines linetype 3 linecolor 2 title '$\langle \SI{20}{\nano\s} | \SI{50}{\nano\s} \rangle$' , \
'FFT_w=100,e=5.txt' using ($1/1e6):($4+100) with lines linetype 1 linecolor 3 title '$\langle \SI{5}{\nano\s} | \SI{100}{\nano\s} \rangle$'
