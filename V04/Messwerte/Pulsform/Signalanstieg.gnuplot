#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Signalanstieg.tex'

set xlabel 'Zeit $t$ $[\si{\nano\s}]$'
set ylabel 'Spannung $U$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables

set arrow nohead from first 6.0,graph 0 to first 6.0,graph 1
set arrow nohead from first 11.4,graph 0 to first 11.4,graph 1

plot \
'Signalanstieg.txt' using ($1/1e-9):4 with lines linetype 1 linecolor 3 title '$U[\textrm{CH1}](t)$'
