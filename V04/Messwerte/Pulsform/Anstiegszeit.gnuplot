#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Anstiegszeit.tex'

set xlabel 'Frequenz $\nu$ $[\si{\mega\Hz}]$'
set ylabel 'Amplitude $\Gamma$ $[\mathrm{a.\,u.}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables

set xrange [0:100]

plot \
'FFT_w=50,e=20.txt' using ($1/1e6):4 with lines linetype 1 linecolor 1 title '\SI{20}{\nano\s}', \
'FFT_w=50,e=10.txt' using ($1/1e6):4 with lines linetype 1 linecolor 2 title '\SI{10}{\nano\s}', \
'FFT_w=50,e=5.txt'  using ($1/1e6):4 with lines linetype 1 linecolor 3 title '\SI{5}{\nano\s}'
