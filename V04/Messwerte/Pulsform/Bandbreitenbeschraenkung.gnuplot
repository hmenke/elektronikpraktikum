#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Bandbreitenbeschraenkung.tex'

set xlabel 'Zeit $t$ $[\si{\nano\s}]$'
set ylabel 'Spannung $U$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 width 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables

plot \
'BW100.txt' using ($1/1e-9):4 with lines linetype 1 linecolor 1 title '$U[\textrm{CH1}](t)$ bei \SI{100}{\mega\Hz}', \
'BW20.txt'  using ($1/1e-9):4 with lines linetype 1 linecolor 3 title '$U[\textrm{CH1}](t)$ bei \SI{20}{\mega\Hz}'
