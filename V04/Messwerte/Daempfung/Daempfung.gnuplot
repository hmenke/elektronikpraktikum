#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Daempfung.tex'

set xlabel 'Frequenz $\nu$ $[\si{\mega\Hz}]$'
set ylabel 'Dämpfung $A$ $[\si{\deci\bel\per\metre}]$'
set format '\num[detect-all]{e%L}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale xy

plot \
'Daempfung.txt' using ($1/1e6):(20*log($2/$3)/10) with points pointtype 5 linecolor 3 title '$A(\nu)$'
