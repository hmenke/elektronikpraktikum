\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}
\newcommand\EKG{\textls{EKG}}

\title{Projekt: Elektrokardiogram (\EKG)}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

%\collaboration{Betreuer: Boris Bonev}\noaffiliation
\date{27.\ Januar 2014}

\begin{abstract}
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Herzzyklus}

Das \EKG\ ist ein medizinisches Diagnoseverfahren, welches die elektrischen Aktivitäten der Herzmuskelzellen (Myokard) sichtbar macht.

Die Erregungsbildung beginnt im Sinusknoten, der sich im rechten Atrium befindet. Von dort aus depolarisiert der Reiz die Myokardzellen der beiden Atria (P-Welle), was zur Myokard-Kontraktion führt und das Blut in die beiden Ventrikel pumpt. Der Reiz erreicht nun den AV-Knoten, wo er wenige Millisekunden verzögert wird (PQ-Strecke) und schließlich über das His-Bündel an die Ventrikel weitergeleitet, wo er über die Tawara-Schenkel auf die Purkinje-Fasern verteilt wird, wo er das Ventrikel-Myokard depolirisiert (QRS-Zacke), was zur Kontraktion und somit zum ausströmen des Blutes aus den Kammern führt. Danach bildet sich die Erregung der Herzmuskelzellen wieder zurück (ST-Strecke und T-Welle).

Der beschriebene Herzzyklus ist schematisch in Abbildung~\ref{fig:EKG} dargestellt.

\begin{figure}[tpbh]
	\centering
	\includegraphics[width=\linewidth]{EKG}
	\caption{Schematische Darstellung der Reizausbreitung und -rückbildung, in Relation zu den \glqq Zacken\grqq\ im \EKG, aus \cite[S.~4, Abb.~2]{SchusterTrappe:EKGKurs}.}
	\label{fig:EKG}
\end{figure}

Die Form der Zacken und Wellen gibt Auskunft über die elektrische Aktivität des Herzens, \emph{nicht} jedoch über die motorische Aktivität. Das \EKG\ kann also nicht anzeigt, \glqq wie gut\grqq\ das Herz schlägt.

Anhand der verschiedenen Erregungsausbreitungs- und -rückbildungsmuster lassen sich jedoch Aussage über die Gesundheit des Myokards treffen. Für das Krankheitsbild \emph{Akutes Koronarsyndrom} (im Volksmund \textsl{Herzinfarkt}) stellt das \EKG\ eines der wichtigsten Diagnoseverfahren dar.

\subsection{Technik}

\subsubsection{Operationsverstärker}

Als Operationsverstärker (engl.: \textsl{operational amplifier}, OpAmp) wird ein integrierter Schaltkreis bezeichnet, der beliebige Eingangsspannungen verstärken kann. Das Schaltbild eines Operationsverstärkers ist in Abbildung~\ref{fig:opamp} dargestellt.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+) ++(-0.1,0) node[left] {$E_+$} to[short,o-] (opamp.+)
		(opamp.-) ++(-0.1,0) node[left] {$E_-$} to[short,o-] (opamp.-)
		(opamp.out) ++(0.1,0) node[right] {$A$} to[short,o-] (opamp.out)
		(opamp.up) ++(0,.5) node[right] {$U_{-\mathrm{Bat}}$} to[short,o-] (opamp.up)
		(opamp.down) ++(0,-.5) node[right] {$U_{+\mathrm{Bat}}$} to[short,o-] (opamp.down);
	\end{tikzpicture}
	\caption{Schaltbild eines idealen Operationsverstärkers.}
	\label{fig:opamp}
\end{figure}

Der Operationsverstärker in Abbildung~\ref{fig:opamp} besitzt einen invertierenden Eingang $E_-$, einen nichtinvertierenden Eingang $E_+$ und einen Ausgang $A$. Zudem besitzt er die Versorgungsleitungen $U_{-\mathrm{Bat}}$ und $U_{+\mathrm{Bat}}$.

Die Differenz der an $E_-$ und $E_+$ anliegenden Spannungen wird vom Operationsverstärker nach dem Muster verstärkt, dass
%
\begin{align*}
	\begin{pmatrix}
		E_+ \\ E_-
	\end{pmatrix}
	\to
	U_A =
	\begin{cases}
		U_{+\mathrm{Bat}} & \text{für } E_+ > E_- \\
		\frac{1}{2} (U_{+\mathrm{Bat}} + U_{-\mathrm{Bat}}) & \text{für } E_+ = E_- \\
		U_{-\mathrm{Bat}} & \text{für } E_+ < E_- \\
	\end{cases}
\end{align*}
%
Für einen idealen Operationsverstärker gilt dabei, dass $U_{\pm\mathrm{Bat}} = \pm \infty$. Zudem gelten zwei weitere Bedingungen:
%
\begin{enumerate}
	\item \label{itm:ideal1} Der Widerstand beider Eingänge ist unendlich, d.\,h.\ es fließt kein Strom in den Eingängen.

	\item \label{itm:ideal2} Der Widerstand des Ausgangs ist null, d.\,h.\ die Ausgangsspannung ist unabhängig vom Lastwiderstand.
\end{enumerate}

Bei einem invertierenden Operationsverstärker liegt $E_+$ auf Masse und $E_-$ bildet aufgrund von $E_+ = E_- = 0$ einen \emph{virtuellen Massepunkt}.

\subsubsection{Der Differenzverstärker}

Im Schaltbild in Abbildung~\ref{fig:diffopamp} ist ein Subtrahierer oder Differenzverstärker abgebildet.

Um mit einem Operationsverstärker zwei Spannungen voneinander zu subtrahieren muss zur Eingangsspannung am invertierenden Eingang $E_-$ zusätzlich eine Spannung am nichtinvertierenden Eingang $E_+$ angelegt werden. Da immer noch gilt $E_+ = E_-$ verliert $E_-$ seine Eigenschaft als virtueller Massepunkt.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-1.5) node[ground] {} to[R,l_=$R_p$,-*] (opamp.+)
		(opamp.-)+(-2,0) node[left] {$E_1$} to[R=$R_1$,o-*] (opamp.-)
		(opamp.+)+(-2,0) node[left] {$E_2$} to[R,l_=$R_2$,o-*] (opamp.+)

		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.out)+(0,1.5) to[R,l_=$R_N$] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		(opamp.out)+(1,0) node[right] {$A$} to[short,o-*] (opamp.out);
	\end{tikzpicture}
	\caption{Schaltbild eines Operationsverstärkers als Differenzverstärker.}
	\label{fig:diffopamp}
\end{figure}

Für seine Ausgangsspannung gilt
%
\begin{align*}
	U_A = -U_{E_1} \cdot \frac{R_N}{R_1} + U_{E_2} \cdot \frac{R_p \cdot (R_N + R_1)}{R_1 \cdot (R_p + R_2)}
\end{align*}

\section{Versuchsaufbau}

Das Prinzip eines \EKG s ist recht einfach. Es handelt sich lediglich um einen Differenzverstärker mit symmetrischer Versorgungsspannung und Glättungskondensatoren.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}
		\draw node[op amp] (opamp) at (0,0) {};
		\draw (opamp.-) to[R,l_=$R_1$] ++(-1.5,0) to[C,l_=$C_1$,-o] ++(-1,0) coordinate (E1);
		\draw (opamp.+) to[R=$R_1$] ++(-1.5,0) to[C=$C_2$,-o] ++(-1,0) coordinate (E2);
		\node[left] at (E1) {$E_-$};
		\node[left] at (E2) {$E_+$};
		\draw (opamp.-) to[short,*-] ++(0,1) coordinate (help1) to[R=$R_3$] (opamp.out |- help1) to[short,-*] (opamp.out);
		\draw (opamp.+) to[R,l_=$R_4$,*-*] ++(0,-2) coordinate (help2);
		\draw (opamp.out) -- ++(0.7,0) to[C=$C_3$] ++(1,0) to[C=$C_4$] ++(1,0) node[ocirc] (A) {};
		\node[right] at (A) {$A$};
		\draw (help2 -| E1) node[rground] {} -- (help2 -| A) node[rground] {};
		\draw (opamp.up) |- ++(2,0.5) -- ++(0,-2) ++(0,-1) coordinate (help3)  to[battery1=$B$] ++(0,1) coordinate(bat);
		\draw (help3) to[short,-*] ($(help3)!.5!(help3 -| opamp.down)$) coordinate(help4) to[short] (help3 -| opamp.down);
		\draw (help3 -| opamp.down) ++(0,1) to[battery1=$B$] (help3 -| opamp.down) -- (opamp.down);
		\draw ($(help3)!.5!(help3 -| opamp.down)$) to[short,-*] (help2 -| help4);
		\draw (bat -| opamp.down) to[short,*-] ++(3,0) coordinate(help5) to[R=$R_5$,-*] (help5 |- help2);
	\end{tikzpicture}
	\caption{\EKG\ auf Basis eines Differenzverstärkers.}
	\label{fig:aufbau}
\end{figure}

Die Schaltung wird folgendermaßen dimensioniert:
%
\begin{description}
	\item[Widerstände]
		\begin{align*}
			R_1 = R_2 &= \SI{10}{\kilo\ohm} \\
			R_3 = R_4 &= \SI{10}{\mega\ohm} \\
			R_5 &= \SI{10}{\kilo\ohm} \\
		\end{align*}
	\item[Kondensatoren]
		\begin{align*}
			C_1 = C_2 = C_3 = C_4 &= \SI{10}{\micro\farad}
		\end{align*}
	\item[Batterien]
		Es werden zwei in Reihe geschaltete \SI{9}{\V} Batterien verwendet.
		\begin{align*}
			B \mathbin{\hat{=}} \text{\SI{9}{\V} Batterie}
		\end{align*}
	\item[Operationsverstärker]
		 Als Operationsverstärker kommt \texttt{OP177G} zum Einsatz.
\end{description}

\section{Durchführung}

Nun wird die Elektrode, die auf Masse liegt ($E_+$) an die linke Hand angeschlossen, die andere Elektrode ($E_-$) wird an die rechte Hand und das linke Bein angeschlossen.

Die Signalleitungen vom Körper zur Schaltung erfolgen mittels Koaxialkabeln um Störungen zu vermeiden.

Der Ausgang $A$ wird mit dem Oszilloskop beobachtet.

\section{Auswertung}

Die Aufnahme eines \EKG s war nicht möglich, da das Signal in der \SI{50}{\Hz}-Netz-Überlagerung unterging.

\nocite{SchusterTrappe:EKGKurs}
\raggedright
\bibliography{../bibliography}

\end{document}
