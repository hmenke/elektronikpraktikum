#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,1.5
set pointsize 0.5
set output 'SlewRate-200Vpp.tex'

set xlabel 'Zeit $t$ $[\si{\micro\s}]$'
set ylabel 'Spannung $U$ $[\si{\volt}]$'
set y2label '$U_e = \SI{200}{\milli\V_{pp}}$'
set format '\num[detect-all]{%g}'

#set key below maxrows 2
unset key
set grid linetype 3 linecolor 0
set fit errorvariables
set ytics 0.05

plot \
'Kurvenform-200mVpp.dat' using ($1*1e6):4 with lines linetype 1 linecolor 1 title '\textrm{CH1}' , \
'Kurvenform-200mVpp.dat' using ($1*1e6):5 with lines linetype 1 linecolor 3 title '\textrm{CH2}'
