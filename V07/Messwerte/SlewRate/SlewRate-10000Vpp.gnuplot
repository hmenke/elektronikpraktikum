#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.0
set pointsize 0.5
set output 'SlewRate-10000Vpp.tex'

set xlabel 'Zeit $t$ $[\si{\micro\s}]$'
set ylabel 'Spannung $U$ $[\si{\volt}]$'
set y2label '$U_e = \SI{10000}{\milli\V_{pp}}$'
set format '\num[detect-all]{%g}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables
set yrange [-6:6]
set ytics 3

load 'fitparameter.dat'
f(a,b,x) = a*x + b
fit[80:125] f(a1,b1,x) 'Kurvenform-10000mVpp.dat' using ($1*1e6):5 via a1,b1
fit[125:170] f(a2,b2,x) 'Kurvenform-10000mVpp.dat' using ($1*1e6):5 via a2,b2
update 'fitparameter.dat'

plot \
'Kurvenform-10000mVpp.dat' using ($1*1e6):4 with lines linetype 1 linecolor 1 title '\textrm{CH1}' , \
'Kurvenform-10000mVpp.dat' using ($1*1e6):5 with lines linetype 1 linecolor 3 title '\textrm{CH2}' , \
f(a1,b1,x) linetype 2 linecolor 4 title '$\mathcal{F}_{a_1}(\nu)$' , \
f(a2,b2,x) linetype 3 linecolor 4 title '$\mathcal{F}_{a_2}(\nu)$'
