#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.7
set pointsize 0.5
set output 'FFT-Sinus.tex'

set xlabel 'Frequenz $\nu$'
set ylabel 'Amplitude $\Gamma$ $[\si{a. u.}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange [:1.2e+11]
set yrange [-60:155]
set ytics ( \
	"" -55+0.0*70, \
	"" -55+0.5*70, \
	"" -55+1.0*70, \
	"" -55+1.5*70, \
	"" -55+2.0*70, \
	"" -55+2.5*70  \
)

plot \
'FFT-Sinus-Vpp=1.5V.dat' using ($1*1e6):($4+0*70) with lines linetype 1 linecolor 1 title '$U = \SI{1.5}{\V_{pp}}$' , \
'FFT-Sinus-Vpp=1V.dat' using ($1*1e6):($4+1*70) with lines linetype 1 linecolor 2 title '$U = \SI{1.0}{\V_{pp}}$' , \
'FFT-Sinus-Vpp=300mV.dat' using ($1*1e6):($4+2*70) with lines linetype 1 linecolor 3 title '$U = \SI{0.3}{\V_{pp}}$'
