#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'OP177G-Phase.tex'

set xlabel 'Frequnz $\nu$ $[\si{\Hz}]$'
set ylabel 'Phasenverschiebung $\Delta \phi$ $[\si{\degree}]$'
set format '\num[detect-all]{%g}'
set format x '\num[detect-all]{e%L}'

set key below maxrows 2 spacing 1.25 width 5
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale x
set ytics 80

plot \
'OP177G-UE=100Vpp-RN=1kOhm-V=1.txt' using 1:3 with linespoints linecolor 3 pointtype 5 title '$\Delta \phi(\nu)[\num{e0}]$', \
'OP177G-UE=100Vpp-RN=10kOhm-V=10.txt' using 1:3 with linespoints linecolor 3 pointtype 7 title '$\Delta \phi(\nu)[\num{e1}]$', \
'OP177G-UE=100Vpp-RN=100kOhm-V=100.txt' using 1:3 with linespoints linecolor 3 pointtype 9 title '$\Delta \phi(\nu)[\num{e2}]$'
