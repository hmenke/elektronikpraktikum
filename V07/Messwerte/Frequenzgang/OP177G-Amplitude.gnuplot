#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'OP177G-Amplitude.tex'

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Ausgangsspannung $U_A$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'
set format x '\num[detect-all]{e%L}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale x
set xrange [1e1:1e8]
set yrange [-10:50]

load 'fitparameter.dat'
f(a,b,x) = 20 * log10(a/sqrt(1+(a*x/b)**2))
fit f(a4,b4,x) 'OP177G-UE=100Vpp-RN=1kOhm-V=1.txt' using 1:2 via a4,b4
fit f(a5,b5,x) 'OP177G-UE=100Vpp-RN=10kOhm-V=10.txt' using 1:2 via a5,b5
fit f(a6,b6,x) 'OP177G-UE=100Vpp-RN=100kOhm-V=100.txt' using 1:2 via a6,b6
update 'fitparameter.dat'

plot \
'OP177G-UE=100Vpp-RN=1kOhm-V=1.txt' using 1:2 with points linecolor 3 pointtype 5 title '$V(\nu)[\num{e0}]$', \
'OP177G-UE=100Vpp-RN=10kOhm-V=10.txt' using 1:2 with points linecolor 3 pointtype 7 title '$V(\nu)[\num{e1}]$', \
'OP177G-UE=100Vpp-RN=100kOhm-V=100.txt' using 1:2 with points linecolor 3 pointtype 9 title '$V(\nu)[\num{e2}]$', \
f(a4,b4,x) linecolor 1 linetype 1 title '$\mathcal{F}_{a_4,b_4}(\nu)$', \
f(a5,b5,x) linecolor 1 linetype 2 title '$\mathcal{F}_{a_5,b_5}(\nu)$', \
f(a6,b6,x) linecolor 1 linetype 3 title '$\mathcal{F}_{a_6,b_6}(\nu)$'
