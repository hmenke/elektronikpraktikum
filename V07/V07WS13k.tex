\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V07: Eigenschaften realer Operationsverstärker}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Kim Kafenda}\noaffiliation
\date{2.\ Dezember 2013}

\newcommand{\SR}{\textnormal{S\kern-1pt\relax R}}
\begin{abstract}
	In diesem Versuch geht es um das Verhalten von realer Operationsverstärkern. Zunächst wird die Phasenverschiebung zwischen Ein- und Ausgangssignal untersucht, sowie der Frequenzgang der Amplitude.
	Die Auswirkungen der endlichen Signalansteigs- und Signalabfallszeit werden im Real- und Fourierraum betrachtet.
	Abschließend wird die Offsetspannung eines realen Operationsverstärkers gemessen und durch eine Kompensationsschaltung minimiert.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

Ein realer Operationsverstärker (engl.: \textsl{operational amplifier}, OpAmp) weicht von einem idealen Operationsverstärker, wie im vorhergehenden Versuch betrachtet, in einigen Punkten ab.
Diverse Dinge, wie zum Beispiel Widerstände gleich unendlich oder gleich null sind natürlich technisch nicht realisierbar.
Die Abweichungen umfassen folgende Punkte
%
\begin{itemize}
	\item Für die Ausgangsspannung eines realen Operationsverstärkers gilt
		%
		\begin{equation}
			U_A =
			\begin{aligned}[t]
				&V_\mathrm{diff} \cdot (U_{E_+} - U_{E_-}) \\
				&+ V_\mathrm{gleich} \cdot \frac{1}{2} (U_{E_+} + U_{E_-}) \\
				&+ V_\mathrm{diff} \cdot U_\mathrm{Offset}
			\end{aligned}
			\label{eq:realopamp}
		\end{equation}
		
		Die Differenzverstärkung $V_\mathrm{diff}$ ist beim realen Operationsverstärker nicht unendlich, sondern lediglich sehr groß.

		Die Gleichtaktverstärkung $V_\mathrm{gleich}$ ist beim idealen Operationsverstärker null, während sie beim realen zwar klein, aber dennoch nicht verschwindend ist.

		Die Gleichtaktunterdrückung ist definiert als:
		%
		\begin{equation*}
			G = \frac{V_\mathrm{diff}}{V_\mathrm{gleich}}
		\end{equation*}
		%
		Man sieht, dass je größer dieser Faktor ist, desto ähnlicher ist der reale OpAmp dem idealen OpAmp.

		Das Absolutglied $V_\mathrm{diff} \, U_\mathrm{Offset}$ liefert auch eine Ausgangsspannung bei verschwindender Differenzspannung. Diese Offsetspannung entsteht irgendwo im Operationsverstärker und ist temperaturabhängig, lässt sich aber kompensieren.

	\item Der ideale Operationsverstärker besitzt unendliche Eingangswiderstände, woraus resultiert, dass die Eingangsströme null sind. In der Realität fließt natürlich immer ein Strom in den Eingängen, der nicht seitengleich sein muss, was auf die Definition führt
		%
		\begin{equation*}
			\begin{aligned}
				I_\mathrm{Bias} &= \frac{1}{2} (I_{E_+} + I_{E_-}) && \text{Biasstrom} \\
				I_\mathrm{Offset} &= I_{E_+} - I_{E_-} && \text{Offsetstrom}
			\end{aligned}
		\end{equation*}

	\item Der ideal Operationsverstärker besitzt einen Ausgangswiderstand von null, es findet also kein Spannungsabfall statt, da kein Innenwiderstand existiert. Ein realer Operationsverstärker hat einen Ausgangswiderstand von ca.\ \SI{100}{\ohm}.

		Im Falle einer Gegenkopplung ist dies jedoch unerheblich, da diese meist nicht vom Ausgangswiderstand abhängen.
\end{itemize}

\subsection{Kenngrößen realer Operationsverstärker}

Diverse Kenngrößen beschreiben das Verhalten eines realen Operationsverstärkers.

\begin{description}
	\item[Verstärkungs-Bandbreiten-Produkt] (engl.: \textsl{gain-bandwidth-product})

		Ein Operationsverstärker verstärkt ein Signal nur bis zu einer Grenzfrequenz $\nu_g$, darüber nimmt die Verstärkung reziprok zu $\nu$ ab, bis sie den Wert $1$ bei der Transitfrequenz annimmt. Der Abfall findet so statt, dass
		%
		\begin{equation*}
			\nu \cdot V(\nu) = \operatorname{const}
		\end{equation*}
		%
		dieses Produkt wird als \emph{Verstärkungs-Bandbreiten-Produkt} bezeichnet.

	\item[Flankensteilheit] (engl.: \textsl{slew rate})

		Legt man eine Spannung an, deren Ableitung einen $\delta$-Peak enthält (z.\,B.\ $U_E = \operatorname{rect}(t)$), so springt die Ausgangsspannung nicht instantan, sondern mit einer Flanken. Die maximale Steigung dieser Flanke (maximale Spannungsänderung) wird als \emph{Flankensteilheit} bezeichnet.

	\item[Eingangs-Offsetspannung] (engl.: \textsl{input offset voltage})

		Die in Gleichung~\eqref{eq:realopamp} zuerst auftauchende Offsetspannung muss, wie bereits beschrieben, kompensiert werden. Die Eingangs-Offsetspannung ist eben die Spannung, die angelegt werden muss um die Offsetspannung am Ausgang zu kompensieren.

	\item[Eingangs-Offsetsstrom] (engl.: \textsl{input offset current})

		Die Eingänge des realen Operationsverstärkers sind nicht stromlos, da durch den endlichen Widerstand und die Eingangs-Offsetspannung zwangsläufig ein Strom fließen muss.
\end{description}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.out) to[short,-o] (opamp.out) node[right] {$A$}

		(opamp.-) to[short,*-*] ($(opamp.-)+(-1,0)$) to[short,-o] ($(opamp.-)+(-2,0)$) node[left] {$E_-$}
		(opamp.+) to[short,*-*] ($(opamp.+)+(-1,0)$) to[short,-o] ($(opamp.+)+(-2,0)$) node[left] {$E_+$}

		(opamp.-) to[R,l_=$R_\mathrm{gleich}$] ($(opamp.-)+(0,1.5)$) node[ground,rotate=180] {}
		(opamp.+) to[R=$R_\mathrm{gleich}$] ($(opamp.+)+(0,-1.5)$) node[ground] {}
		(opamp.+) to[R=$R_\mathrm{gegen}$,bipoles/length=0.8cm] (opamp.-)
		($(opamp.-)+(-1,0)$) to[I,l^=$I_{B_-}$] ($(opamp.-)+(-1,1.5)$) node[ground,rotate=180] {}
		($(opamp.+)+(-1,0)$) to[I,l_=$I_{B_+}$] ($(opamp.+)+(-1,-1.5)$) node[ground] {};
	\end{tikzpicture}
	\caption{Ersatzschaltbild eines realen Operationsverstärkers.}
\end{figure}


\subsection{Fouriertransformation}

Ist $f(t) \in L^1(\mathbb{R}^n)$, dann gilt für die zeitkontinuierliche Fouriertransformierte
%
\begin{align*}
	\mathcal{F}[f](\omega) = \frac{1}{\sqrt{(2 \pi)^n}} \int_{\mathbb{R}^n} f(t) \, \mathrm{e}^{-\mathrm{j} \omega t} \diff t
\end{align*}

Ist nun auch $\mathcal{F}[f](\omega) \in L^1(\mathbb{R}^n)$, so existiert die zeitkontinuierliche Fourierrücktransformierte
%
\begin{align*}
	f(t) = \mathcal{F}^{-1}\left[ \mathcal{F}[f] \right](t) = \frac{1}{\sqrt{(2 \pi)^n}} \int_{\mathbb{R}^n} \mathcal{F}[f](\omega) \, \mathrm{e}^{-\mathrm{j} \omega t} \diff \omega
\end{align*}

Bei Messreihen handelt es sich jedoch meist um zeitdiskrete Signale, die nicht so transformiert werden können. Deshalb bedient man sich der diskreten Fouriertransformation (DFT). Sei dazu $f_k = f(t_k) \in \mathbb{C}$ an den diskreten Zeitpunkten $t_k$ \cite[S.~39]{PhaC:Skript}.
%
\begin{align*}
	\mathrm{DFT}(f_k)_n &= g_n = \sum_{k=0}^{N-1} f_k \mathrm{e}^{-\mathrm{j} \frac{2\pi}{N} n k} \\
	\mathrm{iDFT}(g_n)_k &= f(t_k) = \sum_{n=0}^{N-1} \frac{g_n}{N} \mathrm{e}^{\mathrm{j} \frac{2\pi}{N} n k}
\end{align*}
%
Dieses Verfahren ist von der Ordnung $\mathcal{O}(N^2)$, was für viele Datenpunkte von Nachteil ist.

Deshalb bedient man sich meist der \emph{Fast Fourier Transform} (FFT) nach Cooley und Tukey, welche für eine Anzahl von Datenpunkten $N = 2 M$ das Verfahren auf die Ordnung $\mathcal{O}(N \log N)$ reduzieren kann.

\section{Versuchsaufbau und -durchführung}
\subsection{Frequenzgang}

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.up) ++(0,0.5) node[right] {$U_{-\mathrm{Bat}}$} to[short,o-] (opamp.up)
		(opamp.down) ++(0,-0.5) node[right] {$U_{+\mathrm{Bat}}$} to[short,o-] (opamp.down)
		(opamp.+)+(0,-1) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E$] (lu) to[R=$R_1$,o-*] (opamp.-)
		(ground)+(-2,0) coordinate (ld) to[short,o-*] (ground)
		(ground)+(3.5,0) coordinate (rd) to[short,o-] (ground)
		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.out)+(0,1.5) to[R,l_=$R_N$] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		let \p1 = (rd), \p2 = (opamp.out) in
		(opamp.out) to[short,*-o] (\x1,\y2) coordinate[label=right:$A$] (ru);
		\draw[->] ($(lu)+(0,-0.2)$) -- node[right] {$U_E$} ($(ld)+(0,0.2)$);
		\draw[->] ($(ru)+(0,-0.2)$) -- node[right] {$U_A$} ($(rd)+(0,0.2)$);
	\end{tikzpicture}
	\caption{Schaltbild eines invertierenden Operationsverstärkers.}
	\label{fig:invopamp}
\end{figure}

Die Schaltung wird nach ABB.~\ref{fig:invopamp} aufgebaut. Der Operationsverstärker vom Typ \texttt{OP177G} wird verwendet. Die Widerstände sind anfangs $R_N = \SI{10}{\kilo\ohm}$ und $R_1 = \SI{1}{\kilo\ohm}$. Der Eingang $E_-$ wird mit dem Frequenzgenerator verbunden, außerdem wird das Eingangssignal auf Kanal 1 des Oszilloskops geleitet. Das Ausgangssignal wird ebenfalls mit dem Oszilloskop verbunden. Als Triggersignal wird das Sync-Signal des Frequenzgenerators verwendet. 

Zu Beginn wird der Frequenzgang der Amplitude und Phase für Frequenzen zwischen \SI{50}{\Hz} und \SI{5}{\mega\Hz} (in Faktor 10-Schritten) manuell gemessen.

Von nun an wird zur Messung das Programm LabVIEW verwendet. Es wird erneut eine Messung bei einer Verstärkung von 10 durchgeführt, dann für eine Verstärkung von 1 und 100, indem der Widerstand $R_N$ einmal als \SI{1}{\kilo\ohm} bzw. \SI{100}{\kilo\ohm} gewählt wird.

Die gleiche Messreihe wird anschließend für den Operationsverstärker \texttt{OP27G} durchgeführt.

\subsection{Slew Rate}
Um die Slew Rate zu untersuchen, wird an dem Frequenzgenerator eine Rechteckfrequenz mit einer Frequenz von \SI{10}{\kilo\Hz} eingestellt. Um eine Verstärkung von 10 zu erhalten, wird für $R_N$ wieder ein \SI{10}{\kilo\ohm}-Widerstand gewählt.

Mit LabVIEW und der \texttt{oszisnapshot.vi} wird die Kurvenform der Ein- und Ausgangsspannung für verschiedene Eingansspannungsamplituden aufgenommen. Die Amplitude der Ausgangsspannung soll dabei von \SI{200}{\milli\V_{pp}} auf \SI{10}{\V_{pp}} ansteigen.

Im nächsten Versuchsabschnitt wird beim Generator ein Sinus-Signal eingestellt, durch variieren der Amplitude wird eine Spannungsamplitude gewählt, bei der das Ausgangssignal des Verstärkers beginnt, verzerrt zu werden. Beim Oszilloskop wird die FFT-Funktion eingeschaltet und der Plot für verschiedene Verzerrungsgrade, also verschiedene Spannungsamplituden aufgenommen.

\subsection{Offset-Kompensation}
Die Verstärkung der Schaltung wird auf \SI{40}{\deci\bel}, also auf 100 eingestellt, indem $R_N = \SI{100}{\kilo\ohm}$ gewählt wird. Beide Eingänge werden auf Masse gelegt und die Ausgangsspannung wird mit einenem Digitalmultimeter gemessen. Anschließend wird dieser Offset-Spannung kompensiert. Dazu werden die Pins 1 und 8 wie in ABB:~\ref{fig:Kompensationsschaltung} beschaltet. Als variabler Widerstand kommt ein Potentiometer zum Einsatz. Durch Drehen wird der geeignete Widerstand zur Minimierung der Offset-Spannung ermittelt, welcher mit dem Handmultimeter ausgelesen wird.
%
\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45,every path/.style={}]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.up) node[above right] {8} to[R] ++(0,2) -- ++(-0.5,0) -- ++(0,-1.7) coordinate (n1);
		\draw (n1)+(0,0.1) node[above left] {1};
		\draw[<-] (opamp.up) ++(0.2,1) -- node[circ] (A) {} ++(1,0) node[right] {\SI{15}{\V}};
		\draw (opamp.down) -- +(0,-1) node[below] {\SI{-15}{\V}};
		\draw (A) -- +(0,-1.4) node[above right] {7};
		\node[above] at (A) {\SI{10}{\kilo\ohm}};
		\node[left=1.5cm] at (opamp) {Input};
		\node[right=1.5cm] at (opamp) {Output};
	\end{tikzpicture}
	\caption{Schaltplan der Kompensationsschaltung für die Offset-Spannung}
	\label{fig:Kompensationsschaltung}
\end{figure}
%

\section{Messwerte}
\subsection{Frequenzgang}
Die manuell erfassten Messwerte des Frequenzgangs für Amplitude und Phase bei einer Eingangsspannungsamplitude von $U_E = \SI{100}{\milli\V_{pp}}$ sind in Tabelle~\ref{tab:manuelle_messwerte} zu sehen.\\

\subsection{Offset-Kompensation}
Die Ausgangsspannung ohne Offset-Kompensation ist $U_A = \SI{1.38}{\milli\V}$. Mit einem Widerstand von $R = \SI{9.49}{\kilo\ohm}$ wurde die Ausgangsspannung auf \SI{0.64}{\milli\V} kompensiert.

\section{Formeln}
Die folgenden Zeichen und Einheiten wurden im Weiteren verwendet
%
\begin{itemize}
	\item $V$ ohne Einheit: Reale Verstärkung des OpAmp
	\item $V_0$ ohne Einheit: Eingestelle Verstärkung des OpAmp
	\item $U$ in Volt [\si{\V}]: Spannung
		\begin{itemize}
			\item Index $a/e$: Ein- bzw. Ausgangsspannung des OpAmp
			\item Index $a/e,\textnormal{pp}$: Peak-to-Peak Ein- bzw. 								Ausgangsspannung des OpAmp
		\end{itemize}
	\item $\Delta \phi$ in Grad [\si{\degree}]: Phasenverschiebung
	\item $\nu$ in Hertz [\si{\Hz}]: Frequenz
	\item $\nu_g$ in Hertz [\si{\Hz}]: Grenzfrequenz aufgrund der 				Tiefpasseigenschaft des OpAmp
	\item $\nu$ in Hertz [\si{\Hz}]: Grenzfrequenz aufgrund der Slew Rate
	\item $f_\textnormal{GBW}$ in Hertz [\si{\Hz}]: Verstärkung-Bandbreite Produkt
	\item $\textnormal{SR}$: in Volt pro Sekunde [$\si{\V\per\s}$]: Slew Rate, maximale Anstiegs- und Abfallsszeit der Ausgangsspannung
\end{itemize}

\begin{description}
	\item[Verstärkung] Die Verstärkung eines Operationsverstärkers ist gegeben durch
		\begin{equation}
			V = \frac{U_a}{U_e} = \frac{R_N}{R_1}
			\label{eq: verstärkung}
		\end{equation}

	\item[Phasenverschiebung] Will man die Phasenverschiebung aus der Zeitverschiebung zweier periodischer Signale berechnen gilt
		\begin{equation}
			\Delta \phi = \SI{360}{\degree} \cdot \nu \cdot \Delta t
			\label{eq: phasenverschiebung}
		\end{equation}

	\item[Gain-Bandwidth-Product] Der Zusammenhang zwischen Verstärkung und Frequenz beim Operationsverstärker ist
		\begin{equation}
			V(\nu) = \frac{V_0}{\sqrt{1+ \left( \frac{\nu}{\nu_g} \right)^2}} = 		\frac{V_0}{\sqrt{1+ \left( \frac{\nu V_0}{f_\textnormal{GBW}} \right)^2}} 
			\label{eq: GBP}
		\end{equation}
		Dabei ist $\nu_g = \frac{f_\textnormal{GBW}}{V_0}$.

	\item[Slew-Rate] Die Slew Rate, die maximale Anstiegszeit des Verstärkers, ist definiert als
		\begin{equation}
			\SR = \max(|\dot{U}_a(t)|)
		\end{equation}
		Ein Sinus-Signal mit einer Frequenz 
		\begin{equation}
			\nu_g > \frac{\SR}{\pi \cdot U_{a,\textnormal{pp}}}
			\label{eq. Grenzfrequenz SR}
		\end{equation}
		wird nicht mehr verzerrungsfrei übertragen.	
\end{description}

\section{Auswertung}
\subsection{Frequenzgang}
Die Ergebnisse der manuellen Messung des Frequenzganges der Amplitude und Phase bei einer Verstärkung von 10 ist in Tabelle~\ref{tab:manuelle_messwerte} zu sehen. Anhand der manuell gemessenen Daten kann die Theorie bestätigt werden. Zum einen nimmt die Verstärkung überhalb der Grenzfrequenz ab, die Phasenverschiebung nimmt zu.

\begin{table}
	\centering
	\caption{Manuelle Messung des Frequenzganges von Amplitude und Phase des Verstärkers im Bereich \SI{50}{\Hz} bis \SI{5}{\mega\Hz}. Auf Basis der gemessenen Werte wurden die Verstärkung und die Phasenverschiebung in \si{\degree} berechnet.}
	\begin{tabular}{SSSSS}
		\toprule 
		{Frequenz} &
		{Amplitude} &
		{Phase} &
		{Verstärkung} &
		{Phaseversch.} \\
		{$\nu$ $[\si{\Hz}]$} &
		{$U_A$ $[\si{\V}]$} &
		{$\Delta t$ $[\si{\micro\s}]$} &
		{$V$ $[1]$} &
		{$\Delta \phi$ $[\si{\degree}]$} \\
		\midrule 
		5e1 & 0.960 & 100.0 & 9.6 & 1.8   \\ 
		5e2 & 0.960 & 100.0 & 9.6 & 18.0  \\ 
		5e3 & 0.960 & 100.0 & 9.6 & 180.0 \\ 
		5e4 & 0.760 & 12.4  & 7.6 & 223.2 \\ 
		5e5 & 0.110 & 1.7   & 1.1 & 306.0 \\ 
		5e6 & 0.008 & 0.1   & 0.1 & 324.0 \\ 
		\bottomrule 
	\end{tabular}
	\label{tab:manuelle_messwerte}
\end{table}

Da es sehr mühsam ist ausreichend viele Messpunkte für eine quantitative Auswertung aufzunehmen wird die Auswertung der manuellen Daten hier auf eine qualitative Natur beschränkt.

In dem Plot~\ref{fig:OP177G-Amplitude} ist der Phasengang der Amplitude bei verschiedenen Verstärkungen des Verstärkers \texttt{OP177G} eingetragen. Für den zweiten Verstärker \texttt{OP27G} ist dies in ABB.~\ref{fig:OP27G-Amplitude} zu sehen. Die Messwerte wurden mit LabVIEW erfasst.
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/Frequenzgang/}{OP27G-Amplitude.tex}
	\caption{Frequenzgang der Amplitude bei verschiedenen Verstärkungen des \texttt{OP27G}-Verstärkers.}
	\label{fig:OP27G-Amplitude}
\end{figure}
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/Frequenzgang/}{OP177G-Amplitude.tex}
	\caption{Frequenzgang der Amplitude bei verschiedenen Verstärkungen des \texttt{OP177G}-Verstärkers.}
	\label{fig:OP177G-Amplitude}
\end{figure}
%

An die Messdaten wurde eine Funktion der Form
%
\begin{equation}
	\mathcal{F}_{a,b}(\nu) = 20 \log \left[ \frac{a}{\sqrt{1+\left( \frac{a \nu}{b} \right)^2}} \right]
\end{equation}
%
gefittet. Nach Gleichung~\ref{eq: GBP} ist $a$ die Verstärkung $V_0$ und $b$ das Gain-Bandwidth-product $f_\textnormal{GBW}$. In Tabelle~\ref{tab: Verstärkungen OpAmp 27} und \ref{tab: Verstärkungen OpAmp 177} sind die Ergebnisse der Fits für beide Operationsverstärker zusammengefasst.

\begin{table}[!h]
	\centering
	\caption{Verstärkungen und Bandbreitenprodukte des \texttt{OP27G}.}
	\begin{tabular}{SSSSS}
		\toprule
		{Verst.} & {$a$} & {$b$} & {$V_0$} & {$f_\textnormal{GBW}$ $[\si{\mega\Hz}]$} \\ 
		\midrule
		1   &  1.01 & 8.00e6 &  1.01 & 8.0 \\ 
		10  &  9.73 & 8.00e6 &  9.73 & 8.0 \\ 
		100 & 99.16 & 6.86e6 & 99.18 & 6.8 \\ 
		\bottomrule
	\end{tabular} 
	\label{tab: Verstärkungen OpAmp 27}
\end{table}
%
\begin{table}[!h]
	\centering
	\caption{Verstärkungen und Bandbreitenprodukte des \texttt{OP177G}.}
	\begin{tabular}{SSSSS}
		\toprule
		{Verst.} & {$a$} & {$b$} & {$V_0$} & {$f_\textnormal{GBW}$ $[\si{\mega\Hz}]$} \\ 
		\midrule
		1   &  1.01 & 4.00e5 &  1.01 & 0.40 \\ 
		10  &  9.85 & 4.57e5 &  9.85 & 0.46 \\ 
		100 & 99.54 & 5.50e5 & 99.54 & 0.55 \\ 
		\bottomrule
	\end{tabular} 
	\label{tab: Verstärkungen OpAmp 177}
\end{table}

Die auf den Datenblättern angegebenen Werte für das Bandbreitenprodukt lauten $f_\textnormal{GBW} = \SI{8.0}{\mega\Hz}$ (\texttt{OP27G}) und $f_\textnormal{GBW} = 0.4 - \SI{0.6}{\mega\Hz}$ (\texttt{OP177G}). Die ermittelten Werte stimmen also mit den angegebenen Werten überein.


Der Frequenzgang der Phasenverschiebung beider Operationfsverstärker ist in ABB.~\ref{fig:OP27G-Phase} und \ref{fig:OP177G-Phase} zu sehen.
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/Frequenzgang/}{OP27G-Phase.tex}
	\caption{Frequenzgang der Phase des \texttt{OP27G}}
	\label{fig:OP27G-Phase}
\end{figure}
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/Frequenzgang/}{OP177G-Phase.tex}
	\caption{Frequenzgang der Phase des \texttt{OP177G}}
	\label{fig:OP177G-Phase}
\end{figure}
%
\subsection{Slew Rate}

In ABB.~\ref{fig:SlewRate} sind die Signalverläufe der Ausgangsspannung für verschiedene Amplituden der Eingangsspannung zu sehen. Als Eingangsspannung wird ein Rechtecksignal mit \SI{10}{\kilo\Hz} gewählt. Es fällt auf, dass mit steigender Amplitude das Signal der Ausgangsspannung verändert wird. Es entwickelt sich immer mehr zu einer Freieckspannung. Das liegt an der begrenzten Anstiegszeit des Operationsverstärkers (Slew Rate). Bei einer Eingangsspannung mit $U = \SI{10}{\V_{pp}}$ lässt sich die Slew Rate bestimmen, in dem man eine Gerade der Form
%
\begin{equation}
	\mathcal{F}_a(\nu) = a \nu + b
\end{equation}
%
an die ansteigende bzw. abfallende Flanke der Dreiecksspannung legt. Der Parameter $b$ ist dabei irrelevant. Die Steigung der aufsteigenden Flanke beträgt \[ a_\textnormal{auf} = \num{0.195} \] und die der abfallenden \[ a_\textnormal{ab} = \num{-0.211}. \] Die \emph{maximale} Änderungsgeschwindigkeit beträgt also $\SR = \SI{0.21}{\V \per \micro \s}$.
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/SlewRate/}{SlewRate-200Vpp.tex}
	
	\subimport{Messwerte/SlewRate/}{SlewRate-500Vpp.tex}
		
	\subimport{Messwerte/SlewRate/}{SlewRate-1000Vpp.tex}
			
	\subimport{Messwerte/SlewRate/}{SlewRate-5000Vpp.tex}
			
	\subimport{Messwerte/SlewRate/}{SlewRate-10000Vpp.tex}
	\caption{Signalform der Ausgangsspannung in Abhängigkeit des Amplitudenwertes der Eingangsspannung. Verwendet wurde der \texttt{OP177G} mit einer Verstärkung von 10}
	\label{fig:SlewRate}
\end{figure}

In Abbildung~\ref{fig:FFT-Sinus} sind drei Fourierspektren für drei verschiedene Eingangsspannungen abgebildet. Betrachtet man die Spektren unter dem Gesichtspunkt der in Abbildung~\ref{fig:SlewRate} zunehmenden Verzerrung des Signals, so erkennt man leicht folgende Eigenschaften:
%
\begin{description}
	\item[$U_e = \SI{0.3}{\V_{pp}}$] Das übertragene Signal ist bis auf Rauschen immernoch ein beinahe perfekter Sinus. Es ergibt sich also ein Peak bei der Anregungsfrequenz des Sinus.

	\item[$U_e = \SI{1.0}{\V_{pp}}$] Der Sinus wird zunehmend verzerrt. Der Peak bei der Anregungsfrequenz dominiert weiterhin, jedoch werden höhere Moden ebenfalls sichtbar, was mit den zusätzlich nötigen Termen in der Fourier-Reihe zu begründen ist.

	\item[$U_e = \SI{1.5}{\V_{pp}}$] Der Sinus ist nun komplett zu einem Dreieck-Signal verzerrt. Der Theorie zufolge müssten die Koeffizienten mit $k^{-2}$ abfallen, was im Plot jedoch nicht so ganz stimmt. Es ist jedoch gut sichtbar, dass weitere Koeffizienten hinzukommen.
\end{description}

\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/SlewRate/}{FFT-Sinus.tex}
	\caption{FFT eines Sinussignals bei unterschiedlichen Eingangsspannungen}
	\label{fig:FFT-Sinus}
\end{figure}

\subsection{Limitierte Bandbreite}

Die Slew Rate begrenzt die Bandbreite, bei der eine verzerrungsfreie Übertragung möglich ist, nach Gleichung~\ref{eq. Grenzfrequenz SR}, jedoch begrenzt auch das Tiefpassverhalten der Schaltung die Bandbreite. Hier gilt \[ \nu_g = \frac{f_\textnormal{GBW}}{V_0}. \] Da die Grenzfrequenz des Tiefpasses bei diesem Versuch niedriger war, hat die Slew Rate keinen Einfluss auf die Bandbreitenbegrenzung. Bei schnellen Ausgangsspannungänderungen, z.b bei Rechteckspannungen mit hoher Amplitude, ist die Slew Rate groß, und somit auch die Grenzfrequenz. Dann bestimmt die vom Tiefpassverhalten bestimmte Grenzfrequenz die Bandbreite, diese ist unabhängig von Ein- und Ausgangsspannung. Ist die Slew Rate klein, sorgt sie für die Bandbreitenbegrenzung.


%Nach Gleichung~\ref{eq. Grenzfrequenz SR} hat die Slew Rate einen Einfluss auf die Bandbreite. Für die Grenzfrequenz gilt somit:
\[
	\nu_g > \frac{\SI{0.21}{\V \per \micro \s}}{\pi \cdot \SI{10}{\V}}
\]

\subsection{Offset-Kompensation}
Bei einer kurzgeschlossenen Eingängen wurde eine Ausgangsspannung von $U_A = \SI{1.38}{\milli\V}$ gemessen. Die Verstärkung beträgt 100. Daraus errechnet sich die Eingangs-Offsetspannung zu:
\[
	U_\textnormal{Offset} = \frac{U_A}{V} = \frac{U_A = \SI{1.38}{\milli\V}}{100} = \SI{13.8}{\micro \V}
\]	
Nach ABB.~\ref{fig:Kompensationsschaltung} wurde die Offsetspannung mit Hilfe einer Widerstandsdekade kompensiert. Bei einem Widerstand von $R = \SI{9.49}{\kilo\ohm}$ wurde eine minimale Eingansspannung von \SI{0.64}{\milli\V} erreicht.

\section{Fehlerrechnung}

Im Folgenden werden relativen Abweichungen zu den Werten in den Datenblättern berechnet.

\begin{description}
	\item[Gain-Bandwidth-Product] Für den Verstärker \texttt{OP177G} wurde durchschnittlich $f_\textnormal{GBW} = \SI{7.6}{\mega\Hz}$ gemessen. Das Datenblatt sieht vor $\hat{f}_\textnormal{GBW} = \SI{8.0}{\mega\Hz}$. Folglich gilt
		\begin{align*}
			\mathcal{Q}[\hat{f}_\textnormal{GBW},f_\textnormal{GBW}] &= \frac{\hat{f}_\textnormal{GBW} - f_\textnormal{GBW}}{\hat{f}_\textnormal{GBW}} \\
			&= \frac{\SI{8.0}{\mega\Hz} - \SI{7.6}{\mega\Hz}}{\SI{8.0}{\mega\Hz}} = \SI{5}{\percent}
		\end{align*}
		%
		Für den Verstärker \texttt{OP27G} wurde im Durchschnitt $f_\textnormal{GBW} = \SI{0.47}{\mega\Hz}$ gemessen. Laut Datenblatt gilt typischerweise $\hat{f}_\textnormal{GBW} = \SI{0.6}{\mega\Hz}$. Folglich gilt
		\begin{align*}
			\mathcal{Q}[\hat{f}_\textnormal{GBW},f_\textnormal{GBW}] &= \frac{\hat{f}_\textnormal{GBW} - f_\textnormal{GBW}}{\hat{f}_\textnormal{GBW}} \\
			&= \frac{\SI{0.6}{\mega\Hz} - \SI{0.47}{\mega\Hz}}{\SI{0.6}{\mega\Hz}} = \SI{21}{\percent}
		\end{align*}

	\item[Slew Rate] Die Slew Rate wurde lediglich für \texttt{OP177G} bestimmt mit $\SR = \SI{0.21}{\volt\per\micro\s}$. Die Dokumentation gibt eine typische Slew Rate von $\hat{\SR} = \SI{0.3}{\volt\per\micro\s}$. Die relative Abweichung beträgt
		\begin{align*}
			\mathcal{Q}[\hat{\SR},\SR] &= \frac{\hat{\SR} - \SR}{\hat{\SR}} \\
			&= \frac{\SI{0.3}{\mega\Hz} - \SI{0.21}{\mega\Hz}}{\SI{0.3}{\mega\Hz}} = \SI{30}{\percent}
		\end{align*}

	\item[Input-Offset-Voltage] Für die Offsetspannung wurde $U_\textnormal{Offset} = \SI{13.8}{\micro\V}$ gemessen. Dies steht einem vorgesehenen Wert von \SI{20}{\micro\V} aus dem Datenblatt gegenüber.
		%
		\begin{align*}
			\mathcal{Q}[\hat{U}_\textnormal{Offset},U_\textnormal{Offset}] &= \frac{\hat{U}_\textnormal{Offset} - U_\textnormal{Offset}}{\hat{U}_\textnormal{Offset}} \\
			&= \frac{\SI{20}{\micro\V} - \SI{13.8}{\micro\V}}{\SI{20}{\micro\V}} = \SI{31}{\percent}
		\end{align*}
\end{description}

\section{Zusammenfassung}
In dem Versuch wurde das Verhalten realen Operationsverstärker untersucht. Es kamen die Opereationsverstärker \texttt{OP27G} und \texttt{OP177G} zum Einsatz.

Die gemessenen Bandbreitenprodukte von $f_\textnormal{GBW} = \SI{8.0}{\mega\Hz}$ (\texttt{OP27G}) und $f_\textnormal{GBW} = 0.4 - \SI{0.6}{\mega\Hz}$ (\texttt{OP177G}) stimmen mit den Angaben auf den Datenblättern überein.

Die Slew Rate des \texttt{OP177G} konnte bestimmt werden zu $\SR = \SI{0.21}{\V \per \micro \s}$, auch dieser Wert war laut Datenblatt zu erwarten.

Durch die Analyse der Fouriertransfomierten von Sinussignalen, konnte herausgefunden werden, dass für höhere Eingangsspannungen zunehmend mehr Obertöne der Grundschwingung hinzukommen. Das Sinussignal wird dabei zu einem Dreieckssignal deformiert.

Für die Bandbreitenbeschränkung war in diesem Versuch die Tiefpassverhalten der Schaltung verantwortlich.

Beim \texttt{OP177G} wurde eine Offset-Eingangsspannung von $U_\textnormal{Offset} =\SI{13.8}{\micro \V}$ gemessen, diese wurde nach ABB.~\ref{fig:Kompensationsschaltung} mit einem Widerstand von $R = \SI{9.49}{\kilo\ohm}$ kompensiert.



\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
