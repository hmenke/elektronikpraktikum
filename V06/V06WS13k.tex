\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V06: Grundschaltungen mit Operationsverstärkern}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Kim Kafenda}\noaffiliation
\date{25.\ November 2013}

\begin{abstract}
	Im Versuch werden die Eigenschaften und typischen Schaltungen von Operationsverstärkern untersucht. Es werden die Verstärkungen der verschiedenen Typen gemessen und mit den theoretischen Werten verglichen.
	Ein Schmitt-Trigger wird ebenfalls betrachtet und dessen Schalthysterese untersucht.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Operationsverstärker}

Als Operationsverstärker (engl.: \textsl{operational amplifier}, OpAmp) wird ein integrierter Schaltkreis bezeichnet, der beliebige Eingangsspannungen verstärken kann. Das Schaltbild eines Operationsverstärkers ist in Abbildung~\ref{fig:opamp} dargestellt.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+) ++(-0.1,0) node[left] {$E_+$} to[short,o-] (opamp.+)
		(opamp.-) ++(-0.1,0) node[left] {$E_-$} to[short,o-] (opamp.-)
		(opamp.out) ++(0.1,0) node[right] {$A$} to[short,o-] (opamp.out)
		(opamp.up) ++(0,.5) node[right] {$U_{-\mathrm{Bat}}$} to[short,o-] (opamp.up)
		(opamp.down) ++(0,-.5) node[right] {$U_{+\mathrm{Bat}}$} to[short,o-] (opamp.down);
	\end{tikzpicture}
	\caption{Schaltbild eines idealen Operationsverstärkers.}
	\label{fig:opamp}
\end{figure}

Der Operationsverstärker in Abbildung~\ref{fig:opamp} besitzt einen invertierenden Eingang $E_-$, einen nichtinvertierenden Eingang $E_+$ und einen Ausgang $A$. Zudem besitzt er die Versorgungsleitungen $U_{-\mathrm{Bat}}$ und $U_{+\mathrm{Bat}}$.

Die Differenz der an $E_-$ und $E_+$ anliegenden Spannungen wird vom Operationsverstärker nach dem Muster verstärkt, dass
%
\begin{align*}
	\begin{pmatrix}
		E_+ \\ E_-
	\end{pmatrix}
	\to
	U_A =
	\begin{cases}
		U_{+\mathrm{Bat}} & \text{für } E_+ > E_- \\
		\frac{1}{2} (U_{+\mathrm{Bat}} + U_{-\mathrm{Bat}}) & \text{für } E_+ = E_- \\
		U_{-\mathrm{Bat}} & \text{für } E_+ < E_- \\
	\end{cases}
\end{align*}
%
Für einen idealen Operationsverstärker gilt dabei, dass $U_{\pm\mathrm{Bat}} = \pm \infty$. Zudem gelten zwei weitere Bedingungen:
%
\begin{enumerate}
	\item \label{itm:ideal1} Der Widerstand beider Eingänge ist unendlich, d.\,h.\ es fließt kein Strom in den Eingängen.

	\item \label{itm:ideal2} Der Widerstand des Ausgangs ist null, d.\,h.\ die Ausgangsspannung ist unabhängig vom Lastwiderstand.
\end{enumerate}

In Abbildung~\ref{fig:invopamp} liegt $E_+$ auf Masse und $E_-$ bildet aufgrund von $E_+ = E_- = 0$ einen \emph{virtuellen Massepunkt}.

\subsection{Anwendungen}

\subsubsection{Invertierender Operationsverstärker}

Die einfachste Anwendung des Operationsverstärkers ist der Umkehrverstärker oder invertierender Operationsverstärker. Eine Schaltskizze ist in Abbildung~\ref{fig:invopamp} zu sehen. Für den Eingangswiderstand $R_1$ gilt
%
\begin{align*}
	R_1 = \frac{U_E}{I_E}
\end{align*}
%
Für die Ausgangsspannungen gilt wegen der Eigenschaften~\ref{itm:ideal1} und \ref{itm:ideal2} und der Beschaltung mit Gegenkopplung
%
\begin{align*}
	U_A = - U_E \frac{R_N}{R_1}
\end{align*}

Die Verstärkung beträgt somit
%
\begin{align*}
	V = - \frac{R_N}{R_1}
\end{align*}

Dabei stellt $R_1$ einen Spannungs-Strom-Wandler und die restliche Schaltung eine Strom-Spannungs-Wandler dar.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-1) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E$] (lu) to[R=$R_1$,o-*] (opamp.-)
		(ground)+(-2,0) coordinate (ld) to[short,o-*] (ground)
		(ground)+(3.5,0) coordinate (rd) to[short,o-] (ground)
		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.out)+(0,1.5) to[R,l_=$R_N$] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		let \p1 = (rd), \p2 = (opamp.out) in
		(opamp.out) to[short,*-o] (\x1,\y2) coordinate[label=right:$A$] (ru);
		\draw[->] ($(lu)+(0,-0.2)$) -- node[right] {$U_E$} ($(ld)+(0,0.2)$);
		\draw[->] ($(ru)+(0,-0.2)$) -- node[right] {$U_A$} ($(rd)+(0,0.2)$);
	\end{tikzpicture}
	\caption{Schaltbild eines invertierenden Operationsverstärkers.}
	\label{fig:invopamp}
\end{figure}

\subsubsection{Der Summationsverstärker}

Legt man an den invertierenden Eingang eine zusätzliche Eingangsspannung an, so ergibt sich der Umkehraddierer oder auch Summationsverstärker, wie er in Abbildung~\ref{fig:addopamp} abgebildet ist.

Für die Ausgangsspannung gilt hier
%
\begin{align*}
	U_A = - R_N \sum\limits_{i} \frac{U_{E_i}}{R_i}
\end{align*}

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-0.5) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E_2$] (lu) to[R,l_=$R_2$,o-*] (opamp.-)
		coordinate (helper) at ($(opamp.-)+(0,1)$)
		(helper) to[short] (opamp.-)
		(helper)+(-2,0) coordinate[label=left:$E_1$] (lu) to[R=$R_1$,o-*] (helper)
		let \p1 = (opamp.out), \p2 = (helper) in
		(helper) to[R=$R_N$] (\x1,\y2) to[short,-*] (opamp.out)
		(opamp.out)+(1,0) node[right] {$A$} to[short,o-] (opamp.out);
	\end{tikzpicture}
	\caption{Schaltbild eines Operationsverstärkers als Summationsverstärker.}
	\label{fig:addopamp}
\end{figure}

\subsubsection{Der Differenzverstärker}

Im Schaltbild in Abbildung~\ref{fig:diffopamp} ist ein Subtrahierer oder Differenzverstärker abgebildet.

Um mit einem Operationsverstärker zwei Spannungen voneinander zu subtrahieren muss zur Eingangsspannung am invertierenden Eingang $E_-$ zusätzlich eine Spannung am nichtinvertierenden Eingang $E_+$ angelegt werden. Da immer noch gilt $E_+ = E_-$ verliert $E_-$ seine Eigenschaft als virtueller Massepunkt.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-1.5) node[ground] {} to[R,l_=$R_p$,-*] (opamp.+)
		(opamp.-)+(-2,0) node[left] {$E_1$} to[R=$R_1$,o-*] (opamp.-)
		(opamp.+)+(-2,0) node[left] {$E_2$} to[R,l_=$R_2$,o-*] (opamp.+)

		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.out)+(0,1.5) to[R,l_=$R_N$] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		(opamp.out)+(1,0) node[right] {$A$} to[short,o-*] (opamp.out);
	\end{tikzpicture}
	\caption{Schaltbild eines Operationsverstärkers als Differenzverstärker.}
	\label{fig:diffopamp}
\end{figure}

Für seine Ausgangsspannung gilt
%
\begin{align*}
	U_A = -U_{E_1} \cdot \frac{R_N}{R_1} + U_{E_2} \cdot \frac{R_p \cdot (R_N + R_1)}{R_1 \cdot (R_p + R_2)}
\end{align*}

\subsubsection{Der nichtinvertierende Operationsverstärker (Elektrometerverstärker)}

In Abbildung~\ref{fig:ninvopamp} ist der nichtinvertierende Operationsverstärker oder Elektrometerverstärker abgebildet.

Im Gegensatz zu den anderen Verstärkern ändert er das Vorzeichen der Spannung nicht, außerdem ist sein Eingangswiderstand unendlich (idealisiert).

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp,yscale=-1] (opamp) {}
		(opamp.+)+(-1,0) node[left] {$E$} to[short,o-] (opamp.+)

		(opamp.out)+(1,0) node[right] {$A$} to[short,o-*] (opamp.out)
		(opamp.out)+(0,-1.5) coordinate (h) to[R=$R_1$,*-*] (opamp.out)
		(h)+(0,-1.5) node[ground] {} to[R=$R_2$,-*] (h)
		let \p1 = (h), \p2 = (opamp.-) in
		(\p2) to[short] (\x2,\y1) to[short] (\p1);
	\end{tikzpicture}
	\caption{Schaltbild eines Operationsverstärkers als nichtinvertierender Verstärker.}
	\label{fig:ninvopamp}
\end{figure}

Die Ausgangsspannung ist wegen $U_{E_+} = U_{E_-} = U_E$ gegeben durch
%
\begin{align*}
	U_A = \left( 1 + \frac{R_1}{R_2} \right) U_E
\end{align*}

\subsubsection{Schmitt-Trigger}

Vertauscht man beim invertierenden Operationsverstärker einfach die beiden Eingänge, so erhält man eine positive Rückkopplung. Dies ist bekannt als Schmitt-Trigger; ein Schaltbild ist in Abbildung~\ref{fig:schmitttrig} zu sehen.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp,yscale=-1] (opamp) {}
		(opamp.-)+(0,-0.5) node[ground] (ground) {} to[short] (opamp.-)
		(opamp.+)+(-2,0) node[left] {$E$} to[R=$R_1$,o-*] (opamp.+)
		(opamp.+)+(0,1) coordinate (rn) to[short] (opamp.+)
		(opamp.out)+(0,1.5) to[R,l_=$R_2$] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		(opamp.out) to[short,*-o] ++(1,0) node[right] {$A$};
	\end{tikzpicture}
	\caption{Schaltbild eines Operationsverstärkers als Schmitt-Trigger.}
	\label{fig:schmitttrig}
\end{figure}

Der Schmitt-Trigger wirkt als Schwellwertschalter, da er solange die Ausgangsspannung $U_A = U_{-\mathrm{Bat}}$ liefert, bis die Schwellspannung $U_{S_+}$ von $U_E$ überschritten wird, springt dann auf $U_A = U_{+\mathrm{Bat}}$ und liefert dies solange bis $U_E$ kleiner als $U_{S_-}$ wird.

Diese Schwellspannungen sind
%
\begin{align*}
	U_{S_+} &= U_{+\mathrm{Bat}} \frac{R_1}{R_2} \\
	U_{S_-} &= U_{-\mathrm{Bat}} \frac{R_1}{R_2}
\end{align*}

\section{Versuchsaufbau und -durchführung}
Der Versuch besteht aus mehreren Teilen. Zuerst wird der invertierende Verstärker und Schmitt-Trigger untersucht. dann der Differenz- und Summationsverstärker. Zum Schluss wird noch der nichtinvertierende Verstärker untersucht.

\subsection{Invertierender Verstärker}
Der invertierenden Verstärker wird nach ABB.~\ref{fig:invopamp} aufgebaut. Es werden folgende Widerstände verwendet: $R_N = \SI{10}{\kilo \ohm}$ und $R_1 =\SI{1}{\kilo \ohm}$. Als Betriebsspannungen $U_{\pm}$ für den Operationsverstärker dienen die Anschlüsse auf dem Steckbrett, diese haben $+\SI{15}{\volt}$ und $-\SI{15}{\volt}$. Alle Messungen werden mit LapVIEW erfasst.\\
Es soll die Gleichspannungsverstärkung gemessen werden, dazu wird die Eingangsspannung $U_E$ von \SI{0}{\volt} soweit hochgefahren, bis die Ausgangsspannung $U_A$ gleich der Betriebsspannung ist. Die Spannung $U_-$ wird währenddessen mit dem Handmultimeter gemessen. Nun wird der Rückkopplungswiderstand $R_N$ entfernt und die Messung erneut durchgeführt. Für die nächste Messung werden andere Widerstände eingesetzt, $R_N = \SI{}{\ohm}$ und $R_1 = \SI{}{\ohm}$. Eine weitere Messung wird vorgenommen wenn der Eingang $E_+$ des OpAmp offen gelassen wird.

\subsection{Schmitt-Trigger}
Um einen Schmitt-Trigger zu bekommen, werden die beiden Eingänge vertauscht, sodass man die Schaltung nach ABB.~\ref{fig:schmitttrig} erhält. Der Eingang $E$ wird mit dem Frequenzgenerator verbunden. Dort wird eine \SI{100}{\Hz}-Dreieckspannung mit einer Peak-to-Peak-Amplitude von $U_{V_{pp}} = \SI{2}{\volt}$ eingestellt. Über eine BNC-Stück wird die Eingangsspannung außerdem mit Kanal 1 des Oszilloskops verbunden. Die Ausgangsspannung wird mit Kanal 2 des Oszilloskops verbunden.\\
Die Ausgangspegel und die Schaltschwelle wird mit Hilfe der Curserfunktion des Oszilloskops ermittelt.
Mit LabVIEW und der \texttt{oszisnapshot.vi} wird ein Diagramm der Ausgangsspannung $U_A$ über der Eingangsspannung $U_E$ aufgenommen. Nun wird der Widerstand $R_1$ zu $R_1 = \SI{3.3}{\kilo\ohm}$ geändert und $U_{V_{pp}} = \SI{5}{\volt}$ und das Diagramm erneut aufgenommen.

\subsection{Differenz- und Summationsverstärker}
Der Differenzverstärker wird nach ABB.~\ref{fig:diffopamp} aufgebaut, dabei sind $R_1 = R_2 = R_p = \SI{10}{\ohm}$ und $R_N= \SI{22}{\ohm}$.

Es soll die Ausgansspannung in Abhängigkeit von der Eingangsspannung für beide Eingänge getrennt gemessen werden. Dazu wird zunächst der Eingang $E_2$ auf Masse gelegt und anschließend $E_1$. Mit LabVIEW werden die Messungen aufgenommen. Dabei läuft die Eingansspannung erneut von \SI{0}{\V} soweit hochgefahren, bis der OpAmp in Sättigung geht.

Nun wird einmal an beiden Eingängen die gleiche Spannung $U_{E_1} = U_{E_2} = U_E$ angelegt und einmal $U_{E_1} = - U_{E_2} = U_E$, es wird erneut gemessen.

Die vorliegende Schaltung wird leicht modifiziert, sodass man einen Summationsverstärker erhält (siehe ABB.~\ref{fig:Summationsverstaerker}).
Die Messung der Ausgangsspannung über der Eingansspanung erfolgt mit demselben LabVIEW-Programm.

\subsection{Nichtinvertierender Verstärker}
Der nichtinvertierende Verstärker wird nach ABB.~\ref{fig:ninvopamp} aufgebaut. Dabei ist $R_2 = \SI{220}{\V}$. $R_1$ wird so gewählt, dass eine Verstärkung von 10 entsteht.

Mit LabVIEW wird die Ausgangsspannung in Abhängigkeit der Eingangsspannung gemessen. 

Schlussendlich wird noch durch Strom- und Spannungsmessung der Eingangswiderstand des OpAmp bestimmt. 


\section{Formeln}
Die folgenden Zeichen und Einheiten wurden im Weiteren verwendet
%
\begin{itemize}
	\item $V$ ohne Einheit: Verstärkung
		\begin{itemize}
			\item Index $\textrm{inv}$: des invertierenden Verstärkers.
			\item Index $\pm$: des (nicht)invertierenden Eingangs des Differenzverstärkers.
			\item Index $\textrm{el}$: des Elektrometerverstärkers.
		\end{itemize}
	\item $R$ in Ohm (\si{\ohm}): Widerstand
		\begin{itemize}
			\item Index $N$: Rückkopplungswiderstand
			\item Index $i$: ein Beschaltungswiderstand
			\item Index $P$: Beschaltungswiderstand vor Masse beim Differenzverstärker
		\end{itemize}
	\item $U$ in Volt (\si{\volt}): Spannung
		\begin{itemize}
			\item Index $\pm$: positive und negative Betriebsspannung des OpAmps
			\item Index $A$: Ausgangsspannung des OpAmps
			\item Index $E_i$: Eingangspannung an Eingang $E_I$
			\item Index $S_{\pm}$: positive und negative Schaltschwelle des Schmitt-Triggers
		\end{itemize}
\end{itemize}

\begin{description}
	\item[Invertierender Verstärker] Die Verstärkung eines invertierenden Verstärkers berechnet sich nach:
		\begin{equation}
			V_{\textrm{inv}} = - \frac{R_N}{R_1} = \frac{U_A}{U_E}
		\end{equation}

	\item[Schmitt-Trigger] Für die Berechnung der Schaltschwellen des Schmitt-Triggers gilt:
		\begin{equation}
			U_{S_+} = U_+ \cdot \frac{R_1}{R_2}
			\label{Schwellspannung1}
		\end{equation}
		und
		\begin{equation} 
			U_{S_-} = U_- \cdot \frac{R_1}{R_2}
			\label{Schwellspannung2}
		\end{equation}
		dabei ist $U_{S_+} > \SI{0}{\volt}$ und  $U_{S_-} < \SI{0}{\volt}$.

	\item[Differenzverstärker] Die Ausgangsspannung des Differenzverstärkers ist wie folgt zu berechnen:
		\begin{equation}
			U_A = - U_{E_1} \cdot \frac{R_N}{R_1} + U_{E_2} \cdot \frac{R_P(R_N + R_1)}{R_1 (R_P + R_2)}
			\label{verstaerkung_diff}
		\end{equation}
		Beim Summationsverstärker gilt für die Ausgangsspannung:
		\begin{equation}
			U_A = - R_N \cdot \sum_{i=1}^{n} \frac{U_{E_i}}{R_i}
		\end{equation}


	\item[Nichtinvertierender Verstärker] Die Ausgangsspannung des nichtinvertierenden Verstärkers berechnet sich nach:
		\begin{equation}
			U_A = \left(1+ \frac{R_1}{R_2} \right) \cdot U_E
		\end{equation}
		für seine Verstärkung gilt somit
		\begin{equation}
			V_{\textrm{el}} = 1 + \frac{R_1}{R_2}
			\label{verstaerkung_nichtinv}
		\end{equation}
\end{description}

\section{Messwerte}
Beim \emph{invertierenden Verstärker} wurde im Bereich $U_{E} = \SI{0}{\volt},\ldots,\SI{1.413}{\volt}$ gemessen. Ohne Gegenkopplungwiderstand wurde eine Ausgangsspannung von $U_A = \SI{-14.144}{\volt}$ gemessen. Der Gegenkopplungswiderstand für eine bestimmte Verstärkung kann berechnet werden mittels
\[ R_N = R_1 \cdot 10^{V/20}, \] wobei $V$ die Verstärkung in \si{\deci\bel} ist.

Für den \emph{Schmitt-Trigger} wurde am Oszilloskop eine Peak-to-Peak-Spannung von $U_\textrm{Vpp} = \SI{28.6}{\volt}$, eine Effektivspannung von $U_\textrm{eff} = \SI{14.2}{\volt}$ und mit der Cursorfunktion die Schaltschwellen bei $U_{S\pm} = \pm \SI{1.46}{\volt}$ abgelesen. Für eine weitere Messung wurde $R_1$ variiert zu $R_1 = \SI{3.3}{\kilo\ohm}$.

\section{Auswertung}
\subsection{Invertierender Verstärker}
In ABB.~\ref{fig:Inv-Amp-Gleichspannungsverstaerkung} wurde die Ausgangsspannung $U_A$ gegen die Eingangsspannung $U_E$ geplottet.
%
 \begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/InvertierenderVerstearker/}{Gleichspannungsverstaerkung.tex}
	\caption{Gleichspannungsverstärkung des invertierenden Operationsverstärkers.}
	\label{fig:Inv-Amp-Gleichspannungsverstaerkung}
\end{figure}
%
Es ist deutlich der lineare Zusammenhang zu sehen. Der lineare Fit \[ \mathcal{F}_a(x) = a \cdot x \] liefert für die Steigung den Wert $a = \num{-9.813}$, dieser stimmt bis auf eine relative Abweichung von ca.\ \SI{1.9}{\percent} genau mit der theoretisch berechneten Verstärkung überein: 
\begin{equation}
	V_{\textrm{inv}} = - \frac{R_N}{R_1} = \frac{\SI{-10}{\kilo\volt}}{\SI{1}{\kilo\V}} = -10
\end{equation}
Ab einer Eingangsspanung von ca.\ \SI{1.4}{\V}, steigt die Ausgangsspannung auf Grund der \emph{Over-Voltage-Protection} nicht weiter an. Von da an ist die Augansspannung konstant $U_A = \SI{14.1}{\V}$. Dies liegt daran, dass der OpAmp mit einer Sättingungsspannung von $U = \SI{15}{\V}$ betrieben wird. Wäre die Betriebsspannung höher, so würde auch die Eingangsspannung weiter linear verstärkt werden.
Die Messung von $U_-$ mit dem Handmultimeter ergibt einen leicht fluktuierenden Wert von $U_- \approx \SI{0}{\V}$, solange bis der OpAmp in Sättignung gelangt, dann steigt $U_-$ an. In diesem Bereich ist $U_-$ also unabhängig von der Eingangsspannung $U_E$, es liegt ein \emph{virtueller Massepunkt} vor.

Wird der Gegenkopplungswiderstand entfernt, so misst man sofort eine Ausgangsspannung von $U_A = \SI{-14.144}{\V} \approx \SI{15}{\V}$. Es wird also bis zur Betriebsspannung verstärkt. Dies liegt daran, dass die Rückkopplung dafür gesorgt hat, dass an den beiden Eingängen dasselbe Potential herrscht. Ist dies nicht mehr möglich, so ist $U_-$ kein virtueller Massepunkt mehr und die Potentialdifferenz wird \glqq unendlich\grqq\ (bis zur Betriebsspannung) verstärkt.

%Nun soll durch Änderung der Widerstände eine andere Verstärkung umgesetzt werden. Wir wählen $R_1 = \SI{9999}{\ohm}$ und  $R_N = \SI{9999}{\ohm}$, somit ist eine Verstärkung von 
%\begin{equation}
%	-V_{\textrm{inv}} = \frac{R_N}{R_1} = 
%\end{equation}
%
%zu erwarten.

Wird der Eingang $E_+$ des OpAmp offen gelassen, so erhält man folgendes Diagramm:
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/InvertierenderVerstearker/}{E+-offen.tex}
	\caption{Gleichspannungsverstärkung des invertierenden Operationsverstärkers bei offenem $E+$-Kanal.}
	\label{fig:Inv-Amp-E+-offen}
\end{figure}
%
Die Ausgangsspannung fluktuiert in der Nähe der Betriebsspannung.

Durch bloßes Vertauschen der Eingänge lässt sich \textit{kein} nichtinvertierender Verstärker aufbauen. Das liegt daran, dass so der Ausgang mit dem positiven Eingang $E_+$ rückgekoppelt wäre, da aber der Ausgang ebenfalls eine positive Spannung liefert, ist es nicht möglich die Potentialdifferenz zwischen $E_+$ und $E_-$ auszugleichen. Somit würde sofort die maximale Verstärkung erreicht werden. Eine solche Schaltung wird Schmitt-Trigger genannt.

\subsection{Schmitt-Trigger}
Wird an den Schmitt-Trigger eine \SI{100}{\Hz}-Dreieckspannung angelegt erhält man für unterschiedliche $R_1$-Werte die Diagramme in ABB.~\ref{fig:SchmittTrig-Hysterese1} und~\ref{fig:SchmittTrig-Hysterese2}.
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/SchmittTrigger/}{SchmittTrigger-Hysterese-R1=1000-R2=10000-UE=2Vpp.tex}
	\caption{Hysterese-Kurve des Schmitt-Triggers bei $R_1 = \SI{1}{\kilo\ohm}$, $R_2 = \SI{10}{\kilo\ohm}$ und $U_\textrm{Vpp} = \SI{2}{\volt}$.}
	\label{fig:SchmittTrig-Hysterese1}
\end{figure}
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/SchmittTrigger/}{SchmittTrigger-Hysterese-R1=3300-R2=10000-UE=5Vpp.tex}
	\caption{Hysterese-Kurve des Schmitt-Triggers bei $R_1 = \SI{3.3}{\kilo\ohm}$, $R_2 = \SI{10}{\kilo\ohm}$ und $U_\textrm{Vpp} = \SI{5}{\volt}$.}
	\label{fig:SchmittTrig-Hysterese2}
\end{figure}
%
Wie in den ABB.~\ref{fig:SchmittTrig-Hysterese1} und \ref{fig:SchmittTrig-Hysterese2} zu sehen ist, besitzt der Schmitt-Trigger die Eigenschaft, ab einer bestimmten Eingangsspannung direkt auf die maximal zu erreichende Spannung zu springen (Schwellwertschalter). Es gibt eine positive und negative Schwellspannung mit gleichem Betrag, die Ausgangsspannung springt beim Überschreiten auf die positive bzw. negative Betriebsspannung. Auch zu erkennen ist, dass mit größerem Widerstand $R_1$ die Schwellspannung anwächst.

Die mit dem Cursor des Oszilloskops abgelesenen Ausgangspegel betragen \SI{14.3}{\V} und die Schwellspannungen $U_{S_{\pm}} = \pm \SI{1.46}{\V}$. Über Formel~\eqref{Schwellspannung1} und \eqref{Schwellspannung2} berechnen sich die Schwellspannungen zu $U_{S_{\pm}} = \pm \SI{999}{\V}$.

\subsection{Differenz- und Summationsverstärker}
In ABB.~\ref{fig:Differenzverstaerker} sieht man die Abhängigkeit der Ausgangsspannung von der Eingansspannung für $E_1 = \texttt{GND}$, $E_2 = \texttt{GND}$, $E_1 = E_2$ und $E_1 = -E_2$. An die Messwerte wurde jeweils eine Ursprungsgerade gefittet mit
\begin{align*}
	\mathcal{F}_a(x) = a \cdot x .
\end{align*}
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/Differenzverstaerker/}{Differenzverstaerker.tex}
	\caption{Gleichspannungsverstärkung des Differenzverstärkers abhängig von der Beschaltung der Eingänge.}
	\label{fig:Differenzverstaerker}
\end{figure}
%
In Tabelle~\ref{tab:Differenzverstaerker-Verstaerkung} sind die nach Formel~\ref{verstaerkung_diff} theoretisch berechneten Werte der Verstärkung und die durch das Plotprogramm ausgegebenen Verstärkungen (Steigung der Fitgeraden) aufgelistet.
\begin{table}[!h]
	\centering
	\begin{tabular}{c@{\quad}SS}
		\toprule
		                     & {Fit}  & {berechnet} \\
		\midrule
		$E_1 = \texttt{GND}$ & 1.642  & 1.6         \\
		$E_2 = \texttt{GND}$ & -2.306 & -2.2        \\
		$E_1 = E_2$          & -0.657 & -0.6        \\
		$E_1 = -E_2$         & -3.963 & -3.8        \\
		\bottomrule
	\end{tabular}
	\caption{Vergleich zwischen den gemessenen Verstärkungen und den theoretisch berechneten.}
	\label{tab:Differenzverstaerker-Verstaerkung}
\end{table}
%
Ist einer der beiden Eingänge auf Masse gesetzt so lassen sich die Formeln
\[
	V_{E_1} = - \frac{R_N}{R_1}
\]
und
\[
	V_{E_2} =  \frac{R_P(R_N + R_1)}{R_1(R_P + R_2)}
\]
nach $R_1$ bzw. $R_2$ auflösen. Mit der gemessenen Verstärkung berechnet man für die Eingangswiderstände:
\[
	R_1 = - \frac{\SI{22}{\kilo\ohm}}{-2.306} =  \SI{9.54}{\kilo\ohm}
\]
und
\[
	R_2 = \frac{R_P(R_N + R_1)}{V_{E_2} \cdot R_1} - R_P = \SI{12.47}{\kilo\ohm}
\]

Wird die Schaltung zu einem Summationsverstärker umgebaut und die Eingangsspannungen $E_1 = -E_2$ angelegt, so erhält man ABB.~\ref{fig:Summationsverstaerker}.
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/Differenzverstaerker/}{Summationsverstaerker.tex}
	\caption{Gleichspannungsverstärkung des Summationsverstärkers bei $E_1 = -E_2$.}
	\label{fig:Summationsverstaerker}
\end{figure}
%
Beachtet man die Skala der $y$-Achse, so erkennt man, dass die Ausgangsspannung im Wesentlichen verschwindet. Dies stimmt mit den Erwartungen überein. Der Grund für das leichte Ansteigen der Ausgangsspannung ist die nicht verschwindende Gleich"|taktverstärkung.

Da wir den Summationsverstärker nicht mit $E_1 \neq -E_2$ gemessen haben, ist es nicht möglich dessen Verstärkung auszurechnen.

\subsection{Nichtinvertierender Verstärker}
Nach Formel~\eqref{verstaerkung_nichtinv} berechnet sich für eine Verstärkung von $V=\num{10}$ der Widerstand $R_1$ zu
\[
	R_1 = (V-1)R_2 = (10-1)\SI{220}{\ohm} = \SI{1980}{\ohm}
\]
Der Widerstand wird mit Hilfe einer Widerstandsdekade realisiert.
In ABB.~\ref{fig:NInv-Amp-Gleichspannungsverstaerkung} ist die Ausgangsspannung in Abhängigkeit der Eingangsspannung aufgetragen. 
%
\begin{figure}[tpbh]
	\centering\sffamily
	\subimport{Messwerte/NichtInvertierenderVerstearker/}{Gleichspannungsverstaerkung.tex}
	\caption{Gleichspannungsverstärkung des nicht invertiernden Verstärkers.}
	\label{fig:NInv-Amp-Gleichspannungsverstaerkung}
\end{figure}
%
Der Fit liefert für die positive Gleichspannungsverstärkung den Wert $V=\num{9.985}$, welcher gut mit dem theoretischen Wert übereinstimmt.

Um den Einganswiderstand zu messen, wird ein Strommessgerät von den Eingang geschaltet. Über die Eingangsspannung aufgetragen erhält man den Plot in Abbildung~\ref{fig:NInv-Amp-Eingangsstrom}.
%
\begin{figure}[tpb]
	\centering\sffamily
	\subimport{Messwerte/NichtInvertierenderVerstearker/}{Eingangsstrom.tex}
	\caption{Eingangsstrom des nicht invertierenden Verstärkers.}
	\label{fig:NInv-Amp-Eingangsstrom}
\end{figure}
%
Es fließt so gut wie kein Strom, der Strom fluktuiert um die Null im \si{\micro\A}-Bereich. Somit ist der Widerstand sehr groß, für unsere Messwerte liegt er im Bereich einiger Megaohm.
Da diese Beschaltung des OpAmp wie ein Elektroskop zur Spannungsmessung verwendet werden kann und ebenfalls einen sehr hohen Eingangswiderstand hat, spricht man auch von einem Elektrometerverstärker.

\section{Fehlerrechnung}
Da für den Versuch professionelle Messgeräte verwendet wurden, sind in Gewicht fallende Fehlerquellen weitgehend auszuschließen. Zu beachten ist jedoch, dass für den Versuch der Operationsverstärker als ideal angenommen wurde. Da sämtliche theoretischen Werte unter dieser Voraussetzung berechnet wurden, kann nicht exakt der tatsächliche Wert bestimmt werden.

\begin{description}
	\item[Invertierender Verstärker] Theoretisch ergibt sich $\hat{V} = \num{-10}$, aus der Messung haben wir erhalten $V = \num{-9.813}$. Die relative Abweichung beträgt also
		\[ \mathcal{Q}[\hat{V},V] = \frac{\num{-10} - (\num{-9.813})}{\num{-10}} = \SI{1.9}{\percent} \]

	\item[Differenzverstärker] Der Differenzverstärker wurde in verschiedenen Regimes vermessen: $E_1 = \texttt{GND}$, $E_2 = \texttt{GND}$, $E_1 = E_2$ und $E_1 = -E_2$, die zugehörigen Werte sind in Tabelle~\ref{tab:Differenzverstaerker-Verstaerkung} dargestellt. Es wird jeweils die relative Abweichung vom theoretisch berechneten Wert ermittelt.
		\begin{align*}
			\mathcal{Q}[\hat{V}_{E_1 = \texttt{GND}},V_{E_1 = \texttt{GND}}] &= \frac{\num{1.6} - \num{1.642}}{\num{1.6}} = \SI{2.6}{\percent} \\
			\mathcal{Q}[\hat{V}_{E_2 = \texttt{GND}},V_{E_2 = \texttt{GND}}] &= \frac{\num{-2.2} - (\num{-2.306})}{\num{-2.2}} = \SI{4.8}{\percent} \\
			\mathcal{Q}[\hat{V}_{E_1 = E_2},V_{E_1 = E_2}] &= \frac{\num{-0.6} - (\num{-0.657})}{\num{-0.6}} = \SI{9.5}{\percent} \\
			\mathcal{Q}[\hat{V}_{E_1 = -E_2},V_{E_1 = -E_2}] &= \frac{\num{-3.8} - (\num{-3.963})}{\num{-3.8}} = \SI{4.3}{\percent}
		\end{align*}

	\item[Nichtinvertierender Verstärker] Der theoretische Wert wurde als $\hat{V} = \num{10}$ berechnet, der aus dem Fit erhaltene ist $V = \num{9.985}$. Die relative Abweichung beträgt
		\[ \mathcal{Q}[\hat{V},V] = \frac{\num{10} - (\num{9.985})}{\num{10}} = \SI{0.2}{\percent} \]
\end{description}

\section{Zusammenfassung}
In diesem Versuch sollten Operationsverstärker und die Funktionen der verschiedenen Rückkopplungsnetzwerke untersucht werden.

Im ersten Teil des Versuchs wurde der invertierende Verstärker untersucht. Die gemessene Verstärkung lag mit \num{-9.813} nahe am theoretisch berechneten Wert von \num{-10}. Wird der Gegenkopplungswiderstand entfernt, so erhält man sofort die maximale Ausgangsspannung. Das gleiche Ergebnis erhält man, wenn der Eingang $E_+$ offen gelassen wird.

Mit vertauschten Eingängen erhält man den Schmitt-Trigger. Es wurde dessen Hysterese-Kurve aufgezeichnet und verifiziert, dass er die Eigenschaften eines Schwellwertschalters besitzt.

Nun wurden Differenz- und Summationsverstärker angeschaut. Beim Differenzverstärker wurde die Verstärkung für verschiedene Beschaltungen der Eingänge gemessen. Außerdem wurden die Eingangswiderstände gemessen, zu $R_1= \SI{9.54}{\kilo\ohm}$ und $R_2 =\SI{12.47}{\kilo\ohm}$. In Wirklichkeiten hatten beide Eingangswiderstände den Wert $R= \SI{10}{\ohm}$.

Beim Summationsverstärker wurde, durch anliegen entgegengesetzter Spannungen, dessen Summationsverhalten gezeigt da, wie zu erwarten, so gut wie keine Ausgangsspannung hervorgerufen wurde.

Schlussendlich wurde noch der nichtinvertierende Verstärker untersucht. Die Schaltung wurde mit Hilfe einer Widerstandsdekade so dimensioniert, dass eine Verstärkung von $V = \num{10}$ eintritt.
Über eine zwischengeschaltete Stromstärkemessung wurde gezeigt, dass der Operationsverstärker einen sehr große Eingangswiderstand besitzt.

\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
