#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'SchmittTrigger-Hysterese-R1=1000-R2=10000-UE=2Vpp.tex'

set xlabel 'Eingangsspannung $U_E$ $[\si{\volt}]$'
set ylabel 'Ausgangsspannung $U_A$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables

set xrange [-2:2]
set x2range [-2:2]
set x2tics ('\num[detect-all]{1.46}' 1.46, '\num[detect-all]{-1.46}' -1.46)

set arrow nohead from first 1.46,graph 0 to first 1.46,graph 1 linecolor 0 linetype 2 linewidth 3
set arrow nohead from first -1.46,graph 0 to first -1.46,graph 1 linecolor 0 linetype 2 linewidth 3

plot \
'<sed -e "1,22d" SchmittTrigger-Hysterese-R1=1000-R2=10000-UE=2Vpp.txt' using 2:1 with lines linecolor 3 title '$U_A(U_E)$'
