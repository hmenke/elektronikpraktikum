#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Gleichspannungsverstaerkung.tex'

set xlabel 'Eingangsspannung $U_E$ $[\si{\volt}]$'
set ylabel 'Ausgangsspannung $U_A$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables

load 'fitparameter.dat'
f(x) = a*x
fit[:1.4] f(x) '<sed -e "1,22d" NichtinvertVerstaerker-Gleichspannungsverstaerkung.txt' using 3:2 via a
update 'fitparameter.dat'

plot \
'<sed -e "1,22d" NichtinvertVerstaerker-Gleichspannungsverstaerkung.txt' using 3:2 with points pointtype 5 linecolor 3 title '$U_A(U_E)$', \
f(x) linecolor 1 linetype 2 title 'Fit $\mathcal{F}_a$'
