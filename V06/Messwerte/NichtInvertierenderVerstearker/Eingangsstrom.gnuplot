#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Eingangsstrom.tex'

set xlabel 'Eingangsspannung $U_E$ $[\si{\volt}]$'
set ylabel 'Eingangsstrom $I_A$ $[\si{\ampere}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables

set ytics 1e-7

plot \
'<sed -e "1,22d" NichtinvertVerstaerker-Eingangsstrom.txt' using 3:2 with linespoints pointtype 5 linecolor 3 title '$I_E(U_E)$'
