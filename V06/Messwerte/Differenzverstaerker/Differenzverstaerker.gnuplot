#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,7.3
set pointsize 0.5
set output 'Differenzverstaerker.tex'

set xlabel 'Eingangsspannung $U_E$ $[\si{\volt}]$'
set ylabel 'Ausgangsspannung $U_A$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange [:10]

load 'fitparameter.dat'
f(a,x) = a*x
fit f(a1,x) '<sed "1,22d" Differenzverstaerker-E1=GND.txt' via a1
fit[:6] f(a2,x) '<sed "1,22d" Differenzverstaerker-E2=GND.txt' via a2
fit f(a3,x) '<sed "1,22d" Differenzverstaerker-E1=E2.txt' via a3
fit[:3.8] f(a4,x) '<sed "1,22d" Differenzverstaerker-E1=-E2.txt' via a4
update 'fitparameter.dat'

set multiplot layout 4,1
set ytics 3
plot '<sed "1,22d" Differenzverstaerker-E1=GND.txt' using 3:2 with points pointtype 6 linetype 1 linecolor 3 title '$E_1 = \texttt{GND}$', \
f(a1,x) with lines linetype 3 linecolor 1 title '$\mathcal{F}_{a_1}(U_E)$'

set ytics 5
plot '<sed "1,22d" Differenzverstaerker-E2=GND.txt' using 3:2 with points pointtype 7 linetype 1 linecolor 3 title '$E_2 = \texttt{GND}$', \
f(a2,x) with lines linetype 4 linecolor 1 title '$\mathcal{F}_{a_2}(U_E)$'

set ytics 2
plot '<sed "1,22d" Differenzverstaerker-E1=E2.txt' using 3:2 with points  pointtype 4 linetype 1 linecolor 3 title '$E_1 = E_2$', \
f(a3,x) with lines linetype 1 linecolor 1 title '$\mathcal{F}_{a_3}(U_E)$'

set ytics 10
plot '<sed "1,22d" Differenzverstaerker-E1=-E2.txt' using 3:2 with points pointtype 5 linetype 1 linecolor 3 title '$E_1 = -E_2$', \
f(a4,x) with lines linetype 2 linecolor 1 title '$\mathcal{F}_{a_4}(U_E)$'
unset multiplot
