#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Summationsverstaerker.tex'

set xlabel 'Eingangsspannung $U_E$ $[\si{\volt}]$'
set ylabel 'Ausgangsspannung $U_A$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set ytics 0.02

load 'fitparameter.dat'
f(a,x) = a*x
fit f(b1,x) '<sed -e "1,22d" Summationsverstaerker-E1=-E2.txt' using 3:2 via b1
update 'fitparameter.dat'

plot \
'<sed "1,22d" Summationsverstaerker-E1=-E2.txt' using 3:2 with points pointtype 5 linecolor 3 title '$U_A(U_E)$'
#f(b1,x) with lines linetype 2 linecolor 1 title '$\mathcal{F}_{b}(U_E)$'

