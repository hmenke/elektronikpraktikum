\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V10: Flip-Flops}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Boris Bonev}\noaffiliation
\date{13.\ Januar 2014}

\begin{abstract}
	In diesem Versuch werden die Funktionen verschiedenenr digitaler Schaltungen auf Basis von Flip-Flops untersucht. Damit werden auf Basis von JK-Flip-Flops grundlegenede Schaltungen wie asynchrone 4-stufige Dual- und Dezimalzähler, sowie synchrone Dezimalzähler betrachtet. Abschließend wird mit JK-Flip-Flops ein Schieberegister realisiert und dessen Charakteristika analysiert.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Flip-Flop Schaltungen}

Flip-Flops sind getaktete digitale Schaltungen. Die Ausgänge der logischen Bauelemente sind auf die Eingänge rückgekoppelt, sodass das Ausgangssignal nicht nur vom Eingangssignal, sondern auch von vorherigen Eingangssignal abhängt. Es lässt sich also Information speichern (1 Bit).

\subsubsection{RS-Flip-Flop}

Eine einfache Flip-Flop-Schaltung ist der RS-Flip-Flop, wie in Abbildung~\ref{fig:rsflipflop} dargestellt. \glqq RS\grqq\ deshalb, weil $R$ für \texttt{reset} und $S$ für \texttt{set} steht. 

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,1) node[and port] (and1) {}
		(0,-1) node[and port] (and2) {}
		(and1.in 1) to[short,-o] ++(-1,0) node[left] {$\bar{S}$}
		(and2.in 2) to[short,-o] ++(-1,0) node[left] {$\bar{R}$}
		(and1.in 2) to[short] ++(0,-0.5) to[short] ($(and2.out)+(0,0.5)$) to[short] (and2.out)
		(and2.in 1) to[short] ++(0,0.5) to[short] ($(and1.out)+(0,-0.5)$) to[short] (and1.out)
		(and1.out) to[short,*-o] ++(1,0) node[right] {$Q$}
		(and2.out) to[short,*-o] ++(1,0) node[right] {$\bar{Q}$};
	\end{tikzpicture}
	\caption{Schaltbild eines RS-Flip-Flops}
	\label{fig:rsflipflop}
\end{figure}

Für $\bar{R} = \bar{S}$ nehmen die Ausgänge entweder die Zustände $1$ oder $0$ an. $\bar{Q}$ ist immer im inversen Zustand zu $Q$ gesetzt.
Setzt man $R$ oder $S$, so nimmt der Ausgang den entsprechenden Zustand an. Für $R=S=0$ wird der alte Zustand gespeichert. Für $S=R=1$ wird $Q=\bar{Q}=1$ gesetzt (was wenig sinnvoll ist). In Tabelle~\ref{tab:rsflipflop} sind alle Beschaltungen nochmals aufgeführt.

\begin{table}[htpb]
	\centering
	\begin{tabular}{ccc@{\quad}cc@{\quad}l}
		\toprule
		$\bar{R}$ & $\bar{S}$ & $Q_\mathrm{alt}$ & $Q$ & $\bar{Q}$ & Vorgang \cr
		\midrule
		$0$ & $1$ & $\times$ & $0$ & $1$ & set \cr
		$1$ & $0$ & $\times$ & $1$ & $0$ & reset \cr
		$1$ & $1$ & $1$      & $1$ & $0$ & speichern \cr
		$1$ & $1$ & $0$      & $0$ & $1$ & speichern \cr
		$0$ & $0$ & $\times$ & $1$ & $1$ & \emph{race condition} \cr
		\bottomrule
	\end{tabular}
	\caption{Zustandstabelle eines RS-Flip-Flops.}
	\label{tab:rsflipflop}
\end{table}

\subsubsection{D-Latch}

Eine Erweiterung des RS-Flip-Flops stellt der D-Latch dar. Durch die beiden vorgeschalteten NAND-Gatter kann die beim RS-Flip-Flop beschriebene \emph{race condition} verhindert werden. In Abbildung~\ref{fig:dlatch} ist ein Schaltbild eines D-Latch skizziert.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,1)       node[and port] (and11) {}
		let \p1 = (and11.in 1), \p2 = (and11.in 2), \n1 = {(\y1-\y2)/2} in
		++(-4,\n1)  node[and port] (and21) {}
		(0,-1)      node[and port] (and12) {}
		++(-2,-\n1) node[and port] (and22) {}

		(and11.in 1) to[short,l=$\bar{S}$] (and21.out)
		(and12.in 2) to[short,l=$\bar{R}$] (and22.out)

		(and11.in 2) to[short] ++(0,-0.5) to[short] ($(and12.out)+(0,0.5)$) to[short] (and12.out)
		(and12.in 1) to[short] ++(0,0.5) to[short] ($(and11.out)+(0,-0.5)$) to[short] (and11.out)
		(and11.out) to[short,*-o] ++(0.5,0) node[right] {$Q$}
		(and12.out) to[short,*-o] ++(0.5,0) node[right] {$\bar{Q}$}
		
		(and21.in 1) to[short,-o] ++(-0.5,0) coordinate[label={left:$D$}] (D)
		let \p1 = (D), \p2 = (and21.in 2), \p3 = (and22.in 2) in
		(\p3) to[short] (\x2,\y3) to[short,-o] (\x1,\y3) node[left] {$T$}
		(\p2) to[short,-*] (\x2,\y3)
		(and22.in 1) -| (and21.out) node[circ] {};
	\end{tikzpicture}
	\caption{Schaltbild eines D-Latch.}
	\label{fig:dlatch}
\end{figure}

\subsubsection{D- und T-Flip-Flop}

Meist ist es von Vorteil den Zustand in regelmäßigen Abständen zu speichern, anstatt ihn durch ein Eingangssignal zu triggern. Durch eine Flankensteuerung mit einem Taktsignal $T$ übernimmt der Flip-Flop den $D$-Wert nur, wenn sich $T$ in der absteigenden Flanke befindet ($D$-Flip-Flop).

Beim $T$-Flip-Flop hingegen schwankt $Q$ mit der aktiven Flanke zwischen $0$ und $1$ (\emph{toggle}). Ein Rückkopplung von $\bar{Q}$ auf $D$ bewirkt dies.

\subsubsection{JK-Flip-Flop}

Diese Art Flip-Flop wird im Versuch verwendet. Im Unterschied zu RS-Flip-Flops, die pegelgesteuert sind, sind JK-Flip-Flops flankengesteuert.

Der JK-Flip-Flop verfolgt das sogenannte \emph{Schleusenprinzip}. Die erste Gatterschicht schleust Zustände nur dann ein, wenn $T = 1$ steht. Die zweite Gatterschicht lässt nur für $T = 0$ Zustände passieren. Dadurch wird der darauf folgende RS-Flip-Flop wieder durch die Eingänge $\bar{R}$ und $\bar{S}$ gesteuert. Die Zustände sind nochmals in Tabelle~\ref{tab:jkflipflop} aufgelistet.

\begin{table}[htpb]
	\centering
	\begin{tabular}{ccc@{\qquad}cc@{\qquad}l@{\qquad}l}
		\toprule
		$J$ & $K$ & $Q_\mathrm{alt}$ & $R$ & $S$ & \multicolumn{1}{c}{$Q$}      & Funktion \cr
		\midrule
		$0$ & $0$ & $0$              & $0$ & $0$ & $Q_\mathrm{alt}$             & speichern \cr
		$0$ & $0$ & $1$              & $0$ & $0$ & $Q_\mathrm{alt}$             & speichern \cr
		$1$ & $0$ & $0$              & $0$ & $1$ & $1$                          & D-FF \cr
		$1$ & $0$ & $1$              & $0$ & $0$ & $1$ ($Q_\mathrm{alt}$)       & D-FF \cr
		$0$ & $1$ & $0$              & $0$ & $0$ & $0$ ($Q_\mathrm{alt}$)       & D-FF \cr
		$0$ & $1$ & $1$              & $1$ & $0$ & $0$                          & D-FF \cr
		$1$ & $1$ & $0$              & $0$ & $1$ & $1$ ($\bar{Q}_\mathrm{alt}$) & T-FF \cr
		$1$ & $1$ & $1$              & $1$ & $0$ & $0$ ($\bar{Q}_\mathrm{alt}$) & T-FF \cr
		\bottomrule
	\end{tabular}
	\caption{Zustandstabelle eines JK-Flip-Flops.}
	\label{tab:jkflipflop}
\end{table}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,1) node[and port] (and11) {}
		let \p1 = (and11.in 1), \p2 = (and11.in 2), \n1 = {(\y1-\y2)/2} in
		++(-1.8,\n1)  node[and port] (and21) {}
		++(-3,\n1)  node[and port] (and31) {}
		(0,-1)      node[and port] (and12) {}
		++(-1.8,-\n1) node[and port] (and22) {}
		++(-3,-\n1) node[and port] (and32) {}

		(and11.in 1) to[short] (and21.out)
		(and12.in 2) to[short] (and22.out)
		(and21.in 1) to[\circuitikzbasekey/bipoles/length=1.3cm,generic,l=$\tau$] (and31.out)
		(and22.in 2) to[\circuitikzbasekey/bipoles/length=1.3cm,generic,l=$\tau$] (and32.out)

		(and11.in 2) to[short] ++(0,-0.5) to[short] ($(and12.out)+(0,0.5)$) to[short] (and12.out)
		(and12.in 1) to[short] ++(0,0.5) to[short] ($(and11.out)+(0,-0.5)$) to[short] (and11.out)
		(and11.out) to[short,*-] ++(0.2,0) coordinate (Q) to[short,*-o] ++(0.4,0) node[right] {$Q$}
		(and12.out) to[short,*-] ++(0.4,0) coordinate (Qbar) to[short,*-o] ++(0.2,0) node[right] {$\bar{Q}$}

		let \p1 = (and21.in 2) in
		coordinate (h) at (\x1,0)
		(and21.in 2) to[short,-*] (h)
		(and22.in 1) to[short,-*] (h)
		(h)+(-0.5,0) node[not port] (not) {}
		(not.out) to[short] (h)

		let \p1 = (and31.in 2) in
		coordinate (h) at (\x1,0)
		(and31.in 2) to[short,-*] (h)
		(and32.in 1) to[short,-*] (h)
		(not.in) to[short,-*] (h)

		(and31.in 1)+(0.4,-\n1) to[short,-o] ++(-0.3,-\n1) coordinate[label={left:{$J$}}] (J)
		(and32.in 1)+(0.4,-\n1) to[short,-o] ++(-0.3,-\n1) coordinate[label={left:{$K$}}] (K)

		let \p1 = (J) in
		(h) to[short,-o] (\x1,0) node[left] {$T$}

		(and31.in 1) -- ++(0,0.5) -| (Qbar)
		(and32.in 2) -- ++(0,-0.5) -| (Q)

		let \p1 = ($(and32.out)!.5!(and32.in 1)$),
		    \p2 = ($(and22.out)!.5!(and22.in 1)$),
		    \p3 = ($(and11.out)!.5!(and11.in 1)$),
		    \n1 = {\x1-(\x1-\x2)/2}
		in
		(\x1,-3) node[align=center,text width=1.5cm] {Gatter\-schicht 1}
		(\n1,-3) node[align=center,text width=1.5cm] {Lauf\-zeit}
		(\x2,-3) node[align=center,text width=1.5cm] {Gatter\-schicht 2}
		(\x3,-3) node[align=center,text width=2cm] {RS-Flip-Flop};
	\end{tikzpicture}
	\caption{Schaltbild eines JK-Flip-Flops}
	\label{fig:jkflipflop}
\end{figure}

\subsection{Zähler}

Aus Flip-Flops lassen sich Schaltungen bauen, die die Eingangsimpulse binär summieren. Diese sind meist aus T- oder JK-Flip-Flops aufgebaut.

\subsubsection{Asynchroner Binärzähler}

Ein \emph{Asynchronzähler} besteht aus in Reihe geschalteten T-Flip-Flops. Bei jedem der $N$ Flip-Flops verzögert sich das Signal um das $n$-fache. Daher liegen an den Ausgängen gerade die Bits einer Dualzahl an. Ein schematischer Aufbau ist in Abbildung~\ref{fig:async} dargestellt.

\begin{figure}[htpb]
	\centering
	\def\TFF(#1)#2#3{%
		\begin{scope}[shift={(#1)}]
			\draw (0,0) rectangle (1,1);
			\draw (0.5,1) -- (0.5,0);
			\draw (0.5,0.5) -- (1,0.5);
			\node at (0.25,0.5) {$T$};
			\node at (0.75,0.75) {#3};
			\coordinate (#2 out) at (1,0.75);
			\coordinate (#2 in) at (0,0.5);
		\end{scope}
	}
	\begin{tikzpicture}[every path/.style={}]
		\TFF(0,0){a}{$Q_0$}
		\TFF(1.5,0){b}{$Q_1$}
		\TFF(3,0){c}{$Q_2$}
		\TFF(4.5,0){d}{$Q_3$}
		%
		\coordinate (ab mid) at ($(a out)!0.5!(b in |- a out)$);
		\coordinate (bc mid) at ($(b out)!0.5!(c in |- b out)$);
		\coordinate (cd mid) at ($(c out)!0.5!(d in |- c out)$);
		%
		\draw (a in)+(-0.25,0) node[left] {$E$} to[short,o-] (a in);
		%
		\draw (a out) to[short,-*] (ab mid) |- (b in);
		\draw (ab mid) to[short,-o] ++(0,1) node[above] {Bit 0};
		\draw (b out) to[short,-*] (bc mid) |- (c in);
		\draw (bc mid) to[short,-o] ++(0,1) node[above] {Bit 1};
		\draw (c out) to[short,-*] (cd mid) |- (d in);
		\draw (cd mid) to[short,-o] ++(0,1) node[above] {Bit 2};
		\draw (d out) to[short,-*] ++(0.25,0) to[short,-o] ++(0,1) node[above] {Bit 3};
	\end{tikzpicture}
	\caption{Aufbau eines Asynchronzählers.}
	\label{fig:async}
\end{figure}

Durch die endliche Laufzeit des Signals durch die Schaltung zeigt der Asynchronzähler für die Zeitspanne $N \cdot \tau$ die falsche Binärzahl an, wobei $\tau$ die Laufzeit pro Gatter und $N$ die Anzahl der Gatter darstellt. Mit zunehmender Taktfrequenz wächst also die Anzahl der falschen Zählcodes (prozentual). Der Zähler ist also für hohe Taktfrequenzen unbrauchbar.

\subsubsection{Synchroner Bitzähler}

Für hohe Taktfrequenzen eignet sich der in Abbildung~\ref{fig:sync} dargestellte Synchronzähler besser. Die einzelnen JK-Flip-Flops sind synchron mit dem Eingangssignal getaktet.

\begin{figure}[htpb]
	\centering
	\def\JKFF(#1)#2#3{%
		\begin{scope}[shift={(#1)}]
			\draw (0,0) rectangle (1,1);
			\draw (0.5,1) -- (0.5,0);
			\draw (0.5,0.5) -- (1,0.5);
			\node at (0.75,0.75) {$Q$};
			\draw (1,0.8) -- +(0.2,0) coordinate (#2 Q);
			\draw (1,0.2) coordinate (#2 barQ);
			\draw (0,0.2) node[right] {$K$} -- +(-0.2,0) coordinate (#2 K);
			\draw (0,0.5) node[right] {$T$} -- +(-0.2,0) coordinate (#2 T);
			\draw (0,0.8) node[right] {$J$} -- +(-0.2,0) coordinate (#2 J);
		\end{scope}
	}
	\begin{tikzpicture}[every path/.style={},>=triangle 45]
		\JKFF(0,0){a}{$Q_0$}
		\JKFF(2,0){b}{$Q_1$}
		\JKFF(4,0){c}{$Q_2$}
		\JKFF(6,0){d}{$Q_3$}
		%
		\draw (a K) to[short,-*] (a J);
		\draw (b K) to[short,-*] (b J);
		\draw (c K) to[short,-*] (c J);
		\draw (d K) to[short,-*] (d J);
		%
		\draw (-0.7,-1) node[ocirc,label={left:$E$}] (E) {};
		\draw (a T) -- ++(-0.2,0) coordinate (inter) -| (E -| inter) node[circ] {};
		\draw (b T) -- ++(-0.2,0) coordinate (inter) -| (E -| inter) node[circ] {};
		\draw (c T) -- ++(-0.2,0) coordinate (inter) -| (E -| inter) node[circ] {};
		\draw (d T) -- ++(-0.2,0) coordinate (inter) -| (E -| inter) node[circ] {} -- (E);
		%
		\draw[->] (a J) -- ++(0,1) node[left] {$+$};
		\draw (a Q) to[short] ++(0,2) node[ocirc,label={left:Bit 0}] (bit0) {};
		\draw (b Q) to[short] ++(0,2) node[ocirc,label={left:Bit 1}] (bit1) {};
		\draw (c Q) to[short] ++(0,2) node[ocirc,label={left:Bit 2}] (bit2) {};
		\draw (d Q) to[short] ++(0,2) node[ocirc,label={left:Bit 3}] (bit3) {};
		%
		\draw (c J) -- ++(0,0.5) -| ++(1,0.5) -- ++(-0.1,0) node[scale=0.5,and port] (c and) {};
		\draw (d J) -- ++(0,0.5) -| ++(1,1) -- ++(-0.1,0) node[scale=0.5,and port] (d and) {};
		%
		\draw (b J) to[short,-*] (a Q);
		\draw (c and.in 2) to[short,-*] (c and.in 2 -| bit1);
		\draw (c and.in 1) to[short,-*] (c and.in 1 -| bit0);
		\draw (d and.in 2) to[short,-*] (d and.in 2 -| bit2);
		\draw ($(d and.in 2)!0.5!(d and.in 1)+(0.2,0)$) coordinate (help) to[short,-*] (help -| bit1);
		\draw (d and.in 1) to[short,-*] (d and.in 1 -| bit0);
	\end{tikzpicture}
	\caption{Aufbau eines 4-Bit Synchronzählers mit JK-Flip-Flops.}
	\label{fig:sync}
\end{figure}

Erinnern wir uns an Tabelle~\ref{tab:jkflipflop}. Dort wird gezeigt, dass der JK-Flip-Flop für $J=K=0$ seinen Wert hält und für $J=K=1$ invertiert. Durch die \texttt{AND}-Gatter werden also alle Zählerstufen solange gehalten, bis sie alle auf $1$ stehen. Es können nun Impuls gezählt werden, wie mit dem Asynchronzähler, mit dem Unterschied, dass sich keine falschen Zwischenzustände einstellen.

\subsubsection{Schieberegister}

Aus einer Kaskade an D-Flip-Flops lässt sich ein Schieberegister realisieren. In Abbildung~\ref{fig:schieberegister} sind die JK-Flip-Flops so verschalten, dass die Bedingung $\bar{J} = K$ erfüllt ist und sie als D-Flip-Flops arbeiten.

Bei aktiver Taktflanke übernimmt der nachfolgende Flip-Flop immer den Zustand des vorangehenden, der erste nimmt den Eingangszustand an. Solche Schieberegister arbeiten als FIFO-Speicher (first in, first out), da das erste Bit, das am Anfang eingegeben wird auch am Ende als erstes \glqq herunterfällt\grqq.

\begin{figure}[htpb]
	\centering
	\def\JKFF(#1)#2#3{%
		\begin{scope}[shift={(#1)}]
			\draw (0,0) rectangle (1,1);
			\draw (0.5,1) -- (0.5,0);
			\draw (0.5,0.5) -- (1,0.5);
			\node at (0.75,0.75) {$Q$};
			\node at (0.75,0.25) {$\bar{Q}$};
			\draw (1,0.8) -- +(0.2,0) coordinate (#2 Q);
			\draw (1,0.2) -- +(0.2,0) coordinate (#2 barQ);
			\draw (0,0.2) node[right] {$K$} -- +(-0.2,0) coordinate (#2 K);
			\draw (0,0.5) node[right] {$T$} -- +(-0.2,0) coordinate (#2 T);
			\draw (0,0.8) node[right] {$J$} -- +(-0.2,0) coordinate (#2 J);
		\end{scope}
	}
	\begin{tikzpicture}[every path/.style={},>=triangle 45]
		\JKFF(0,0){a}{$Q_0$}
		\JKFF(1.5,0){b}{$Q_1$}
		\JKFF(3,0){c}{$Q_2$}
		\JKFF(4.5,0){d}{$Q_3$}
		%
		\draw (-2.5,-0.5) node[ocirc,label={left:$T$}] (T) {};
		\draw (-2.5,0.8) node[ocirc,label={left:$E$}] (E) {};
		\draw (E) -- (a J);
		\draw (a T) -- (T -| a T) node[circ] {};
		\draw (b T) -- (T -| b T) node[circ] {};
		\draw (c T) -- (T -| c T) node[circ] {};
		\draw (d T) -- (T -| d T) node[circ] {} -- (T);
		%
		\draw (a Q) -- (b J);
		\draw (b Q) -- (c J);
		\draw (c Q) -- (d J);
		\draw (a barQ) -- (b K);
		\draw (b barQ) -- (c K);
		\draw (c barQ) -- (d K);
		%
		\draw (a K) -- ++(-0.2,0) node[not port] (not) {};
		\draw (not.in) to[short,-*] (not.in |- E);
	\end{tikzpicture}
	\caption{Schieberegister}
	\label{fig:schieberegister}
\end{figure}

Zu den wichtigsten Anwendungen des Schieberegisters gehören:
%
\begin{description}
	\item[Buffer] Verbindet man Ein- und Ausgang miteinander, so können so viele Bits, wie JK-Flip-Flops vorhanden sind gespeichert werden. Trennt man die Verbindung wieder, so können sie ausgelesen werden.
	\item[Serialisierung und Parallelisierung von Bitmustern]
		Möchte man parallele Bitmuster serialisieren, so muss jedes Flip-Flop über einen Lade-Eingang verfügen, um das Bitmuster seriell zu laden.
		Zur Parallelisierung von seriellen Bitmustern muss jedes Flip-Flop eine lesbaren Ausgang besitzen, damit zur Zeit $\tau$ alle Flip-Flops des Schieberegisters gleichzeitig ausgelesen werden können.
	\item[Bitshift in Maschinensprachen] Maschinensprachen wie \emph{Assembler} verfügen über einen Shift-Befehl auf den Registern. Dadurch wird es schneller Integer zu multiplizieren, indem man beide Register shiftet und dann addiert.
	\item[Erzeugung von Pseudozufallszahlen] Mit Hilfe von linear rückgekoppelten Schieberegistern können Pseudozufallszahlen erzeugt werden. Dazu rückkoppelt man zwei Ausgänge über ein \texttt{XOR}-Gatter. So lassen sich Zyklen zufälliger Länge erzeugen, die die Länge eines einfacher Zyklus weit überragen. Die Zyklenlänge $T$ muss jedoch kleiner als $2^\textnormal{\#JK-FF}$ bleiben, denn danach wiederholt sich die Zahlenfolge.
\end{description}

\section{Versuchsaufbau und -durchführung}

\subsection{JK-Flip-Flop}

Ziel des ersten Versuchsteils ist eine Funktionstabelle des JK-Flip-Flops \texttt{74LS107} zu ermitteln. Dieser wird nach Abbildung~\ref{fig:aufbau-jkff} aufgebaut.
Die Eingänge \texttt{J}, \texttt{T}, \texttt{K} und \texttt{Clear} werden mit dem Tastermodul, der Ausgang mit dem LED-Modul verbunden.
Alle acht Kombinationen an Eingangszuständen werden durchgegangen und die zugehörigen Ausgangszustände notiert, um so eine Funktionstabelle zu erhalten.
Durch Halten und Loslassen des Taster kann zudem ermittelt werden, ob die Zustandsänderung beim Drücken oder Loslassen des Tasters erfolgt.
Um das Flip-Flop betriebsbereit zu machen muss der \texttt{Clear} Eingang auf $V_\textnormal{CC}$ geschlossen werden, um es zurückzusetzen.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=\linewidth]{Anleitung/JK-FF}
	\caption{Schaltbild zum Versuchsteil Funktionsweise eines JK-Flip-Flops, aus \cite[S.~1]{EP:Anleitung}.}
	\label{fig:aufbau-jkff}
\end{figure}

\subsection{Zählerschaltungen}

Zunächst wird ein asynchroner 4-Bit-Zähler nach der Schaltung in Abbildung~\ref{fig:aufbau-async} aufgebaut. Für die JK-Flip-Flops werden die ICs \texttt{74LS107} und \texttt{74107} verwendet. Am Ausgang werden wechselweise das LED-Modul oder die 7-Segment-Anzeige angeschlossen. Am $T$-Eingang wird mit einem Taster von Hand der Takt vorgegeben.

Nach Überprüfung auf korrekte Funktionsweise wird der \texttt{T}-Eingang mit dem \texttt{Sync}-Ausgang des Funktionsgenerators verbunden und eine Ausgangsfrequenz von \SI{8}{\mega\Hz} eingestellt. Mit Hilfe des Oszilloskops werden die Signale am Takteingang \texttt{T} und an den Flip-Flop-Ausgängen \texttt{Q$_\text{A}$} bis \texttt{Q$_\text{D}$} aufgenommen. Das Oszilloskop wird mit dem Signal an Ausgang \texttt{D} getriggert.

Verbindet man die Ausgänge \texttt{B} und \texttt{D} mit den Eingängen eines NAND-Gatters und legt alle \texttt{Clear}-Eingänge auf den Ausgang des NAND-Gatters, so erhält man einen Modulo-10-Zähler.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=\linewidth]{Anleitung/Asynchronzaehler}
	\caption{Schaltbild eines asynchronen 4-Bit-Zählers, aus \cite[S.~2]{EP:Anleitung}.}
	\label{fig:aufbau-async}
\end{figure}

Mit dem IC \texttt{74191} wird nach dem Schaltbild in Abbildung~\ref{fig:aufbau-sync} ein synchroner 4-Bit-Zähler aufgebaut. Der Takt am Eingang \texttt{T} wird wiederum durch den \texttt{Sync}-Ausgang des Signalgenerators vorgegeben. Der Signalverlauf an den Ein- und Ausgängen wird wieder mit dem Oszilloskop beobachtet. Dabei wird \texttt{CH1} entweder auf \texttt{T} oder auf \texttt{Q$_\text{A}$} gelegt und \texttt{CH2} nacheinander auf \texttt{Q$_\text{B}$}, \texttt{Q$_\text{C}$} und \texttt{Q$_\text{D}$}.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=\linewidth]{Anleitung/Synchronzaehler}
	\caption{Schaltbild eines synchronen 4-Bit-Zählers, aus \cite[S.~3]{EP:Anleitung}.}
	\label{fig:aufbau-sync}
\end{figure}

\subsection{Schieberegister}

Nach dem Schaltbild in Abbildung~\ref{fig:aufbau-schiebe} wird eine Schieberegister aus den zwei ICs \texttt{74107} aufgebaut, die je einen JK-Flip-Flop beinhalten.
Mit Hilfe eines TTL-NAND-Gatters wird \texttt{K$_\text{A}$} gegenüber \texttt{J$_\text{A}$} negiert.

Das Eingangssignal wird durch einen Taster vorgegeben. Schreibt man mit dem Taster eine $1$ ins erste Register, so wird dieser Zustand durch das Register geschoben. Schließt man nun den Ausgang des letzten Registers an den Eingang des ersten an und setzt den Taster wieder auf $0$, so erhält man ein zyklisches Register.

\begin{figure}[htpb]
	\centering
	\includegraphics[width=\linewidth]{Anleitung/Schieberegister}
	\caption{Schaltbild eines Schieberegisters, aus \cite[S.~4]{EP:Anleitung}.}
	\label{fig:aufbau-schiebe}
\end{figure}

\section{Auswertung}
\subsection{JK-Flipflop}
Für den aufgebauten JK-Flipflop ergibt sich genau die erwartete Funktionstabelle~\ref{tab:jkflipflop}, wie in dem Grundlagenteil zu sehen. Es wurde beobachtet, dass sich der Zustand bei der negativen Flanke des Taktsignals ändert.
Wenn $J=K=1$ ist arbeitet die Schaltung als T-Flipflop, eine solche Schaltung kann als Frequenzhalbierer verwendet werden. Außerdem kommt sie als Grundelement in asynchronen digitalen Zählern vor.

\subsection{Zählerschaltungen}
In Tabelle~\ref{tab:asynchroner Zaehler} sind die ersten 16 Taktschritte des asynchronen Zählers zu sehen. Nach dem 15ten Takt befindet sich der Zähler wieder im Anfangszustand mit der Dezimalzahl $0$. Mit einem weiteren angehängten JK-Flipflop könnte man bis 31 zählen, hat man nur 4 JK-Flipflops in der Schaltung beginnt man nach dem 15ten Takt an, von vorne zu zählen. 

\begin{table}
	\begin{tabular}{S@{\quad}SSSS@{\quad}S}
		\toprule
		{Takt} & {D} & {C} & {B} & {A} & {Dezimalzahl} \\
		\midrule
		0 & 0 & 0 & 0 & 0 & 0 \\ 
		1 & 0 & 0 & 0 & 1 & 1 \\ 
		2 & 0 & 0 & 1 & 0 & 2 \\ 
		3 & 0 & 0 & 1 & 1 & 3 \\ 
		4 & 0 & 1 & 0 & 0 & 4 \\ 
		5 & 0 & 1 & 0 & 1 & 5 \\ 
		6 & 0 & 1 & 1 & 0 & 6 \\ 
		7 & 0 & 1 & 1 & 1 & 7 \\ 
		8 & 1 & 0 & 0 & 0 & 8 \\ 
		9 & 1 & 0 & 0 & 1 & 9 \\ 
		10 & 1 & 0 & 1 & 0 & 10 \\ 
		11 & 1 & 0 & 1 & 1 & 11 \\ 
		12 & 1 & 1 & 0 & 0 & 12 \\ 
		13 & 1 & 1 & 0 & 1 & 13 \\ 
		14 & 1 & 1 & 1 & 0 & 14 \\ 
		15 & 1 & 1 & 1 & 1 & 15 \\ 
		16 & 0 & 0 & 0 & 0 & 0 \\ 
		\bottomrule
	\end{tabular}
	\caption{Messwerte des 4-stufigen Binärzählers.}
	\label{tab:asynchroner Zaehler}
\end{table}

Nun wird zur Takterzeugung der Sync-Ausgang des Funktionsgenerators mit einer Frequenz von $\nu = \SI{30}{\Hz}$ verwendet.
Am Oszilloskop wird nacheinander der Zustandsverlauf an den Ausgängen $Q_A,Q_B,Q_C,Q_D$ (siehe ABB.~\ref{fig:aufbau-sync}) gemessen, dieser Plot sieht für den synchronen und asynchronen Zähler gleich aus. Man kann in ABB.~\ref{fig:synchronzaehler} deutlich erkennen, wie die Frequenz nach jedem JK-Flipflop halbiert wird. Da die Frequenz nach dem letzten JK-Flipflop, also am Ausgang $Q_D$ am größten ist, eignet sich dieser am Besten als Trigger für das Oszilloskop. Denn triggert man mit höherer Frequenz, so werden Signale mit niedrigerer Frequenz nicht mehr sauber aufgelöst.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{Synchronzaehler.tex}
	\caption{Pulsverlauf des Synchronzählers bzw. Asynchtonzählers an den Ausgängen $Q_A$ bis $Q_D$ (vgl.\ Abbildung~\ref{fig:aufbau-sync}).}
	\label{fig:synchronzaehler}
\end{figure}

In ABB.\ref{fig:asynchronzaehler-sprung} ist der Spannungsverlauf der vier Ausgänge bei einer höheren Zeitauflösung und zusammen mit dem Taktsignal geplottet. Hier kann man erkennen, dass die JK-Flipflops bei der negativen Flanke des Taktsignals schalten. Aus den Messdaten geht eine Schaltverzögerung zwischen Taktsignal und Ausgang $Q_D$ von $\tau \approx \SI{88}{\nano\s}$ hervor. Die Schaltverzögerung pro Flipflop beträgt also $\frac{\tau}{4} \approx \SI{22}{\nano\s}$. Damit ergibt sich für diesen 4-stufigen Binärzähler eine Grenzfrequenz von $f_g = \frac{1}{\tau} \approx \SI{11.4}{\mega\Hz}$. Ab dieser Frequenz würde der erste Flipflop seinen Zustand ändern, bevor der vorherige Puls eine Änderung des letzten Flipflops bewirkt hätte. Somit würde der Zähler zu keinem Zeitpunkt den richtigen Wert anzeigen.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{Asynchronzaehler.tex}
	\caption{Pulsverlauf des Asynchronzählers an der abfallenden Flanke. Man erkennt klar die Zunahme der Laufzeit zwischen Eingangs- und Ausgangssignal mit stiegendem Bit.}
	\label{fig:asynchronzaehler-sprung}
\end{figure}

\subsubsection{Modulo-10-Zähler}
Einen dekadischen Zähler bekommt man, indem man anstatt die Versorgungsspannung das 1te und 3te Bit über ein NAND-Gatter mit der Clear-Leitung verbindet. Sobald das 1te und 3te Bit gesetzt sind, also die Dezimalzahl $10$ erreicht ist, werden die JK-Flipflops \glqq gecleart\grqq\ und der Zählvorgang startet von vorne.

\subsubsection{4-bit synchroner Dualzähler}
Es wird ein 4-bit-Dualzähler als fertiges Bauteil untersucht.
Im Gegensatz zum asynchronen Zähler werden beim synchronen Zähler alle JK-Flipflops gleichzeitig getriggert. In ABB.~\ref{fig:synchronzaehler} ist dessen Zustandsverlauf zu sehen, wie auch beim asynchronen Zähler wird die Frequenz nach jedem JK-Flipflop halbiert.
Dem Timing Diagramm des Datenblatts kann entnommen werden, dass der synchrone 4-bit-Dualzähler im Gegensatz zum asynchronen Zähler \emph{positiv} flankengetriggert ist.

\subsection{Schieberegister}
Die Verschiebung der $1$ beim Schieberegister entspricht der arithmetischen Operation \glqq mit $2$ multiplizieren\grqq\ bzw. \glqq durch $2$ teilen \grqq\ (bei umgekehrter Richtung). Dies gilt natürlich nur bis zum letzten Bit, da dieses seine Information nicht weitergeben kann und diese somit verloren geht.

Diesen Effekt kann man verhindern, indem man nach dem Einschreiben in das Register den Ausgang $Q_4$ mit dem Eingang verbindet. Dann werden die 4 gespeicherten Bits auf unbegrenzte Taktzahl durch das Schieberegister gegeben. In dieser Betriebsweise spricht man von einem zyklischen Schieberegister.

\section{Zusammenfassung}
\subsection{JK-Flipflop}
Es konnte die von der Theorie vorausgesagte Funktionstabelle gemessen werden. Bei $J = K = 1$ arbeitet das JK-Flipflop wie ein T-Flipflop. Für $J = K = 0$ wird der Zustand beibehalten. Außerdem wurde beobachtet, dass das JK-Flipflop \emph{negativ} flankengetriggert ist.

\subsection{Zählerschaltungen}
Der 4-stufige Binärzähler zeigt nach 15 Taktpulsen den Wert $0000$ an, da er die Zahl 16 (Binär: $10000$) nicht mehr anzeigen kann.
Als Laufzeit eines JK-Flipflops wurden $\SI{22}{\nano\s}$ gemessen. Damit gab sich als Grenzfrequenz $\nu_g \approx \SI{11.4}{\mega\Hz}$. Ab dieser Frequenz kann der Zähler zu keinem Zeitpunkt den richtigen Wert darstellen. Außerdem wurde festgestellt, dass nach jedem JK-Flipflop die Frequenz halbiert wird.

Durch den Einbau eines NAND-Gatters konnte der Modulo-16 Zähler in ein Modulo-10 Zähler umgebaut werden.

Der anschließend untersuchte synchrone 4-bit-Dualzähler ist im Gegensatz zum asynchronen Zähler \emph{positiv} flankengetriggert. Bei ihm tritt auch nach jedem Flipflop eine Frequenzhalbierung auf. Da hier jedoch keine Laufzeitverschiebung auftritt, ist dieser Zähler vor allem für größere Frequenzen geeignet.

\subsection{Schieberegister}
Auch das Schieberegister gehorchte der Theorie. Es konnte beobachtet werden, wie eine binäre 1 bei jedem Takt um eine Position weitergeschoben wird, bis sie bei 4. Takt aus dem Register fällt und damit gelöscht ist. Will man Informationen länger speichern, kann man den Ausgang mit dem Eingang des Schieberegisters verbinden, sodass die Bits für unbegrenzte Dauer zyklisch durch das Register geschoben werden.

\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
