#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.8
set pointsize 0.5
set output 'Asynchronzaehler.tex'

set xlabel 'Zeit $t$ $[\si{\micro\s}]$'
set ylabel 'Zustand $\Gamma$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25 width 7
set grid linetype 3 linecolor 0
set fit errorvariables

shift = 6
set xrange [0:0.4]
set xtics 0.1
set yrange [-2:4.5+3*shift]

set ytics ( \
	"0" 0.2+0*shift, \
	"1" 3.5+0*shift, \
	"0" 0.2+1*shift, \
	"1" 3.5+1*shift, \
	"0" 0.2+2*shift, \
	"1" 3.5+2*shift, \
	"0" 0.2+3*shift, \
	"1" 3.5+3*shift  \
)
set label "Bit 0" at graph 0.80,first 1.8+0*shift
set label "Bit 1" at graph 0.80,first 1.8+1*shift
set label "Bit 2" at graph 0.80,first 1.8+2*shift
set label "Bit 3" at graph 0.80,first 1.8+3*shift

plot \
'Asynchronzaehler-30Hz-bit0-Sprung.txt' using ($1*1e6-1.1):($4+0*shift) with lines linetype 1 linecolor 1 title '$U_E \equiv U[\mathrm{CH1}]$', \
'Asynchronzaehler-30Hz-bit0-Sprung.txt' using ($1*1e6-1.1-0.05):($5+0*shift) with lines linetype 1 linecolor 3 title '$U_A \equiv U[\mathrm{CH2}]$', \
'Asynchronzaehler-30Hz-bit1-Sprung.txt' using ($1*1e6-1.1-0.05):($4+1*shift) with lines linetype 1 linecolor 1 notitle 'Bit 1', \
'Asynchronzaehler-30Hz-bit1-Sprung.txt' using ($1*1e6-1.1-0.05):($5+1*shift) with lines linetype 1 linecolor 3 notitle 'Bit 1', \
'Asynchronzaehler-30Hz-bit2-Sprung.txt' using ($1*1e6-1.1):($4+2*shift) with lines linetype 1 linecolor 1 notitle 'Bit 2', \
'Asynchronzaehler-30Hz-bit2-Sprung.txt' using ($1*1e6-1.1):($5+2*shift) with lines linetype 1 linecolor 3 notitle 'Bit 2', \
'Asynchronzaehler-30Hz-bit3-Sprung.txt' using ($1*1e6-1.1):($4+3*shift) with lines linetype 1 linecolor 1 notitle 'Bit 3', \
'Asynchronzaehler-30Hz-bit3-Sprung.txt' using ($1*1e6-1.1):($5+3*shift) with lines linetype 1 linecolor 3 notitle 'Bit 3'
