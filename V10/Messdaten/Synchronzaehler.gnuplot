#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.7
set pointsize 0.5
set output 'Synchronzaehler.tex'

set xlabel 'Zeit $t$ $[\si{\s}]$'
set ylabel 'Zustand $\Gamma$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25 width 2
set grid linetype 3 linecolor 0
set fit errorvariables

shift = 5.5
set xtics 0.1
set yrange [-0.7:5.0+3*shift]

set ytics ( \
	"0" 0.2+0*shift, \
	"1" 4.0+0*shift, \
	"0" 0.2+1*shift, \
	"1" 4.0+1*shift, \
	"0" 0.2+2*shift, \
	"1" 4.0+2*shift, \
	"0" 0.2+3*shift, \
	"1" 4.0+3*shift  \
)

plot \
'Synchronzähler-30Hz-QB.txt' using ($1):($5+0*shift) with lines linetype 1 linecolor 3 title '$Q_A$', \
'Synchronzähler-30Hz-QB.txt' using ($1):($4+1*shift) with lines linetype 1 linecolor 1 title '$Q_B$', \
'Synchronzähler-30Hz-QC.txt' using ($1):($4+2*shift) with lines linetype 2 linecolor 1 title '$Q_C$', \
'Synchronzähler-30Hz-QD.txt' using ($1):($4+3*shift) with lines linetype 3 linecolor 1 title '$Q_C$'
