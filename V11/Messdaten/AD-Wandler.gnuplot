#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'AD-Wandler.tex'

set xlabel 'Zeit $t$ $[\mathrm{a.u.}]$'
set ylabel 'Zustand $\gamma$ $[1]$'
set format '\num[detect-all]{%g}'

unset key
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange [0:16]
set xtics 1
set yrange [2*0-1:2*6+1.5]
set ytics ( \
	'$Q_D$' 2*6, \
	'$Q_C$' 2*5, \
	'$Q_B$' 2*4, \
	'$Q_A$' 2*3, \
	'$T^*$' 2*2, \
	'$C$'   2*1, \
	'$T$'   2*0  \
)


plot \
'AD-Wandler.dat' using 1:($2+2*6) with lines linetype 1 linecolor 3 notitle, \
'AD-Wandler.dat' using 1:($3+2*5) with lines linetype 1 linecolor 3 notitle, \
'AD-Wandler.dat' using 1:($4+2*4) with lines linetype 1 linecolor 3 notitle, \
'AD-Wandler.dat' using 1:($5+2*3) with lines linetype 1 linecolor 3 notitle, \
'AD-Wandler.dat' using 1:($6+2*2) with lines linetype 1 linecolor 3 notitle, \
'AD-Wandler.dat' using 1:($7+2*1) with lines linetype 1 linecolor 3 notitle, \
'AD-Wandler.dat' using 1:($8+2*0) with lines linetype 1 linecolor 3 notitle
