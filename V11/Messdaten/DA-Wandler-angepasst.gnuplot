#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.8
set pointsize 0.5
set output 'DA-Wandler-angepasst.tex'

set xlabel 'Zustand $\gamma$ $[1]$'
set ylabel 'Spannung $U_A$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25 width 0
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange [0:16]
set yrange [:0]

load 'fitparameter.dat'
f(x) = b * x
fit f(x) 'Messungen.txt' using 1:4 via b
update 'fitparameter.dat'

plot \
'Messungen.txt' using 1:4 with points pointtype 7 linecolor 3 title 'Ausgangsspannung $U_A$', \
f(x) with lines linetype 2 linecolor 3 title 'Fit $\mathcal{F}_b(\gamma)$', \
-0.5*x with lines linetype 3 linecolor 1 title '$f(x) = \num{-0.5} x$'
