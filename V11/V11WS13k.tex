\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}
\newcommand\DA{D\kern-0.1ex/\kern-0.1ex A}
\newcommand\AD{A/D}

\title{Versuch V11: \DA\ und \AD-Wandler}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Boris Bonev}\noaffiliation
\date{20.\ Januar 2014}

\begin{abstract}
	Im Versuch sollen der Aufbau und die Funktionsweise einfacher Digital\slash Analog- und Analog\slash Digital-Wandler untersucht werden.
	Es wird die Ausgangsfunktion eines \DA-Wandler aufgenommen und dessen Linearität angepasst.
	Weiterhin wird die Funktionsweise eines \AD-Wandlers bestimmt, der auf Basis des \DA-Wandlers aufgebaut ist.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Operationsverstärker}

Um die Funktionsweise eines \DA-Wandler besser zu verstehen, lohnt es sich noch einmal den Aufbau wichtiger Operationsverstärker-Schaltungen zu betrachten.

\subsubsection{Invertierender Operationsverstärker}

Die einfachste Anwendung des Operationsverstärkers ist der Umkehrverstärker oder invertierender Operationsverstärker. Eine Schaltskizze ist in Abbildung~\ref{fig:invopamp} zu sehen. Für den Eingangswiderstand $R_1$ gilt
%
\begin{align*}
	R_1 = \frac{U_E}{I_E}
\end{align*}
%
Für die Ausgangsspannungen gilt wegen der Idealität des Operationsverstärkers und der Beschaltung mit Gegenkopplung
%
\begin{align*}
	U_A = - U_E \frac{R_N}{R_1}
\end{align*}

Die Verstärkung beträgt somit
%
\begin{align*}
	V = - \frac{R_N}{R_1}
\end{align*}

Dabei stellt $R_1$ einen Spannungs-Strom-Wandler und die restliche Schaltung eine Strom-Spannungs-Wandler dar.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-1) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E$] (lu) to[R=$R_1$,o-*] (opamp.-)
		(ground)+(-2,0) coordinate (ld) to[short,o-*] (ground)
		(ground)+(3.5,0) coordinate (rd) to[short,o-] (ground)
		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.out)+(0,1.5) to[R,l_=$R_N$] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		let \p1 = (rd), \p2 = (opamp.out) in
		(opamp.out) to[short,*-o] (\x1,\y2) coordinate[label=right:$A$] (ru);
		\draw[->] ($(lu)+(0,-0.2)$) -- node[right] {$U_E$} ($(ld)+(0,0.2)$);
		\draw[->] ($(ru)+(0,-0.2)$) -- node[right] {$U_A$} ($(rd)+(0,0.2)$);
	\end{tikzpicture}
	\caption{Schaltbild eines invertierenden Operationsverstärkers.}
	\label{fig:invopamp}
\end{figure}

\subsubsection{Der Summationsverstärker}

Legt man an den invertierenden Eingang eine zusätzliche Eingangsspannung an, so ergibt sich der Umkehraddierer oder auch Summationsverstärker, wie er in Abbildung~\ref{fig:addopamp} abgebildet ist.

Für die Ausgangsspannung gilt hier
%
\begin{align}
	U_A = - R_N \sum\limits_{i} \frac{U_{E_i}}{R_i}
\end{align}

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-0.5) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E_2$] (lu) to[R,l_=$R_2$,o-*] (opamp.-)
		coordinate (helper) at ($(opamp.-)+(0,1)$)
		(helper) to[short] (opamp.-)
		(helper)+(-2,0) coordinate[label=left:$E_1$] (lu) to[R=$R_1$,o-*] (helper)
		let \p1 = (opamp.out), \p2 = (helper) in
		(helper) to[R=$R_N$] (\x1,\y2) to[short,-*] (opamp.out)
		(opamp.out)+(1,0) node[right] {$A$} to[short,o-] (opamp.out);
	\end{tikzpicture}
	\caption{Schaltbild eines Operationsverstärkers als Summationsverstärker.}
	\label{fig:addopamp}
\end{figure}

\subsection{\DA-Wandler}

Wie der Name bereits suggeriert können mit einem Digital\slash Analog-Wandler digitale Eingangssignale in analoge Ausgangssignale umgewandelt werden.
So sollen am Eingang eingegebene Binärzahlen in entsprechende Spannungspegel am Ausgang transformiert werden.
Dabei nutzt man die Eigenschaft des Summationsverstärkers, um die verschiedenen Wertigkeiten der Bits verschieden zu gewichten.

Zur Gewichtung wird der Eingangswiderstand vom MSB (engl.: \textsl{most significant bit}) zum LSB (engl.: \textsl{least significant bit}) mit jedem Bit verdoppelt.

Der Gegenkopplungswiderstand $R_N$ wird so gewählt, dass der Verstärker den erwünschten Maximalstrom liefert, wenn alle Bits auf $1$ stehen.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\node[op amp] (opamp) at (0,0)  {};
		\draw
		(opamp.+) to[short] ++(0,-0.5) node[ground] {}
		(opamp.-) to[short,*-] ++(0,1) coordinate (helper) to[R=$R_N$] (opamp.out |- helper) to[short,-*] (opamp.out)
		(opamp.-) ++(0,3) node[ocirc,label={right:$U_\textnormal{ref}$}] (Uref) {}
		(Uref) ++(-0.5,0) coordinate (16R) to[R=$16R$,*-] ++(0,-2) to[cspst,-*] (16R |- opamp.-) node[below] {Bit 0}
		(16R) ++(-1.2,0) coordinate (8R) to[R=$8R$,*-] ++(0,-2) to[cspst,-*] (8R |- opamp.-) node[below] {Bit 1}
		(8R) ++(-1.2,0) coordinate (4R) to[R=$4R$,*-] ++(0,-2) to[cspst,-*] (4R |- opamp.-) node[below] {Bit 2}
		(4R) ++(-1.2,0) coordinate (2R) to[R=$2R$,*-] ++(0,-2) to[cspst,-*] (2R |- opamp.-) node[below] {Bit 3}
		(Uref) -- (2R)
		(opamp.-) -- (2R |- opamp.-)
		(opamp.out) to[short,-o] ++(0.5,0) node[above] {$A$}
		;
	\end{tikzpicture}
	\caption{Schaltbild eines 4-Bit-\DA-Wandlers.}
	\label{fig:DA-Wandler}
\end{figure}

Mit dem Ausdruck für die Übertragungsfunktion des invertierenden Verstärkers ergibt sich
\[ U_A = - \sum_i \frac{R_N}{R_i} U_\textnormal{ref} \]
wobei $R_N$ der Gegenkopplungswiderstand und $R_i$ der Widerstand an den gesetzten Bits ist.

\subsection{\AD-Wandler}

Ein \AD-Wandler lässt sich aufbauen aus einem \DA-Wandler und einem Komparator, siehe Schaltplan in Abbildung~\ref{fig:AD-Wandler}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[every path/.style={}]
		\begin{scope}[scale=1.5,shift={(-2,-1)}]
			\draw (0,0) rectangle (1,1);
			\draw (0,0) -- node[above left] {D} node[below right] {A} (1,1);
			\coordinate (DA-bit3) at (0,0.8);
			\coordinate (DA-bit2) at (0,0.6);
			\coordinate (DA-bit1) at (0,0.4);
			\coordinate (DA-bit0) at (0,0.2);
			\coordinate (DA-out) at (1,0.5);
		\end{scope}
		\begin{scope}[scale=1.5,shift={(-3.7,-1)}]
			\draw (0,0) rectangle (1,1);
			\node at (0.5,0.5) {Zähler};
			\coordinate (count-bit3) at (1,0.8);
			\coordinate (count-bit2) at (1,0.6);
			\coordinate (count-bit1) at (1,0.4);
			\coordinate (count-bit0) at (1,0.2);
			\coordinate (count-in1) at (0.25,0);
			\coordinate (count-in2) at (0.75,0);
		\end{scope}
		\node[op amp,yscale=-1] (opamp) at (0,0) {};
		\node[below=3em] at (opamp) {Komparator};
		\draw (opamp.+) to[short,-o] ++(-5,0) node[left] {$E$};
		\draw (DA-out) -| (opamp.-);
		\draw (count-bit3) -- node[above] {Bit 3} (DA-bit3);
		\draw (count-bit0) -- node[below] {Bit 0} (DA-bit0);
		\draw (count-bit2) -- (DA-bit2);
		\draw (count-bit1) -- (DA-bit1);
		\draw (count-in2) |- ++(2,-0.5) |- ++(-0.5,-0.7) node[and port] (and) {};
		\draw (count-in1) |- ++(-0.8,-0.4) node[ocirc,label={left:reset}] {};
		\draw (and.in 1) -| ++(-0.5,-0.2) rectangle ++(-1,0.4) node[pos=0.5,below=0.5em] {Oszillator};
		\draw (and.in 2) -- ++(0,-0.3) -| (opamp.out);
	\end{tikzpicture}
	\caption{Schaltbild eines generischen \AD-Wandlers.}
	\label{fig:AD-Wandler}
\end{figure}

Ein \AD-Wandler übersetzt analoge Eingangssignale in binäre Ausgabe. Der externe Takt des Oszillators wird an den Zähler weitergereicht, solange der Ausgang des Komparators auf $1$ steht. Am Komparatorausgang steht die $1$, wenn $U_E > U_\textnormal{\AD}$. Der Zähler wird also solange hochgezählt bis $U_E \leq U_\textnormal{\AD}$, weil dann eine $0$ am Komparatorausgang steht, die den Ausgang des AND-Gatters auch auf $0$ setzt. Der Zähler hält dann den zuletzt geschriebenen Zustand.

Der Nachteil dieser Art der \AD-Wandlung ist, dass für eine $N$-Bit-Zahl $2^N$ Zyklen abgewartet werden müssen.

\section{Versuchsaufbau und -durchführung}

\subsection{\DA-Wandler}

Nach Abbildung~\ref{fig:aufbau-DA} wird ein \DA-Wandler aufgebaut. Für den Operationsverstärker wird, der aus früheren Versuchen bekannte \texttt{OP177G} verwendet. Die Widerstande werden so dimensioniert, dass ein Ändern des LSB eine Spannungsänderung am Ausgang um \SI{0.5}{\V} erwirkt.

\begin{figure}[tpbh]
	\centering
	\includegraphics[width=\linewidth]{Anleitung/DA-Wandler}
	\caption{Schaltbild des 4-Bit-\DA-Wandlers.}
	\label{fig:aufbau-DA}
\end{figure}

Nun wird die Ausgangsspannung in Abhängigkeit des Eingangszustandes gemessen.

Als nächstes werden die Schalter in der Skizze durch der 4-Bit-Zähler \texttt{74191} ersetzt, der mit dem \texttt{Sync}-Ausgang des Frequenzgenerators getaktet wird. Der Verlauf der Ausgangsspannung wird mit dem Oszilloskop überwacht. Dabei werden Nullpunkt und Maximalwert der Spannung bestimmt. Durch Offsetkompensation am Operationsverstärker wird die Verschiebung des Nullpunkts korrigiert. Der Widerstand $R_5$ wird durch die Widerstandskaskade ersetzt und so variiert, dass der Zustand \texttt{1111} genau \SI{7.5}{\V} entspricht.

Zuletzt werden die Gewichtungswiderstände der Bits so variiert, dass der Fehler gegenüber der gewünschten Übertragungsfunktion auf \SI{2}{\percent} absinkt.

\subsection{\AD-Wandler}

Nach dem Schaltplan in Abbildung~\ref{fig:aufbau-AD} wird der \DA-Wandler zum \AD-Wandler erweitert.

\begin{figure}[tpbh]
	\centering
	\includegraphics[width=\linewidth]{Anleitung/AD-Wandler}
	\caption{Schaltbild des 4-Bit-\AD-Wandlers.}
	\label{fig:aufbau-AD}
\end{figure}

Der Takt wird wiederum durch den \texttt{Sync}-Ausgang des Frequenzgenerators vorgegeben. Die Ausgänge des 4-Bit-Zählers werden an die 7-Segment-Anzeige angeschlossen.

Der Zähler wird zurückgesetzt und das Netzgerät auf eine Spannung zwischen \SI{0}{\V} und \SI{7.5}{\V} eingestellt.

Nun wird die Funktionsweise des \AD-Wandlers qualitativ untersucht und die Ergebnisse dokumentiert.

\section{Auswertung}
\subsection{\DA-Wandler}
Damit sich die Ausgangsspanung um \SI{0.5}{\V} ändert, wenn niederwertigste Bit seinen Zustand ändert, werden die Widerstände aus der E12-Reihe wie folgt gewählt:

\begin{itemize}
	\item $R_1 = \SI{1.2}{\ohm}$
	\item $R_2 = \SI{2.2}{\ohm}$
	\item $R_3 = \SI{3.9}{\ohm}$
	\item $R_4 = \SI{8.2}{\ohm}$
\end{itemize}

In Tabelle~\ref{tab:DA-Wandler} sind die manuell ermittelten Messwerte des \DA-Wandlers zu sehen. Es ist erkennbar, dass bei der Aktivierung des dritten Bits, die Monotoniebedingung verletzt wird.

\begin{table}[h!]
	\begin{tabular}{SSSSSSS}
		\toprule
		{Zustand} & {Bit 3} & {Bit 2} & {Bit 1} & {Bit 0} & {$U_A$ $[\si{\volt}]$} & {$(U_n - U_{n+1})$ $[\si{\volt}]$} \\
		\midrule
		0         & 0       & 0       & 0       & 0       & 0.000                  & 0.503                              \\
		1         & 0       & 0       & 0       & 1       & -0.503                 & 0.560                              \\
		2         & 0       & 0       & 1       & 0       & -1.063                 & 0.503                              \\
		3         & 0       & 0       & 1       & 1       & -1.556                 & 0.314                              \\
		4         & 0       & 1       & 0       & 0       & -1.880                 & 0.502                              \\
		5         & 0       & 1       & 0       & 1       & -2.382                 & 0.560                              \\
		6         & 0       & 1       & 1       & 0       & -2.942                 & 0.503                              \\
		7         & 0       & 1       & 1       & 1       & -3.445                 & -0.003                             \\
		8         & 1       & 0       & 0       & 0       & -3.442                 & 0.502                              \\
		9         & 1       & 0       & 0       & 1       & -3.944                 & 0.559                              \\
		10        & 1       & 0       & 1       & 0       & -4.503                 & 0.502                              \\
		11        & 1       & 0       & 1       & 1       & -5.005                 & 0.311                              \\
		12        & 1       & 1       & 0       & 0       & -5.316                 & 0.502                              \\
		13        & 1       & 1       & 0       & 1       & -5.818                 & 0.557                              \\
		14        & 1       & 1       & 1       & 0       & -6.375                 & 0.502                              \\
		15        & 1       & 1       & 1       & 1       & -6.877                 & {---}                              \\ 
		\bottomrule
	\end{tabular}
	\caption{Ausgangsspannungen und Monotoniebedingung des \DA-Wandlers in Abhängigkeit des Zustandes.}
	\label{tab:DA-Wandler}
\end{table}


Die Nichtlinearität ergibt sich als
\begin{equation}
	\delta = \max \left( \left| U_i - U_{i+1} - \frac{U_{ref}}{2^N} \right|\right) = \num{-0.503}
\end{equation}


Damit ist $\delta \leq \frac{U_{ref}}{2^N}$ und somit ist die Monotoniebedingung nicht mehr erfüllt. 


In ABB.~\ref{fig:DA} sind die Ausganngsspannungen gegen die Zustände aufgetragen. Der Fit \[ \mathcal{F}_a(\gamma) = a \cdot x \] gibt eine Steigung von $a =  \num{-0.454}$ aus. Die Abweichung von der gewünshten Geraden $f$ mit der Steigung \num{-0.5} hat ihre Ursache in den nicht genau passenden Widerständen der E12-Reihe. 

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{DA-Wandler.tex}
	\caption{Treppenfunktion des \DA-Wandlers.}
	\label{fig:DA}
\end{figure}

Will man einen 12-bit Wandler konstruieren, müssen die Widerstände eine sehr hohe Genauigkeit aufweisen.
Die Toleranz muss unter \SI{0.03}{\percent} liegen.
Bei der Aktivierung des hochwertigsten Bits ist die Wahrscheinlichkeit für eine Verletzung der Monotoniebedingung am größten.

Nun wird der \DA-Wandler an die Ausgänge eines 4-bit Zählers angeschlossen. Der Zähler wird mit einer Rechteckspannung getaktet. In ABB.~\ref{fig:DA-angepasst} ist die Treppenfunktion des Wandlers zu sehen.

Der Nullpunkt liegt jetzt nicht mehr bei \SI{0}{\V}, sondern bei \SI{200}{\milli\V} und der Maximalwert anstatt bei \SI{-6.877}{\V} bei \SI{-4.48}{\V}. Der Grund dafür ist, dass der Zähler nicht exakt \SI{0}{\V} und \SI{5}{\V} ausgibt, sonder etwas höhere bzw. niedrigere Spannungen.
Der Nullpunkt kann durch einen entgegengesetzten Strom am Summationspunkt kompensiert werden. Hierzu wurde ein Strom von der negativen Versorgungsspannung \SI{-15}{\V} mit Hilfe einer Widerstandsdekade ($\ohm$ = \SI{55.4}{\kilo\ohm}) genutzt.
Dadurch verschiebt sich auch der Maximalwert. Um den alten Wert widerherzustellen wird zu dem Widerstand $R_5$ ein 1k-Potentiometers in Reihe geschalten. Dieser wird dazu auf \SI{46}{\ohm} gestellt.

In ABB.~\ref{fig:DA-angepasst} ist die Treppenfunktion des angepassten \DA-Wanlders zu sehen. Da der Maximalwert nicht auf die alte Spannung angepasst wurde%ich hatte doch Recht Henri :) -- Tatsache
, macht ein Vergleich der Steigungen keinen Sinn.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{DA-Wandler-angepasst.tex}
	\caption{Treppenfunktion des \emph{angepassten} \DA-Wandlers.}
	\label{fig:DA-angepasst}
\end{figure}

\subsection{\AD-Wandler}
Nachdem die Schaltung zu einem \AD-Wandler erweitert wurde, wird dessen Funktion überprüft und der Theorie entsprechend bestätigt.
Die Spannung am Analogeingang muss negativ sein, da ansonsten der Komparator immer eine 1 ausgibt und der Zähler niemals anhalten würde.
Die Wandlungszeit hängt von der Anzahl der verwendeten Bits ab und der Frequenz des Taktes. In dieser Wandlungsart werden bis zu $2^N$ Takte benötigt, da mit dem Verfahren der sukzessiven Approximation maximal N Takte benötgt werden, ist die Wandlungsart nicht zeitoptimal.
Die Genauigkeit könnte erhöht werden, indem man einen \DA-Wandler mit einer höheren Bitzahl verwendet. 
Ist der verwendete \DA-Wandler nicht-monoton, so kann es zu sogenannten \emph{missing codes} kommen. Diese treten auf, da der zur Nichtmonotonie gehörende Digitalcode nie als AD-Ergebnis auftreten kann.



ABB-~\ref{fig:AD} zeigt den Pulsfahrplan des \AD-Wandlers. C beschreibt den Spannungsverlauf am Ausgang des Komparators, $T$ entpricht dem externen Taktsignal und $T^*$ dem Takt des \DA-Wandlers nach dem NAND-Gatter. 
Nach dem 5. Takt behält der Komparator seinen Zustand, die Zustände $Q_{D-A}$ entsprechen der Duahlzahl 0101

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{AD-Wandler.tex}
	\caption{Pulsfolge des \AD-Wandlers.}
	\label{fig:AD}
\end{figure}

\section{Zusammenfassung}
Der mit Widerständen der E12-Reihe aufgebaute \DA-Wandler konnte die Monotoniebedingung nicht erfüllen, auch eine Anpassung der Widerstände mit einer Widerstandsdekade brachte keinen Erfolg.
Beim Anschluss an einen 4-bit Zähler kommt es zu einer Nullpunktverschiebung und der Maximalwert des \DA-Wandlers wird verändert.
Durch das Zuführen eines negativen Stroms an den Summationspunkt, konnte die Nullpunktsverschiebung korrigiert werden.
Der alter Maximalwert konnte durch eine Änderung des Rückkoppelwiderstandes wiederhergestellt werden.

Durch Erweiterung der Schaltung wurde eine \AD-Wandler hergestellt. Dessen Funktion wurde überprüft. Es wurde festgestellt, dass die Spannung am Analogeingang eine negative Polarität besitzen muss.
Ist der verwendete \DA-Wandler durch schlecht gewählte Widerstände nicht-monoton, kommt es zu sogenannten \emph{missing codes}.



\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
