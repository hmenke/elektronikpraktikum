#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output "10-w_0.tex"

set xlabel 'Zeit $t$ $[\si{\s}]$'
set ylabel 'Amplitude $A$ $[\si{\V}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 1 width 1
set grid linetype 3 linecolor 0

plot \
"10-w_0.lvm" using 1:4 with lines linetype 1 linecolor 1 title '$U_R$', \
"10-w_0.lvm" using 2:5 with lines linetype 1 linecolor 3 title '$U_C$'
