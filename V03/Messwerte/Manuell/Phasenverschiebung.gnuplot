#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output "Phasenverschiebung.tex"

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Phasenverschiebung $\Delta \phi$ $[1]$'
set format x '\num[detect-all]{e%L}'
set format y '\num[detect-all]{%g}'
set ytics 0.4

set key below maxrows 1 width 1
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale x

load 'fitparameter.dat'
f(x) = atan(2*pi*x/c)
fit f(x) 'ManuelleMessung.dat' u 1:(2*pi*$1*$6*1e-6) via c
update 'fitparameter.dat'

set arrow nohead from graph 0,first pi/4 to graph 1,first pi/4
set arrow nohead from first c/(2*pi),graph 0 to c/(2*pi),graph 1

plot \
'ManuelleMessung.dat' u 1:(2*pi*$1*$6*1e-6) with points linecolor 3 pointtype 5 title '$\Delta \phi$', \
f(x) with lines linecolor 1 title 'Fit $\mathcal{F}_{c}(\nu)$'
