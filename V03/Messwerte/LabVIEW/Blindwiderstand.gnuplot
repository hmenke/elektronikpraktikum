#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output "Blindwiderstand.tex"

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Blindwiderstand $X$ $[\si{\ohm}]$'
set format '\num[detect-all]{e%L}'

set key below maxrows 1 width 1
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale xy

R = 1e3 # 1 kΩ
load 'fitparameter.dat'
f(x) = 1/(2*pi*x*a)
a = 1e-7
fit f(x) 'LabVIEWMessung.dat' u 1:(R*$2/$3) via a
update 'fitparameter.dat'

plot \
'LabVIEWMessung.dat' u 1:(R*$2/$3) with points linecolor 1 pointtype 5 title '$X$', \
f(x) with lines linecolor 3 title 'Fit $\mathcal{F}_{a}(\nu)$'
