#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.3,2.3
set pointsize 0.5
set output "Spannungsverlauf.tex"

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Spannung $U_R$ bzw.\ $U_C$ $[\si{\V}]$'
set format '\num[detect-all]{e%L}'

set key below maxrows 1 width 1
set grid linetype 3 linecolor 0
set logscale xy

plot \
'LabVIEWMessung.dat' using 1:2 with points linecolor 1 pointtype 5 title '$U_R$', \
'LabVIEWMessung.dat' using 1:3 with points linecolor 3 pointtype 7 title '$U_C$'
