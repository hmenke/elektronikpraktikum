#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output "Amplitudengang.tex"

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Amplitudengang $U_C/U_R$ $[1]$'
set format '\num[detect-all]{e%L}'

set key below maxrows 1 width 1
set grid linetype 3 linecolor 0
set fit errorvariables
set logscale xy

load 'fitparameter.dat'
f(x) = 1/sqrt(1 + (2*pi*x/b)**2)
fit f(x) 'LabVIEWMessung.dat' u 1:($2/sqrt($2**2+$3**2)) via b
update 'fitparameter.dat'

plot \
'LabVIEWMessung.dat' u 1:($2/sqrt($2**2+$3**2)) with points linecolor 1 pointtype 5 title '$U_C/U_R$', \
f(x) with lines linecolor 3 title 'Fit $\mathcal{F}_{b}(\nu)$'
