\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V03: Passive Netzwerke}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Torsten Rendler}\noaffiliation
\date{4.\ November 2013}

\begin{abstract}
	Im Versuch soll in erster Linie der Frequenzgang eines Tiefpasses aufgenommen werden. Anhand dessen werden charakteristische Größen, wie Phasenverschiebung und Blindwiderstand ermittelt.
	 Der Tiefpassfilter wird mit Hilfe von LabVIEW untersucht. Zudem werden Integrier- und Differenzierverhalten überprüft.
	
	Außerdem soll die Messgenauigkeit von Multimetern untersucht werden. Dazu werden die Multimeter parallel an einen Frequenzgenerator geschaltet und verschiedene Frequenzbereiche durchfahren.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

Als \emph{Wechselspannung} bezeichnet man eine Spannung, die im zeitlichen Verlauf ihre Polarität, periodisch, ändert. Wechselspannung lässt sich durch einige Größen beschreiben. Im Alltag begegnet man dem Sinusverlauf in Form der \SI{50}{\Hz} Netzspannung. Dieser ist durch eine Frequenz $\nu$ charakterisiert.
Die Amplitude der Wechselspannung ist die betragsmäßige Maximalspannung, die während einer Periode erreicht wird und wird meist mit einem Zirkumflex gekennzeichnet~($\hat{U}$). Teilweise ist mit der Amplitude jedoch auch der Peak-to-peak-Abstand, also der Abstand zwischen dem Maximum und dem Minimum einer Periode, gemeint.
Eine weitere zu nennende Größe ist der Effektivwert der Spannung, der mit dem Index $U_\mathrm{eff}$ gekennzeichnet ist. Dieser Wert entspricht der über eine Periode gemittelten Spannung.

Da der Wechselstrom periodisch verläuft ist es wichtig eine \emph{Phase} zu betrachten. Um eine Größe mit einer Phase zu versehen bietet es sich an in die komplexe Ebene zu wechseln.
%
\begin{description}
	\item[Kondensator] Ein Kondensator hat für Gleichstrom einen (nahezu) unendlichen Widerstand, während für Wechselspannung gilt
		%
		\begin{align*}
			Z_{C} = \frac{1}{\mathrm{j} \omega C}
		\end{align*}
		%
		mit der imaginären Einheit $\mathrm{j}$, der Wechselstromfrequenz $\omega$ und der Kapazität des Kondensators $C$.
		Die Phasenverschiebung der Spannung zum Strom beträgt also \SI{-90}{\degree} oder $-\pi/2$; der Strom läuft der Spannung voraus.

	\item[Spule] Eine Spule bewirkt durch ihre Induktivität ebenfalls eine Phasenverschiebung. Hier gilt
		%
		\begin{align*}
			Z_{L} = \mathrm{j} \omega L
		\end{align*}
		%
		mit $\mathrm{j}$ und $\omega$ wie oben und der Induktivität der Spule $L$.
		Die Phasenverschiebung der Spannung zum Strom beträgt \SI{90}{\degree} oder $\pi/2$; der Strom läuft der Spannung hinterher.
\end{description}

\begin{figure}[htpb]
	\centering
	\subfloat[Zeiger- und Phasendiagramm für einen Kondensator.]{
		\centering
		\begin{tikzpicture}[scale=0.8]
			\draw[->] (-0.3,0) -- (pi+0.5,0) node[below] {$t$};
			\draw[->] (0,-1.1) -- (0,1.1);
			\draw[blue] plot[domain=0:pi+0.5,samples=100] (\x,{sin(deg(2*\x))});
			\draw[red] plot[domain=0:pi+0.5,samples=100] (\x,{0.8*sin(deg(2*(\x+pi/4)))});
			\begin{scope}[xshift=pi*0.5cm,yshift=-2cm]
				\draw (0,0) circle (0.8);
				\draw[blue,->] (0,0) -- node[above] {$U$} (0:0.8);
				\draw[red,->] (0,0) -- node[left] {$I$} (90:0.8);
			\end{scope}
		\end{tikzpicture}
	}%
	\subfloat[Zeiger- und Phasendiagramm für eine Spule.]{
		\centering
		\begin{tikzpicture}[scale=0.8]
			\draw[->] (-0.3,0) -- (pi+0.5,0) node[below] {$t$};
			\draw[->] (0,-1.1) -- (0,1.1);
			\draw[blue] plot[domain=0:pi+0.5,samples=100] (\x,{sin(deg(2*\x))});
			\draw[red] plot[domain=0:pi+0.5,samples=100] (\x,{0.8*sin(deg(2*(\x-pi/4)))});
			\begin{scope}[xshift=pi*0.5cm,yshift=-2cm]
				\draw (0,0) circle (0.8);
				\draw[blue,->] (0,0) -- node[above] {$U$} (0:0.8);
				\draw[red,->] (0,0) -- node[left] {$I$} (-90:0.8);
			\end{scope}
		\end{tikzpicture}	
	}

	\caption{(Color online) Zeiger- und Phasendiagramme.}
	\label{fig:basics zeiger}
\end{figure}

Im Zeigerdiagramm in Abbildung~\ref{fig:basics zeiger} kann das Verhalten leicht nachvollzogen werden.

Für komplexe Widerstände gelten dieselben Rechenregeln wie für reelle was Reihen- und Parallelschaltung betrifft.

Der Realteil eines komplexen Widerstandes wird als \emph{Wirkwiderstand}, der Imaginärteil als \emph{Blindwiderstand} bezeichnet. Addiert man die Beträge der beiden, so erhält man den \emph{Scheinwiderstand}.

Unterschiedliche Widerstände haben unterschiedliche Leistung zur Folge, weshalb man das Prinzip der Wirk-, Blind- und Scheingrößen darauf übertragen kann. So ergibt sich
%
\begin{align*}
	\text{Scheinleistung:} && S &= U_\mathrm{eff} \cdot I_\mathrm{eff}, && \\
	\text{Wirkleistung:} && P &= U \cdot I, && \\
	\text{Blindleistung:} && Q &= \sqrt{S^2 - P^2}. &&
\end{align*}
%
Wird eine sinusförmige Spannung angelegt, so verändern sich die obigen Größen dahingehend, dass
%
\begin{align*}
	\text{Wirkleistung:} && P &= U_\mathrm{eff} \cdot I_\mathrm{eff} \cdot \cos\varphi && \\
	\text{Blindleistung:} && Q &= U_\mathrm{eff} \cdot I_\mathrm{eff} \cdot \sin\varphi &&
\end{align*}
%
wobei $\varphi$ die durch die Bauteile hervorgerufene Phasenverschiebung ist.

Sogenannte \emph{Passfilter} können aus einer Reihenschaltung von Kondensatoren und Spulen bestehen. Eine einfache Konstruktion wäre ein kapazitiver Passfilter, der aus einem Widerstand und einem Kondensator aufgebaut ist. Die Eingangsspannung liegt an beiden Bauteilen an. Beim \emph{Hochpass} wird die Ausgangsspannung am Widerstand, beim \emph{Tiefpass} am Kondensator abgegriffen. Ein derartiger Filter besitzt die Grenzfrequenz
%
\begin{align*}
	\omega_0 = \frac{1}{RC}
\end{align*}
%
wobei $R$ der ohmsche Widerstand und $C$ die Kapazität des Kondensators sind.
Diese Grenzfrequenz gibt den Punkt an, an dem die Ausgangsspannung um \SI{-3}{\deci\bel} gegenüber der Eingangsspannung abgefallen ist. Überhalb dieser Grenzfrequenz wird mit jeder Frequenzverdopplung die Spannung um weitere \SI{-6}{\deci\bel} abgesenkt.

Zwischen der Spannung $U$ und dem Strom $I$ kommt es durch die komplexen Widerstände in Hoch- und Tiefpass zu einer Phasenverschiebung. Für den Hochpassfilter gilt
\[ \phi_\textrm{Hochpass} = \arctan\left( \frac{1}{\omega R C} \right) \]
für den Tiefpass hingegen
\[ \phi_\textrm{Hochpass} = - \arctan\left( \omega R C \right). \]
Dabei ist $R$ der ohmsche Widerstand und $C$ die Kapazität des Kondensators.

Mit Hilfe der \emph{Dezibel-Skala} können Verhältnisse beschrieben werden, bei denen unterschiedliche Größenordnungen im Spiel sind. Man bildet dazu den Logarithmus zur Basis zehn des Verhältnisses und multipliziert dies mit zehn.
\SI{10}{\deci\bel} entspricht also einer Verzehnfachung, \SI{3}{\deci\bel} ungefähr einer Verdoppelung, \SI{-3}{\deci\bel} einer Halbierung, usw.
In einer Formel ausgedrückt lautet logarithmische Pegelmaß
%
\begin{align*}
	\mathcal{D}(U_1,U_2)
	&= 10 \cdot \log \frac{U_1^2}{U_2^2} \,\si{\deci\bel} \\
	&= 20 \cdot \log \frac{U_1}{U_2} \,\si{\deci\bel} \\
	&= 2 \cdot \log \frac{U_1}{U_2} \,\si{\bel}.
\end{align*}

Die elektrische Leistung ist proportional zum Quadrat der Spannung, was in der Dezibel-Skala einen Faktor $2$ zur Folge hat. Folglich entsprechen \SI{6}{\deci\bel} einer Verdopplung.

%Mit einem \emph{Oszilloskop} können Spannungen mit einer hohen zeitlichen Auflösung gemessen werden. Die meisten Oszilloskope können so eingestellt werden, dass sie die Aufzeichnung beginnen, sobald ein gewisser Schwellwert für die Eingangsspannung überschritten wurde (Triggerung). Nach einer voreingestellten Messdauer wird der Trigger erneut gesetzt und das Bild auf der Anzeige überschrieben.

%Mit einem \emph{Oszilloskop-Tastkopf} kann an beliebigen Stellen innerhalb einer Schaltung die Spannung relativ zur Masse gemessen werden. Um die Belastung auf die Signalquelle zu vermindern sind Tastköpfe meist mit einem 10:1-Spannungsteiler ausgerüstet oder lassen sich darauf umschalten \cite{elektroniknet:tastkopf}.

\section{Versuchsaufbau und -durchführung}

Der Versuch gliedert sich im Wesentlichen in zwei Teile. Im ersten Teil sollen die Spannungen die an dem Widerstand und dem Kondensator abfallen mit den Digitalmultimetern gemessen werden. Außerdem soll die Phasenverschiebung gemessen werden. Alle Messwerte werden handschriftlich in einer Tabelle festgehalten.

Im zweiten Versuchsteil werden nocheinmal die Spannungen gemessen, diesmal jedoch mit Unterstützung eines Computers über das Programm LabVIEW. Außerdem wird untersucht was für einen Einfluss die Tiefpassschaltung auf ein Rechtecksignal hat.

Der Versuch wird nach Abbildung~\ref{fig:Versuchsaufbau} aufgebaut. Es wird ein \SI{1}{\kilo\ohm} Widerstand und ein \SI{0.1}{\micro\farad} Kondensator verwendet. Dazu wird die Frequenz am Frequenzgenerator im 1-2-5-Raster von \SI{50}{\Hz} auf \SI{100}{\kilo\Hz} hochgeregelt. Nach jedem Schritt werden sowohl die an den Digitalmultimetern, als auch die am Oszilloskop angezeigten Spannungen notiert. Anschließend werden noch einmal alle Frequenzen durchgeschaltet und es wird bei jeder Frequenz die Phasenverschiebung zwischen der Spannung am Widerstand und der am Kondensator gemessen. Dazu wird die Cursorfunktion des Oszilloskops verwendet.

Nach einer Anleitung zur automatischen Datenaufnahme mit dem Programm LabVIEW, wird das Programm erstellt um die Spannungswerte an Widerstand und Kondensator automatisch zu erfassen und in eine Liste zu schreiben.

Der Frequenzgenerator wird auch Rechteckspannung umgeschaltet. Anschließend wird ein Programm in LabVIEW gestartet, mit dem es möglich ist, einen Abzug der Daten der aktuellen Oszilloskopanzeige zu erstellen. Es werden drei Schnappschüsse erstellt, bei der Grenzfrequenz, bei einem Zehntel der Grenzfrequenz und beim Zehnfachen der Grenzfrequenz. Die Daten werden jeweils abgespeichert.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\begin{scope}[xshift=6cm,yshift=2cm]
			\draw[thick] (0,0) circle (0.5);
			\draw[|-] (0:0.3) -- +(0.4,0) coordinate (R);
			\draw[|-] (60:0.3) -- +(0,0.4) coordinate[label={right:CH 1}] (CH1);
			\draw[|-] (120:0.3) -- +(0,0.4) coordinate[label={left:CH 2}] (CH2);
			\draw[|-] (180:0.3) -- +(-0.4,0) coordinate (L);
			\draw[|-] (270:0.3) -- +(0,-0.4) coordinate[label={left:GND}] (GND);
		\end{scope}
		\node[rotate=90] at (-0.8,2) {Keithley 3390};
		\draw
		(0,0) to[sV]
		(0,4) to[short,-*]
		(2,4) to[short,-*]
		(4,4) to[short]
		(6,4);
		\draw
		(0,0) to[short,-*]
		(2,0) to[short,-*]
		(4,0) to[short]
		(6,0);
		\draw
		(2,4) to[R,l_=1<\kilo\ohm>,-*]
		(2,2) to[C,l_=0.1<\micro\farad>]
		(2,0);
		\draw
		(4,4) to[voltmeter,l_=$U_R$,-*]
		(4,2) to[voltmeter,l_=$U_C$]
		(4,0);
		\draw
		(2,2) -- (4,2)
		(4,4) -| (CH1)
		(4,2) -| +(0.8,1) -| (CH2)
		(4,0) -| (GND);
		\node[rotate=-90] at (7.3,2) {Tektronix TDS};
	\end{tikzpicture}
	\caption{Versuchsaufbau zur Messung des Frequenzganges eines $RC$-Tiefpasses.}
	\label{fig:Versuchsaufbau}
\end{figure}

\section{Formeln}

Die folgenden Zeichen und Einheiten wurden im Weiteren verwendet
%
\begin{itemize}
	\item $U_a$ in Volt (\si{\V}): Ausgangsspannung
	\item $U_e$ in Volt (\si{\V}): Eingangsspannung
	\item $\omega$ in Hertz (\si{\Hz}): Frequenz der Eingangsspannung
	\item $R$ in Ohm (\si{\ohm}): Ohmscher Widerstand
	\item $C$ in Farad (\si{\farad}): Kapazität eines Kondensators
	\item $\phi$ in Radiant (\si{\rad}): Phasenverschiebung
	\item $|X|$ in Ohm (\si{\ohm}): Realteil des Blindwiderstandes
\end{itemize}

\begin{description}
	\item[Amplitudengang beim Tiefpass] Der betragsmäßige Amplitudengang beim \emph{Tiefpass} ist gegeben durch
		\begin{equation}
			\left|\frac{U_a}{U_e}\right| = \frac{1}{\sqrt{1 + (\omega R C)^2}} \label{eq:AmplitudeTiefpass}
		\end{equation}

	\item[Amplitudengang beim Hochpass] Der betragsmäßige Amplitudengang beim \emph{Hochpass} ist gegeben durch
		\begin{equation}
			\left|\frac{U_a}{U_e}\right| = \frac{1}{\sqrt{1 + \frac{1}{(\omega R C)^2}}} \label{eq:AmplitudeHochpass}
		\end{equation}

	\item[Phasenanteil des Amplitudengangs] Beim Hochpass, als auch beim Tiefpass folgt der Phasengang der Beziehung
		\begin{equation}
			|\phi| = \arctan(\omega R C) \label{eq:PhaseHochpass}
		\end{equation}

	\item[Scheinwiderstand des Kondensators beim Tiefpass] Der Blindwiderstand eines, in einen Tiefpass eingebrachten Kondensators wird berechnet durch
		\begin{equation}
			|X| = \frac{R}{\sqrt{\frac{U_e^2}{U_a^2} - 1}} \label{eq:Scheinwiderstand}
		\end{equation}

	\item[Phasen- und Zeitverschiebungen] Der Zusammenhang von Zeit- und Phasendifferenz muss zusätzlich um den Faktor $2 \pi$ skaliert werden, da es sich um Frequenzen statt um Kreisfrequenzen handelt
		\begin{equation}
			\phi = 2 \pi \frac{\Delta t}{T} = 2 \pi \nu \Delta t \label{eq:Phase}
		\end{equation}

	\item[Grenzfrequenz von Hoch- und Tiefpass] Die Grenzfrequenz ergibt sich zu
		\begin{equation}
			\omega_0 = \frac{1}{RC} \label{eq:Grenzfrequenz}
		\end{equation}
\end{description}

\section{Messwerte}

%In Tabelle~\ref{tab:Messwerte} sind die Messwerte des ersten Versuchsteils zu sehen.
Die handschriftliche Tabelle aller Messwerte befindet sich im Anhang.

%\begin{table*}[htpb]
%	\begin{tabular}{S@{\quad}SS@{\quad}SS@{\quad}S}
%		\toprule
%		{Frequenz [\si{\Hz}]} &
%		{$U_R$ [\si{\V}]} &
%		{(Oszilloskop)} &
%		{$U_C$ [\si{\V}]} &
%		{(Oszilloskop)} &
%		{Phasendifferenz [\si{\micro\s}]} \\
%		\midrule
%		50     & 0.16   & 4.99 & 4.985   & 5.00   & 100.00 \\
%		100    & 0.3233 & 4.99 & 4.9784  & 4.99   & 100.00 \\
%		200    & 0.6415 & 4.99 & 4.944   & 4.95   & 100.00 \\
%		500    & 1.5492 & 4.97 & 4.7229  & 4.73   & 100.00 \\
%		1000   & 2.6675 & 4.93 & 4.125   & 4.12   & 88.00  \\
%		2000   & 3.823  & 4.86 & 2.963   & 2.96   & 70.00  \\
%		5000   & 4.568  & 4.79 & 1.422   & 1.41   & 38.00  \\
%		10000  & 4.710  & 4.73 & 0.7358  & 0.734  & 21.20  \\
%		20000  & 4.752  & 4.75 & 0.3727  & 0.371  & 14.70  \\
%		50000  & 4.763  & 4.72 & 0.1506  & 0.149  & 5.80   \\
%		100000 & 4.761  & 4.71 & 0.07585 & 0.0755 & 2.28   \\
%		200000 & 4.767  & 4.72 & 0.03797 & 0.0381 & 1.20   \\
%		\bottomrule
%	\end{tabular}
%	\caption{Messwerte der Messung der Übertragungsfunktion von Hand.}
%	\label{tab:Messwerte}
%\end{table*}

\section{Auswertung}

\subsection{Manuelle Messung}

\subsubsection{Wechselstromwiderstand}

Mit Formel~\eqref{eq:Scheinwiderstand} wird der Blindwiderstand des Kondensators berechnet und in ABB.~\ref{fig:Blindwiderstand} doppel logarithmisch gegen die Frequenz $\omega$ geplottet. Man kann erkennen, dass der Widerstand mit zunehmender Frequenz abnimmt. Dies stimmt mit der Theorie überein, denn es gilt
%
\begin{equation*}
	X = - \frac{1}{\omega C}
\end{equation*}

Für die graphische Auswertung machen wir uns zu Nutze, dass $U_e = \sqrt{U_R^2 + U_C^2}$ und $U_a = U_C$. Damit folgt nämlich
%
\begin{align*}
	|X|
	&= \frac{R}{\sqrt{\frac{U_e^2}{U_a^2} - 1}}
	= \frac{R}{\sqrt{\frac{U_R^2 + U_C^2}{U_C^2} - 1}}
	= \frac{R}{\sqrt{\frac{U_R^2}{U_C^2}}} \\
	&= R \frac{U_C}{U_R}
	= \frac{1}{\omega C}
\end{align*}

Es wird als Fitfunktion $\mathcal{F}_a(\omega)$ verwendet
\[ \mathcal{F}_a(\omega) = \frac{a}{\omega} \]
mit dem Resultat (\texttt{gnuplot})
\[ a = \SI{1.0167e-07}{\farad} \pm \SI{2.3744e-10}{\farad} \]
also \[ C(a) \approx \SI{0.1}{\micro\farad} \] was ziemlich genau dem eingesetzten Kondensator mit $C = \SI{0.1}{\micro\farad}$ entspricht.

\subsubsection{Grenzfrequenz}

In ABB.~\ref{fig:Amplitudengang} ist der Amplitudengang $U_a/U_e$ doppelt logarithmisch gegen die Frequenz $\omega$ geplottet. Zur Nachbildung des Verhaltens von \eqref{eq:AmplitudeTiefpass}, wurde an die Messwerte eine Fitfunktion \[ \mathcal{F}_b(\omega) = \frac{1}{\sqrt{1+\left(\frac{1}{b}\omega\right)^2}} \] gelegt.
Somit entspricht $b \hat{{}={}} \omega_0 = \frac{1}{RC}$. Unter Verwendung von \texttt{gnuplot} ergibt sich schließlich
\[ b = \SI{9661.19}{\per\s} \pm \SI{29.35}{\per\s} \]
%Rechnet man diese Kreisfrequenz nun noch zurück auf eine Frequenz, so erhält man
%\[ \nu_0 = \frac{\omega_0}{2 \pi} = \frac{\SI{9661}{\per\s}}{2\pi} = \SI{1538}{\Hz}. \]

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/Manuell/}{Amplitudengang.tex}
	\caption{(Color online) Der Amplitudengang $U_a/U_e$ wurde doppelt logarithmisch gegen die angelegte Wechselstromfrequenz $\omega$ aufgetragen.}
	\label{fig:Amplitudengang}
\end{figure}

Die Phasenverschiebung $\Delta \phi$ ist in ABB.~\ref{fig:Phasenverschiebung} mit einer Fitfunktion
\[ \mathcal{F}_{c}(\omega) = \arctan\left(\frac{1}{c} \omega\right) \]
geplottet, denn für die Phasenverschiebung gilt \eqref{eq:PhaseHochpass}.
%Auch hier gilt wieder $c \hat{{}={}} \omega_0 = \frac{1}{RC}$.
%Hier liefert \texttt{gnuplot}
Die Grenzfrequenz als Kreisfrequenz ausgerechnet ist
\[ \omega_0 = \SI{10117.2}{\per\s} \pm \SI{1831.22}{\per\s} \]
%umgerechnet auf eine Frequenz erhält man wiederum
%\[ \nu_0 = \frac{\omega_0}{2 \pi} = \frac{\SI{10117}{\per\s}}{2\pi} = \SI{1610}{\Hz}. \]

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/Manuell/}{Phasenverschiebung.tex}
	\caption{(Color online) Die Phasenverschiebung $\Delta \phi$ wurde halblogarithmisch gegen die angelegte Wechselstromfrequenz $\nu=\frac{\omega}{2\pi}$ aufgetragen. Die Phasenverschiebung bei der Grenzfrequenz wurde durch den Schnittpunkt der beiden durchgezogen Linien markiert.}
	\label{fig:Phasenverschiebung}
\end{figure}

%Daraus ergibt sich die Grenzfrequenz $\omega_0$ zu
%%
%\begin{equation*}
%	\begin{aligned}
%		\omega_0(b) &= \SI{9661}{\per\s} \\
%		\omega_0(c) &= \SI{10117}{\per\s}		
%	\end{aligned}
%	\implies
%	\begin{aligned}
%		\nu_0(b) &= \SI{1538}{\Hz} \\
%		\nu_0(c) &= \SI{1610}{\Hz}		
%	\end{aligned}
%\end{equation*}
%
Theoretisch liegt die Grenzfrequenz bei
%
\begin{align*}
	\omega_0^* &= \frac{1}{R C} = \frac{1}{\SI{e3}{\ohm} \cdot \SI{0.1e-6}{\farad}} = \SI{10000}{\per\s}
	%\nu_0^* &= \frac{\omega_0^*}{2 \pi} = \SI{1592}{\Hz}
\end{align*}

Bei der Grenzfrequenz $\omega_0 = $ \SI{10}{\kilo\Hz} berechnet sich die Phasenverschiebung zu $\left| \Delta \phi \right| = \arctan(\omega_0 R C) = \frac{\pi}{4} \approx \num{0.785}$. Dieser Wert ist auch in ABB.~\ref{fig:Phasenverschiebung} zu finden und dort durch den Schnittpunkt von zwei horizontalen Linien markiert.

%\begin{figure}[ht]
%	\centering\sffamily
%	\subimport{Messwerte/Manuell/}{Spannungsverlauf.tex}
%	\caption{(Color online) Die Spannungen $U_R$ und $U_C$ aufgetragen über der angelegten Wechselstromfrequenz $\omega$.}
%	\label{fig:Spannungsverlauf}
%\end{figure}

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/Manuell/}{Blindwiderstand.tex}
	\caption{(Color online) Der Blindwiderstand $X$ wurde doppelt logarithmisch gegen die angelegte Wechselstromfrequenz $\omega$ aufgetragen.}
	\label{fig:Blindwiderstand}
\end{figure}

\subsection{Implementierung in LabVIEW}

\subsubsection{Wechselstromwiderstand}

Mit den durch LabVIEW gemessenen Werten für die Spannungsamplituden wird erneut der Wechselstromwiderstand $|X|$ berechnet und gegen die Frequenz $\omega$ aufgetragen (Abb. ~\ref{fig:Blindwiderstand-LabVIEW}). Es ist wieder der Zusammenhang $X \sim \frac{1}{\omega}$ zu sehen. Es wird wieder die Fitfunktion
\[ \mathcal{F}_a(\omega) = \frac{a}{\omega} \] verwendet.
\texttt{gnuplot} gibt für den Fitparameter $a$ den Wert \[ a = \SI{1.0632e-07}{\farad} \pm \SI{2.5630e-10}{\farad} \] aus.

\subsubsection{Grenzfrequenz}

In ABB.~\ref{fig:Amplitudengang-LabVIEW} ist der Amplitudengang $U_a/U_e$ aus den LabVIEW-Daten doppelt logarithmisch gegen die Frequenz $\omega$ geplottet. Mit dem gleichen Fit wie in der manuellen Messung lautet die Grenzfrequenz \[ \omega_0 = \SI{9750.25}{\per\s} \pm \SI{4.96}{\per\s} \]
%Rechnet man diese Kreisfrequenz nun noch zurück auf eine Frequenz, so erhält man
%\[ \nu_0 = \frac{\omega_0}{2 \pi} = \frac{\SI{9750}{\per\s}}{2\pi} = \SI{1552}{\Hz}. \]

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/LabVIEW/}{Amplitudengang.tex}
	\caption{(Color online) Zu sehen ist der Frequenzgang eines Tiefpasses. Dieser wurde doppelt logarithmisch gegen die angelegte Wechselstromfrequenz $\omega$ aufgetragen.}
	\label{fig:Amplitudengang-LabVIEW}
\end{figure}

%Theoretisch liegt die Grenzfrequenz bei
%%
%\begin{align*}
%	\omega_0^* &= \frac{1}{R C} = \frac{1}{\SI{e3}{\ohm} \cdot \SI{0.1e-6}{\farad}} = \SI{10000}{\per\s} \\
%	\nu_0^* &= \frac{\omega_0^*}{2 \pi} = \SI{1592}{\Hz}	
%\end{align*}

%\begin{figure}[ht]
%	\centering\sffamily
%	\subimport{Messwerte/Manuell/}{Spannungsverlauf.tex}
%	\caption{(Color online) Die Spannungen $U_R$ und $U_C$ aufgetragen über der angelegten Wechselstromfrequenz $\omega$.}
%	\label{fig:Spannungsverlauf-LabVIEW}
%\end{figure}

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/Manuell/}{Blindwiderstand.tex}
	\caption{(Color online) Der Blindwiderstand $X$ wurde doppelt logarithmisch gegen die angelegte Wechselstromfrequenz $\omega$ aufgetragen.}
	\label{fig:Blindwiderstand-LabVIEW}
\end{figure}

\subsection{Integrierverhalten}

Wird an den Tiefpass eine Rechteckspannung angelegt, so weist die Sprungantwort für Frequenzen $\omega \gg \frac{1}{RC}$ eine integrierende Eigenschaft auf.
Der Umladestrom den Kondensators $C \cdot \frac{\diff U_a}{\diff t}$ muss durch den Widerstand $R$ fließen. Es gilt:
%
\begin{align*}
	\frac{U_e - U_a}{R} &= I =C \cdot \frac{\diff U_a}{\diff t} \\
	\implies U_e - U_a &= RC \cdot \frac{\diff U_a}{\diff t}
\end{align*}
%
Für hinreichend große Frequenzen wird $U_a$ vernachlässigbar klein, sodass sich ergibt:
%
\begin{align*}
	U_e &= RC \cdot \frac{\diff U_a}{\diff t} \\
	\implies U_a &= \frac{1}{RC} \int U_e(t) \diff t
\end{align*}
%
In den Abbildungen~\ref{fig:0.1-w_0}, \ref{fig:1-w_0} und \ref{fig:10-w_0} ist schön zu erkennen, wie sich die Sprungantwort bei der Grenzfrequenz, darunter und darüber verhält. In ABB.~\ref{fig:0.1-w_0}, bei einem Zehntel der Grenzfrequenz, lässt der Tiefpass sie fast unverändert passieren und die Signalformen von Ein- und Ausgang unterscheiden sich so gut wie nicht.

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/}{0.1-w_0.tex}
	\caption{(Color online) Integratorfunktion des Tiefpasses evaluiert bei $\num{0.1} \cdot \omega_0$.}
	\label{fig:0.1-w_0}
\end{figure}

Bei der Grenzfrequenz in ABB.~\ref{fig:1-w_0} kann man erkennen, wie durch den Kondensator die Auf- und Abstiegskanten geglättet werden.

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/}{1-w_0.tex}
	\caption{(Color online) Integratorfunktion des Tiefpasses evaluiert bei $\omega_0$.}
	\label{fig:1-w_0}
\end{figure}

Wirklich integrierende Eigenschaften sind jedoch erst in ABB.~\ref{fig:10-w_0} zu erkennen, bei dem Zehnfachen der Grenzfrequenz. Die deutlich erkennbare Dreiecksfunktion stimmt mit dem, durch die Theorie vorhergesagten, Integral einer Rechteckfunktion überein.

\begin{figure}[ht]
	\centering\sffamily
	\subimport{Messwerte/}{10-w_0.tex}
	\caption{(Color online) Integratorfunktion des Tiefpasses evaluiert bei $\num{10} \cdot \omega_0$.}
	\label{fig:10-w_0}
\end{figure}

\section{Fehlerbetrachtung}

Als theoretische Werte für die Grenzkreisfrequenz und die Grenzfrequenz ergeben sich
%
\begin{align*}
	\omega_0^* &= \frac{1}{R C} = \frac{1}{\SI{e3}{\ohm} \cdot \SI{0.1e-6}{\farad}} = \SI{e4}{\per\s} \\
	\implies \nu_0^* &= \frac{\omega_0}{2 \pi} = \SI{1592}{\Hz}
\end{align*}
%
wobei $R = \SI{1}{\kilo\ohm}$ und $C = \SI{0.1}{\micro\farad}$ verwendet wurden.

\subsection{Manuelle Messung}

Es wurden die relativen Abweichungen aller Werte vom theoretischen Wert berechnet.
%
\begin{description}
	\item[Kapazität] Aus dem Fit ergibt sich eine experimentelle Frequenz von $C(a) \approx \SI{1.0336e-7}{\farad}$ und somit
		\begin{align*}
			Q[C(a),C] &= \frac{C(a) - C}{C} = \frac{\SI{1.0336e-7}{\farad} - \SI{1e-7}{\farad}}{\SI{1e-7}{\farad}} \\
			&= \num{0.033} \approx \SI{3}{\percent}
		\end{align*}
	\item[Grenzfrequenz] Die Grenzfrequenz wurde aus zwei Graphen ermittelt. Einmal aus dem Amplitudengang (Parameter $b$) und aus der Phasenverschiebung (Parameter $c$).
		\begin{align*}
			Q[\omega_0(b),\omega_0^*] &= \frac{\omega_0(b) - \omega_0^*}{\omega_0^*} = \frac{\SI{9661.19}{\per\s} - \SI{10000}{\per\s}}{\SI{10000}{\per\s}} \\
			&= \num{-0.033} \approx \SI{3}{\percent} \\
			Q[\omega_0(c),\omega_0^*] &= \frac{\omega_0(c) - \omega_0^*}{\omega_0^*} = \frac{\SI{10117.2}{\per\s} - \SI{10000}{\per\s}}{\SI{10000}{\per\s}} \\
			&= \num{0.011} \approx \SI{1}{\percent}
		\end{align*}
\end{description}

Im Versuch kam ein Folienkondesator der Güteklasse M zum Einsatz. Diese Güteklasse entspricht einer Toleranz der Kapazität von \SI{20}{\percent}. Die Messergebnisse liegen also alle innerhalb der Toleranzgrenze.

\subsection{LabVIEW-Messung}

Auch hier wurden die relativen Abweichungen aller Werte vom theoretischen Wert berechnet. Hier fällt jedoch die Messung der Phasenverschiebung weg, womit nur eine Grenzfrequenz betrachtet werden kann.
%
\begin{description}
	\item[Kapazität] Aus dem Fit ergibt sich eine experimentelle Frequenz von $C(a) \approx \SI{1.0336e-7}{\farad}$ und somit
		\begin{align*}
			Q[C(a),C] &= \frac{C(a) - C}{C} = \frac{\SI{1.0632e-07}{\farad} - \SI{1e-7}{\farad}}{\SI{1e-7}{\farad}} \\
			&= \num{0.063} \approx \SI{6}{\percent}
		\end{align*}
		Die Abweichung für die Kapazität ist bei der LabVIEW-Messung größer.%, weil hier in kurzen Intervallen von nur \SI{1}{\s} gemessen wurde und der Kondensator kurze Zeit braucht um sich an die neue Frequenz anzupassen.
	\item[Grenzfrequenz] Die Grenzfrequenz wurde aus zwei Graphen ermittelt. Einmal aus dem Amplitudengang (Parameter $b$) und aus der Phasenverschiebung (Parameter $c$).
		\begin{align*}
			Q[\omega_0(b),\omega_0^*] &= \frac{\omega_0(b) - \omega_0^*}{\omega_0^*} = \frac{\SI{9750.25}{\per\s} - \SI{10000}{\per\s}}{\SI{10000}{\per\s}} \\
			&= \num{-0.025} \approx \SI{3}{\percent}
		\end{align*}
\end{description}

\section{Zusammenfassung}
In dem Versuch wurde eine Tiefpass- und eine Hochpassschaltung untersucht. Dabei wurden unter anderem der Phasengang, und die Grenzfrequenz ermittelt, sowie den Wechselstromwiderstand des verwendeten Kondensators.

Der Wechselstromwiderstand $X$ wurde aus den gemessenen Amplituden berechnet und doppellogarithmisch gegen die Frequenz $\omega$ geplottet. In dem Graph (ABB.~\ref{fig:Blindwiderstand}) ist der Zusammenhang $X \sim \omega^{-1}$ zu sehen. Mit Hilfe des Blindwiderstandes wurde auch die Kapazität des Kondensators ausgemessen. Diese konnte bis auf \SI{6}{\percent} Genauigkeit bestimmt werden.

Die Grenzfrequenz $\omega_0$ wurde auf zwei Wegen berechnet. Einmal wurde der Amplitudengang $|{U_C}/{U_R}|$ doppelt logarithmisch, einmal der Phasengang $\Delta \phi$ halblogarithmisch gegen die Frequenz geplottet. \texttt{gnuplot} hat für die jeweiligen Fitfunktionen die Fitparameter $b$ und $c$ bestimmt und damit wurden folgende Werte für die Grenzfrequenz berstimmt:
%
\begin{equation*}
	\begin{aligned}
		\omega_0(b) &= \SI{9661}{\per\s} \\
		\omega_0(c) &= \SI{10117}{\per\s}		
	\end{aligned}
\end{equation*}

Der theoretisch berechnete Wert für die Grenzfrequenz des Tiefpasses ist $\omega_0=\SI{10}{\kilo \Hz}$. Die erhaltenen Werte stimmen also gut mit dem theoretischen Wert überein. Die relative Abweichung beträgt nur ca.\ \SI{3}{\percent}.
Die Phasenverschiebung zwischen Eingangs- und Ausgangssignal beträgt bei dieser Frequenz gerade $\pi/4$.

\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
