# Elektronikpraktikum

This repository contains all files to reproduce my lab reports, which I made while doing Elektronikpraktikum at University of Stuttgart in the 4th term of my studies of physics.

## Build

To build the reports one by one, open a terminal in a directory containing a report of your choice and enter

    $ make

A `Makefile` to build all reports is not included, but I will include it, if anybody asks for it.

## Prerequisites

You will need a LaTeX distribution and `gnuplot`. The whole stuff is known to work with

* TeXlive 2013
* `gnuplot 4.6 patchlevel 3`

## Bugs and Errors

"Errare humanum est" - Cicero. Please report bugs on the issue tracker.