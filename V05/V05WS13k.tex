\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V05: Bipolar- und Feldeffekttransistoren}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Torsten Rendler}\noaffiliation
\date{18.\ November 2013}

\begin{abstract}
	In diesem Versuch werden die Eingangs- und Ausgangskennlinien von Bipolar- und Feldeffekttransistoren. Für den Bipolartransistor wird zudem die Stromverstärkung $\mathcal{B}$, für den Feldeffekttransistor die Steilheit $\mathcal{S}$ an ausgezeichneten Punkten ermittelt.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Halbleiter}

Ein Transistor oder eine Diode ist in der Regel aus einem Halbleiter aufgebaut. Der Unterschied zwischen normalen Leitern, Halbleitern und Isolatoren besteht in der Lage der Fermienergie im Bezug auf die Bandlücke zwischen Valenz- und Leitungsband. Eine Illustration ist in Abbildung~\ref{fig:baendermodell} abgebildet.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[
			electron/.style = {
				circle,
				fill=black,
				font=\tiny\color{white},
				inner sep=0pt
			},
			defect/.style = {
				circle,
				draw=black,
				fill=white,
				font=\tiny,
				inner sep=0pt
			}
		]
		\begin{scope}
			\draw[->] (0,0) -- node[below] {Metall} (2,0) node[below] {$x$};
			\draw[->] (0,0) -- (0,2) node[left] {$E$};
			\draw[opacity=0.5,fill=blue] (0,0.2) rectangle (2,0.7);
			\draw[opacity=0.5,fill=orange] (0,0.5) rectangle (2,1);
			\draw[dashed] (-0.1,0.6) node[left] {$E_F$} -- (2,0.6);
			\foreach \p in {(0.2,0.4), (0.6,0.5), (1,0.4), (1.4,0.5), (1.8,0.4), (1.6,0.8)} {
				\node[electron] at \p {$-$};
			}
		\end{scope}
		
		\begin{scope}[xshift=2.8cm]
			\draw[->] (0,0) -- node[below] {Halbleiter} (2,0) node[below] {$x$};
			\draw[->] (0,0) -- (0,2) node[left] {$E$};
			\draw[opacity=0.5,fill=blue] (0,0.2) rectangle (2,0.7);
			\draw[opacity=0.5,fill=orange] (0,0.9) rectangle (2,1.4);
			\draw[dashed] (-0.1,0.8) node[left] {$E_F$} -- (2,0.8);
			\foreach \p in {(0.2,0.4), (1,0.4), (1.4,0.5), (1.8,0.4)} {
				\node[electron] at \p {$-$};
			}
			\node[defect] (p) at (0.6,0.5) {$+$};
			\node[electron] (n) at (0.7,1.1) {$-$};
			\draw[->] (p) edge[bend left] (n);
		\end{scope}
		
		\begin{scope}[xshift=5.6cm]
			\draw[->] (0,0) -- node[below] {Isolator} (2,0) node[below] {$x$};
			\draw[->] (0,0) -- (0,2) node[left] {$E$};
			\draw[opacity=0.5,fill=blue] (0,0.2) rectangle (2,0.7);
			\draw[opacity=0.5,fill=orange] (0,1.3) rectangle (2,1.8);
			\draw[dashed] (-0.1,1) node[left] {$E_F$} -- (2,1);
			\foreach \p in {(0.2,0.4), (0.6,0.5), (1,0.4), (1.4,0.5), (1.8,0.4)} {
				\node[electron] at \p {$-$};
			}
		\end{scope}
	\end{tikzpicture}
	\caption{Das Bändermodell. Das Leitungsband ist orange, das Valenzband blau dargestellt. Kreise mit einem \glqq$-$\grqq\ sind Elektronen, Kreise mit einem \glqq$+$\grqq\ sind Defektstellen.}
	\label{fig:baendermodell}
\end{figure}

Bei Leitern ist keine Bandlücke vorhanden, bei Isolatoren ist sie sehr groß. Halbleiter haben eine kleine Bandlücke und leiten von sich aus sehr schlecht, weshalb sie dotiert werden. Hat der zu dotierende Halbleiter vier Valenzelektronen, so wird er mit einem Element mit fünf Valenzelektronen verunreinigt um zusätzliche Leitungselektronen zu erhalten (n-Dotierung) oder mit einem Element mit drei Valenzelektronen um durch die enstehenden Defektstellen eine Löcherleitung zu etablieren (p-Dotierung).

Beim Kontakt einer p- mit einer n-Schicht beginnt ein Strom zu fließen, bei dem die Elektronen der n-Schicht in die Löcher der p-Schicht diffundieren und dort rekombinieren. So bildet sich eine Sperrschicht zwischen den beiden Schichten und der Rekombinationsprozess kommt zum erliegen.

\subsection{Transistoren}

\subsubsection{Bipolartransistoren}

Der schematische Aufbau eines Bipolartransistors (engl.: \textsl{bipolar junction transistor}, BJT) mit npn-Belegung ist in Abbildung~\ref{fig:bipolartransistor} zu sehen. Der Bipolartransistor verfügt über drei Anschlüsse: Basis, Emitter und Kollektor. An den n-p- bzw.\ p-n-Übergängen entstehen Sperrschichten, das entstehende elektrische Feld wirkt einer weiteren Diffusion entgegen. Eine der beiden Sperrschichten ist auf jeden Fall in Sperrrichtung geschaltet, wenn man einen Strom anlegt.

\begin{figure*}[htpb]
	\centering
	\input{NPN_BJT_Basic_Operation_(Active)_DE.pdf_tex}
	\caption{Funktionsweise eines Bipolartransistors aus \cite{WikipediaDE:Bipolartransistor}}
	\label{fig:bipolartransistor}
\end{figure*}

Legt man nun einen Strom zwischen Emitter und Basis an, sodass die Sperrschicht dazwischen durchlässig wird, so treten Elektronen aus dem Emitter in die Basis ein und es fließt ein Strom $I_{BE}$. Die p-Schicht wird daraufhin mit Elektronen geflutet und die p-n-Schicht zwischen Basis und Kollektor wird durchlässig. Somit besteht eine Verbindung zwischen Kollektor und Emitter und es kann ein Strom hindurchfließen.

Legt man den Strom zwischen Basis und Emitter anders herum an, so bleiben die Sperrschichten undurchlässig und nichts passiert. Der Transistor wird also durch den Strom zwischen Basis und Emitter $I_{BE}$ gesteuert.

Die Eingangskennlinie $I_B$ eines BJT sieht aus wie die einer Diode, die Ausgangskennlinie $I_C$ steigt exponentiell an, bis ein durch $I_B$ festgelegter Grenzwert erreicht wird.

\subsubsection{Feldeffekttransistoren}

Anders als BJTs werden Feldeffekttransistoren (engl.: \textsl{field effect transistor}, FET) durch das durch die anliegende Spannung erzeugte elektrische Feld gesteuert. Ein FET hat ebenfalls drei Anschlüsse, die jedoch Source, Gate und Drain heißen. Die Steuerspannung am Gate steuert die Leitfähigkeit.

Zwei Ausführungen von FETs sind der JFET und der MOSFET.
%
\begin{description}
	\item[JFET] Ein JFET ist aus einem n dotierten Kanal zwischen Source und Drain mit einer p dotierten Elektrode namens Gate darin. Die Sperrschicht am p-n-Übergang wächst in den n-Kanal hinein und kann diesen blockieren. Die Sperrschicht wächst mit steigender Spannung zwischen Gate und Source $U_{GS}$.
	
	\item[MOSFET] Der Aufbau ist ähnlich dem eines JFETs bis darauf hin, dass sich zwischen Gate und Kanal ein Isolator befindet. Ein durch die Spannung $U_{GS}$ erzeugtes elektrisches Feld beeinflusst die Elektronendichte im Kanal.
\end{description}

\begin{figure}[htpb]
	\centering
	\subfloat[BJT]{
		\begin{tikzpicture}[circuitikz/tripoles/mos style/arrows]
			\draw (0,0) node[npn] (npn) {}
			(npn.base) node[left] {B}
			(npn.collector) node[above] {C}
			(npn.emitter) node[below] {E};
		\end{tikzpicture}
	}
	\subfloat[n-Channel MOSFET]{
		\begin{tikzpicture}[circuitikz/tripoles/mos style/arrows]
			\draw (3,0) node[nmos] (mos) {}
			(mos.gate) node[left] {G}
			(mos.drain) node[above] {D}
			(mos.source) node[below] {S};
		\end{tikzpicture}
	}
	\caption{Schaltsymbole für Transistoren.}
\end{figure}

Da bei FETs der Übergang zwischen Gate und Source in Sperrrichtung betrieben wird ist der Strom nur sehr klein. Der Strom zwischen Drain und Source lässt sich quasi \emph{stromlos} steuern. Man spricht deshalb von spannungsgesteuerten Widerständen.

\section{Versuchsaufbau und -durchführung} 

\subsection{Bipolartransistor}

Die Schaltung wird gemäß der Versuchsanleitung aufgebaut. Die Strom- und Spannungsbegrenzungen werden ebenfalls gesetzt um eine Überhitzung des Transistors zu verhindern.

\textit{Durchführung}: Es wird bei konstanter Emitter-Kollektor-Spannung $U_{EC}$ von \SI{10}{\volt} die Basis-Spannung $U_B$ durchgefahren. Zunächst werden Basisstrom $I_B$, Basis-Emitter-Spannung $U_{BE}$ und Kollektorstrom $I_C$ von Hand notiert. Im Anschluss wird ein LabVIEW-Programm verwendet, das die Werte vollautomatisch durchfährt.

Im nächsten Versuchsteil soll die Ausgangskennlinie des Bipolartransistors bei den Basisströmen $I_B$ = \SI{3}{\micro \ampere}, \SI{10}{\micro \ampere} \SI{30}{\micro \ampere} gemessen werden. Dazu wird in den Messwerten aus dem vorangegangenen Versuchsteil geschaut, bei welchen Steuersspannungen $U_\textrm{St}$ die entprechenden Basisströme auftreten. Außerdem wird LabVIEW entsprechend angepasst, sodass der Kollektorstrom $I_C$ gegen die Kollektor-Emitter-Spannung $U_{CE}$ aufgetragen wird.


\subsection{Feldeffekttransistor}

An der Schaltung wird nun der Bipolarstransistor mit dem Feldeffekttransistor ausgetauscht und die Polarität der Basisspannung umgepolt.

\textit{Durchführung}:
Die Durchführung läuft analog. Zuerst werden die Messwerte für die Eingangskennlinie von Hand aufgenommen, anschließend mit Hilfe von LabVIEW. Dann wird bei drei verschiedenen Werten der Gate-Source-Spannung $U_{GS}$, der Drainstrom $I_D$ in Abhängigkeit von der Drain-Source-Spannung $U_{DS}$ mit LabVIEW gemessen.

\section{Formeln}

Die folgenden Zeichen und Einheiten wurden im Weiteren verwendet
%
\begin{itemize}
	\item $P$ in Watt (\si{\W}): Leistung
		\begin{itemize}
			\item Index $V$: Verlustleistung
			\item Index $\textrm{therm}$: Über therm. Kontakt fließende Leistung
		\end{itemize}
	\item $U_{CE}$ in Volt (\si{\V}): Kollektor-Emitter-Spannung 
	\item $I_C$ in Watt (\si{\W}):
	\item $I_B$ in Ampere (\si{\ampere}): Basistrom
	\item $\mathcal{B}$ ohne Einheit: Stromverstärkung
	\item $I_D$ in Ampere (\si{\ampere}): Drainstrom
	\item $U_{GS}$ in Volt (\si{\V}): Gate-Source-Spannung
	\item $\mathcal{S}$ in Ampere pro Volt ($\si{\ampere\per\V}$): Steilheit
\end{itemize}

\begin{description}
	\item[Leistung] Die im Bipolarstransistor verbrauchte Leistung $P_V$ berechnet sich nach:
		%
		\begin{equation}
			P_V = U_{CE} \cdot I_C
			\label{eq:Leistung}
		\end{equation}

	\item[Stromverstärkung] Für die Stromverstärkung $\mathcal{B}$ des Bipolartransistors gilt:
		%
		\begin{equation}
			\mathcal{B} = \frac{I_C}{I_B}
			\label{eq:Stromverstaerkung}
		\end{equation}

	\item[Steilheit] Für die Steilheit des Feldeffekttransistors gilt:
		%
		\begin{equation}
			\mathcal{S} = \frac{\delta I_D}{\delta U_{GS}}\biggr|_{U_{DS}}
		\end{equation}
\end{description}

\section{Messwerte}

Die handschriftlich aufgenommenen Messwerte sind nochmals in den Tabellen~\ref{tab:BIT} und~\ref{tab:FET} dargelegt. Ein handschriftliches Original befindet sich im Anhang.

\begin{table}[!ht] 
	\centering
	\begin{tabular}{SSS}
		\toprule
		{Basisstrom $I_C$ [\si{\micro \ampere}]} & {$U_{BE}$ [\si{\volt}]\footnote{Dabei ist $U_{BE}$ die Basis-Emitter-Spannung.}} & {Kollektorstrom $I_C$ [\si{\milli \ampere}]} \\
		\midrule
		1   & 0.609 & 0.285  \\
		3   & 0.634 & 0.779  \\
		10  & 0.662 & 2.584  \\
		30  & 0.680 & 8.328  \\
		100 & 0.625 & 31.000 \\
		165 & 0.611 & 50.000 \\
		\bottomrule
	\end{tabular}
	\caption{Manuell gemessene Werte zur Bestimmung der Stromverstärkung bis zum Erreichen des maximalen Kollektorstroms.}
	\label{tab:BIT}
\end{table}

\begin{table}[!ht] 
	\centering
	\begin{tabular}{SSSS}
		\toprule
		{$U_\textrm{St}$ [\si{\volt}] \footnote{$U_\textrm{St}$: Steuerspannung}} &
		{$U_{GS}$ [\si{\volt}]\footnote{$U_{GS}$: Gatespannung}} &
		{$I_D$ [\si{\milli\A}]\footnote{$I_D$: Drainstrom}} &
		{$I_G$ [\si{\milli\A}]\footnote{$I_G$: Gatestrom}} \\
		\midrule
		0.5 & -0.5  & 9.9    & 0.00001 \\
		1.0 & -0.99 & 7.5    & 0.00001 \\
		1.5 & -1.49 & 5.4    & 0.00001 \\
		2.0 & -1.99 & 3.58   & 0.00001 \\
		2.5 & -2.49 & 1.98   & 0.00002 \\
		3.0 & -2.99 & 0.74   & 0.00003 \\
		3.5 & -3.49 & 0.0001 & 0.00003 \\
		4.0 & -3.98 & 0.0001 & 0.00003 \\
		4.5 & -4.48 & 0.0001 & 0.00004 \\
		5.0 & -4.98 & 0.0001 & 0.00005 \\
		5.5 & -5.48 & 0.0001 & 0.00005 \\
		6.0 & -5.97 & 0.0001 & 0.00006 \\
		\bottomrule
	\end{tabular}
	\caption{Manuell erfasste Messwerte vom Feldeffekttransistor.}
	\label{tab:FET}
\end{table}

\section{Auswertung}
\subsection{Bipolartransistor}
\subsubsection{Stromverstärkung}
Wird die Stromverstärkung $\mathcal{B}$ nach Formel~\eqref{eq:Stromverstaerkung}  mit den von Hand aufgenommen Werten (siehe Tabelle~\ref{tab:BIT}) in jedem Messpunkt berechnet und der Mittelwert gebildet, ergibt sich:
\[\mathcal{B} = 282\]

Die mit LabVIEW aufgenommenen Werte werden mit \texttt{gnuplot} in ABB.~\ref{fig:BPT-Stromverstaerkung} geplottet, $\mathcal{B}$ ist dann die Steigung der an die Messpunkte gelegten Fitgeraden. Die Fitfunktion lautet
%
\begin{align*}
	\mathcal{F}_\mathcal{B} = \mathcal{B} \cdot I_B
\end{align*}
%
Der Fitparameter $\mathcal{B}$ ergibt sich zu
%
\begin{align*}
	\mathcal{B} = \num{288.186} \pm \num{0.693}
\end{align*}
%
Dieser Wert stimmt bis auf eine Abweichung von ca.\ \SI{2}{\percent} mit dem manuell bestimmten überein.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/BPT/}{Stromverstaerkung.tex}
	\caption{(LabVIEW-Daten) Für die Stromverstärkung wurde der Kollektorstrom über dem Basistrom aufgetragen.}
	\label{fig:BPT-Stromverstaerkung}
\end{figure}

Die am Transistor dissipierte elektrische Leistung $P_V$ berechnet sich nach Formel~\eqref{eq:Leistung}. Die dissipierte Leistung wird über den Basisstrom $I_B$ aufgetragen, es ergibt sich das Schaubild in ABB.~\ref{fig:BPT-Leistung}.
 
\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/BPT/}{Leistung.tex}
	\caption{(LabVIEW-Daten) Die Leistung wurde berechnet unter der Voraussetzung, dass $U_{CE} = \SI{10}{\volt}$ ist.}
	\label{fig:BPT-Leistung}
\end{figure}

\subsubsection{Ausgangskennlinie}
Nun wird die Ausgangskennlinie für 3 verschiedene Basisströme $I_B$ gemessen. Aus den Messdaten des vorherigen Versuchteils, kann die zugehörige Steuerspannung ablesen:
%
\begin{itemize}
	\item $I_B = \SI{3}{\micro\ampere}$, $U_\textrm{St} = \SI{0.74}{\V}$
	\item $I_B = \SI{10}{\micro\ampere}$, $U_\textrm{St} = \SI{1.0}{\V}$
	\item $I_B = \SI{30}{\micro\ampere}$, $U_\textrm{St} = \SI{1.71}{\V}$
\end{itemize}

Das Ausgangskennlinienfeld erhält man, indem man den Kollektorstrom $I_C$ über die Kollektor-Emitter-Spannung plottet, siehe dazu ABB.~\ref{fig:BPT-Ausgangskennlinie}.
%
\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/BPT/}{Ausgangskennlinie.tex}
	\caption{(LabVIEW-Daten) Für die Ausgangskennlinie wurde der Kollektorstrom $I_C$ gegen die Kollektor-Emitter-Spannung $U_{CE}$ aufgetragen.}
	\label{fig:BPT-Ausgangskennlinie}
\end{figure}

\subsection{Feldeffekttransistor}
\subsubsection{Eingangskennlinie}
Die Eingangskennlinie des Feldeffekttransistors ist in Abbildung~\ref{fig:FET-Eingangskennlinie} abgebildet. Es gibt zwei Möglichkeiten die Steilheit der Eingangskennlinie zu bestimmen.
%
\begin{enumerate}
	\item Die Eingangskennlinie selbst kann gefittet werden, z.\,B.\ mit einer Fitfunktion der Form
		\[ \mathcal{F}_{a,b,c}(U_{GS}) = a \left( 1 + \frac{U_{GS}}{b} \right)^2 + c. \]
		Leitet man diese Fitfunktion hab, so erhält man die Steilheit in jedem Punkt durch einsetzen.
		\[ \mathcal{F}_{a,b}'(U_{GS}) = 2\cdot \frac{a}{b} \left(1+\frac{U_{GS}}{b} \right) \]
	\item Die Eingangskennlinie kann stückweise durch lineare Fits der Form
		\[ \mathcal{F}_\mathcal{S}(U_{GS}) = \mathcal{S} \cdot U_{GS} + c \]
		angenährt werden. Für die Linearisierung wurde die Funktion an ein Intervall $[U_{GS}^*-0.1,U_{GS}^*+0.1]$ gefittet.
\end{enumerate}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/FET/}{Eingangskennlinie.tex}
	\caption{(LabVIEW-Daten) Eingangskennlinie $I_D(U_{GS})$ des JFETs.}
	\label{fig:FET-Eingangskennlinie}
\end{figure}

Zum besseren Vergleich wurden die durch die beiden unterschiedlichen Fitfunktionen erhaltenen Steilheiten in Tabelle~\ref{tab:Steilheit} gegenübergestellt.

\begin{table}[!ht]
	\centering
	\begin{tabular}{S S S}
		\toprule
		{$U_{GS}$ $[\si{\volt}]$} &
		{$\mathcal{F}_{a,b}'$ $[\si{\ampere\per\volt}]$} &
		{$\mathcal{F}_{\mathcal{S}}$ $[\si{\ampere\per\volt}]$} \\
		\midrule
		-0.5 & 4.63 & 4.52 \\
		-1.5 & 3.33 & 3.44 \\
		-2.5 & 2.04 & 1.94 \\
		\bottomrule
	\end{tabular}
	\caption{Die Steilheit $\mathcal{S}$ wurde mit zwei unterschiedlichen Methoden bestimmt.}
	\label{tab:Steilheit}
\end{table}

\subsubsection{Ausgangskennlinie}
In ABB.~\ref{fig:FET-Ausgangskennlinie} sind Ausgangskennlinien für die Gatespannungen $\{ \SI{0}{\V}, \SI{-0.83}{\V}, \SI{-1.96}{\V} \}$ geplottet.
Man erkennt, dass je kleiner die Steuerspannung am Gate $U_G$ ist, desto größer ist der Drain-Strom $I_D$. Für kleine Drain-Source-Spannungen $U_{DS}$ steigt der Drain-Strom $I_D$ nahezu linear an, während er für große $U_{DS}$ gegen einen konstanten Grenzwert strebt.
Auf Grund des linearen Verhaltens bei kleinen Drain-Source-Spannungen $U_{DS}$ kann der Feldeffektransistor in diesem Bereich als verstellbarer ohmscher Widerstand verwendet werden.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/FET/}{Ausgangskennlinie.tex}
	\caption{(LabVIEW-Daten) Ausgangskennlinie $I_D(U_{DS})$ des JFETs.}
	\label{fig:FET-Ausgangskennlinie}
\end{figure}

\section{Fehlerrechnung}
Die im mit der Versuchsanleitung mitgelieferten Datenblatt aufgeführten Literaturwerte können nicht reprouziert werden, da das dafür erforderliche Setup nicht nachgebildet werden kann.
Trotz der ansonsten qualitativen Natur des Versuches sind die folgenden Fehlerquellen zu berücksichtigen:
%
\begin{itemize}
	\item Die Messgeräte und das Netzgerät haben eine unbekannte Anzeigeungenauigkeit.
	\item Die Leitfähigkeit des Halbleiters im Transistor ist temperaturabhängig und die Raumtemperatur ist unbekannt.
\end{itemize}

\section{Zusammenfassung}
Im Vesuch wurden ein npn-Bipolartransistor und ein JFET-Feldeffekttransistor untersucht.

Für den Bipolartransistor wurden die Stromverstärkung berechnet, sowie die Verlustleistung und die Ausgangskennlinie geplottet.
Die Stromverstärkung ergab sich aus der manuellen Messung zu $\mathcal{B} = 282$ und mit einem Fit an die LabVIEW-Messung zu $\mathcal{B} = 288$.
Die Ausgangskennlinien wurden bei Basisströmen von \SIlist[list-final-separator={ und }]{3;10;30}{\micro\A} geplottet, wobei die Form der Kurven der Erwartung entspricht.

Für den Feldeffekttransistor wurden die Eingangskennlinie und einige Ausgangskennlinien aufgezeichnet. Aus der Eingangskennlinie wurde deren Steilheit ermittelt.
Die Ausgangskennlinien wurden für \SIlist[list-final-separator={ und }]{0;-0.83;-1.96}{\micro\A} geplottet. Es ist klar zu erkennen, dass mit (betragsmäßig) steigender Gatespannung der Drain-Strom abnimmt.

\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
