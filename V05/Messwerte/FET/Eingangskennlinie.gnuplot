#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.7
set pointsize 0.5
set output 'Eingangskennlinie.tex'

set xlabel 'Gate-Source-Spannung $U_{GS}$ $[\si{\volt}]$'
set ylabel 'Drain-Strom $I_D$ $[\si{\milli\A}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange[-4:]
set yrange[-2:10]
set ytics 2

load 'fitparameter.dat'
f(a,b,x) = a*x + b
fit[-2.5-0.1:-2.5+0.1] f(a1,a2,x) 'eingangskennlinie.txt' using (-$3):($1*1e3) via a1,a2
fit[-1.5-0.1:-1.5+0.1] f(b1,b2,x) 'eingangskennlinie.txt' using (-$3):($1*1e3) via b1,b2
fit[-0.5-0.1:-0.5+0.1] f(c1,c2,x) 'eingangskennlinie.txt' using (-$3):($1*1e3) via c1,c2
g(a,b,c,x) = a * ( 1 + x/b )**2 + c
fit[-3:] g(d1,d2,d3,x) 'eingangskennlinie.txt' using (-$3):($1*1e3) via d1,d2,d3

s05 = 2 * d1/d2 * (1 + (-0.5)/d2)
s15 = 2 * d1/d2 * (1 + (-1.5)/d2)
s25 = 2 * d1/d2 * (1 + (-2.5)/d2)
update 'fitparameter.dat'

plot \
'eingangskennlinie.txt' using (-$3):($1*1e3) with lines linetype 1 linecolor 3 title '$I_D(U_{GS})$', \
g(d1,d2,d3,x) with lines linetype 2 linecolor 3 title '$\mathcal{F}_{a,b,c}(U_{GS})$', \
f(a1,a2,x) with lines linetype 2 linecolor 1 title '$\mathcal{F}_{\mathcal{S}_1}(\SI{-2.5}{\volt})$', \
f(b1,b2,x) with lines linetype 3 linecolor 1 title '$\mathcal{F}_{\mathcal{S}_2}(\SI{-1.5}{\volt})$', \
f(c1,c2,x) with lines linetype 4 linecolor 1 title '$\mathcal{F}_{\mathcal{S}_3}(\SI{-0.5}{\volt})$'
