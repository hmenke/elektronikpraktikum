#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.7
set pointsize 0.5
set output 'Ausgangskennlinie.tex'

set xlabel 'Drain-Source-Spannung $U_{DS}$ $[\si{\volt}]$'
set ylabel 'Drain-Strom $I_D$ $[\si{\milli\A}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set yrange [0:]
set ytics 2

plot \
'<sed -e "1,22d" ausgangskennlinie_0V.txt' using 3:($1*1e3) with lines linetype 1 linecolor 3 title '$U_\textrm{G} = \SI{0}{\V}$', \
'<sed -e "1,22d" ausgangskennlinie_0.83V.txt' using 3:($1*1e3) with lines linetype 2 linecolor 3 title '$U_\textrm{G} = \SI{-0.83}{\V}$', \
'<sed -e "1,22d" ausgangskennlinie_1.96V.txt' using 3:($1*1e3) with lines linetype 3 linecolor 3 title '$U_\textrm{G} = \SI{-1.96}{\V}$'
