#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.7
set pointsize 0.5
set output 'Ausgangskennlinie.tex'

set xlabel 'Kollektor-Emitter-Spannung $U_{CE}$ $[\si{\volt}]$'
set ylabel 'Kollektorstrom $I_C$ $[\si{\milli\A}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set yrange [0:]
set ytics 2

plot \
'<sed -e "1,22d" ausgangskennlinie3microA.txt' using 3:($1*1e3) with lines linetype 1 linecolor 3 title '$I_B = \SI{3}{\micro\A}$, $U_\textrm{St} = \SI{0.74}{\V}$', \
'<sed -e "1,22d" ausgangskennlinie10microA.txt' using 3:($1*1e3) with lines linetype 2 linecolor 3 title '$I_B = \SI{10}{\micro\A}$, $U_\textrm{St} = \SI{1.0}{\V}$', \
'<sed -e "1,22d" ausgangskennlinie30microA-bis0.5V.txt' using 3:($1*1e3) with lines linetype 3 linecolor 3 title '$I_B = \SI{30}{\micro\A}$, $U_\textrm{St} = \SI{1.71}{\V}$', \
'<sed -e "1,22d" ausgangskennlinie30microA-ab0.5V.txt' using 3:($1*1e3) with lines linetype 3 linecolor 3 notitle
