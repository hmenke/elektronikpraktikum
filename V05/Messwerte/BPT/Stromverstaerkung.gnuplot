#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Stromverstaerkung.tex'

set xlabel 'Basisstrom $I_B$ $[\si{\milli\A}]$'
set ylabel 'Kollektorstrom $I_C$ $[\si{\milli\A}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange [0:]
set yrange [0:]
set xtics 0.04
set ytics 10

load 'fitparameter.dat'
f(x) = a*x
fit f(x) 'verstärkung.txt' using ($2*1e3):($1*1e3) via a
delta_a = 100*a_err/a
update 'fitparameter.dat'

plot \
'verstärkung.txt' using ($2*1e3):($1*1e3) with points pointtype 1 linecolor 3 title '$I_C(I_B)$', \
f(x) linetype 1 linecolor 1 title 'Fit $\mathcal{F}_\mathcal{B}(I_B)$'
