#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Leistung.tex'

set xlabel 'Basisspannung $U_B$ $[\si{\volt}]$'
set ylabel 'Leistung $P_V$ $[\si{\watt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25
set grid linetype 3 linecolor 0
set fit errorvariables
set xrange [0:]
set ytics 0.1

set arrow nohead from first 0.6,graph 0 to first 0.6,graph 1

plot \
'verstärkung.txt' using ($3):(10*$1) with points pointtype 1 linecolor 3 title '$P_V(I_B)$'
