#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Phase.tex'

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Phase $\Delta \phi$ $[\si{\degree}]$'
set format '\num[detect-all]{%g}'
set format x '\num[detect-all]{e%L}'

set key below maxrows 2 spacing 1.25 width 7
set grid linetype 3 linecolor 0
set logscale x
set fit errorvariables
set ytics 30

plot \
'Durchlasskurve.dat' using 1:3 with points pointtype 6 linecolor 3 title 'Phasengang'
