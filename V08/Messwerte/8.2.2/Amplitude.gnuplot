#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Amplitude.tex'

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Verstärkung $V$ $[\si{\deci\bel}]$'
set format '\num[detect-all]{%L}'
set format x '\num[detect-all]{e%L}'

set key below maxrows 2 spacing 1.25 width 7
set grid linetype 3 linecolor 0
set logscale xy
set fit errorvariables
set yrange [:5]
#set ytics 1

load 'fitparameter.dat'
w(x) = 2*pi*x
f(x) = w(x)*a*b/sqrt((1 - a**2 * w(x)**2)**2 + ((3-b)*a*w(x))**2)
fit f(x) 'Durchlasskurve.dat' using 1:2 via a,b
V0 = b
omega0 = 1/a
nu0 = 1/(2*pi*a)
update 'fitparameter.dat'

plot \
'Durchlasskurve.dat' using 1:2 with points pointtype 6 linecolor 3 title 'Frequenzgang', \
f(x) with lines linecolor 1 linetype 2 title 'Fit $\mathcal{F}_{a,b}(\nu)$'
