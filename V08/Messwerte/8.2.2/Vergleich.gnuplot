#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Vergleich.tex'

set xlabel 'Frequenz $\nu$ $[\si{\Hz}]$'
set ylabel 'Verstärkung $V$ $[\si{\deci\bel}]$'
set format '\num[detect-all]{%L}'
set format x '\num[detect-all]{e%L}'

set key below maxrows 2 spacing 1.25 width -4
set grid linetype 3 linecolor 0
set logscale xy
set fit errorvariables
set yrange [:100]
#set ytics 1

load 'fitparameter.dat'
a1 = a
b1 = b
load '../8.2.1/fitparameter.dat'
a2 = a
b2 = b

w(x) = 2*pi*x
f(x,a,b) = w(x)*a*b/sqrt((1 - a**2 * w(x)**2)**2 + ((3-b)*a*w(x))**2)

plot \
'Durchlasskurve.dat' using 1:2 with points pointtype 6 linecolor 3 title 'entdämpft', \
f(x,a1,b1) with lines linecolor 1 linetype 2 title 'Fit $\mathcal{F}_\textnormal{entdämpft}(\nu)$', \
'../8.2.1/Durchlasskurve.dat' using 1:2 with points pointtype 7 linecolor 3 title 'gedämpft', \
f(x,a2,b2) with lines linecolor 1 linetype 3 title 'Fit $\mathcal{F}_\textnormal{gedämpft}(\nu)$'
