#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Sine.tex'

set xlabel 'Zeit $t$ $[\si{\micro\s}]$'
set ylabel 'Eingangsspannung $U_E$ $[\si{\milli\volt}]$'
set ytics nomirror
set y2label 'Ausgangsspannung $U_A$ $[\si{\volt}]$'
set y2tics
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25 width 7
set grid linetype 3 linecolor 0
set fit errorvariables

plot \
'Begrenzung.dat' using ($1*1e6):($5*1e3) with lines axis x1y1 linetype 1 linecolor 1 title '$U_E \equiv U[\mathrm{CH1}]$', \
'Begrenzung.dat' using ($1*1e6):4 with lines axis x1y2 linetype 1 linecolor 3 title '$U_A \equiv U[\mathrm{CH2}]$'
