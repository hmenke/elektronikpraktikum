\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V08: Filterschaltungen mit Operationsverstärkern}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Kim Kafenda}\noaffiliation
\date{9.\ Dezember 2013}

\begin{abstract}
	In diesem Versuch wurden differenzierende und integrierende Schaltungen mit Operationsverstärkern untersucht. Außerdem werden Bandpassfilter untersucht, mit denen sich bestimmt Frequenzen aus einem Signal filtern lassen. Verschiedene Schwingungsformen werden bei diversen Dämpfungen betrachtet. Zuletzt wird eine Fourieranalyse verschiedener Signal durchgeführt.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

\subsection{Oszillatoren}

Aus der Mechanik ist der getriebene, gedämpfte harmonische Oszillator bekannt in der Form \cite[S.~357]{Demtroeder:Experimentalphysik1}
%
\begin{equation*}
	m \ddot{x} + b \dot{x} + D x = F_0 \cdot \cos(\omega t)
\end{equation*}
%
Um die Form zu vereinfachen führt man sinnvolle Abkürzungen ein
%
\begin{equation*}
	\gamma \equiv \frac{b}{2 m} \; , \quad \omega_0^2 \equiv \frac{D}{m} \; , \quad K \equiv \frac{F_0}{m}
\end{equation*}
%
Damit lautet die Bewegungsgleichung des harmonischen Oszillators
%
\begin{equation*}
	\ddot{x} + 2 \gamma \dot{x} + \omega_0^2 x = K \cdot \cos(\omega t)
\end{equation*}

Die allgemeine Lösung dieser Gleichung lautet
%
\begin{equation*}
	x(t) = A_1 \mathrm{e}^{-\gamma t} \cos(\omega_1 t + \varphi) + A_2 \cos(\omega t + \varphi)
\end{equation*}
%
Nach dem Einschwingvorgang, der mit $\mathrm{e}^{-\gamma t}$ abklingt reduziert sich die Lösung auf
%
\begin{equation*}
	x(t) = A_2(\omega) \cos(\omega t + \varphi)
\end{equation*}

Setzen wir diesen Ansatz in die Gleichung ein, so erhalten wir für den Phasenfaktor
%
\begin{equation*}
	\tan \varphi = - \frac{2 \gamma \omega}{\omega_0^2 - \omega^2}
\end{equation*}
%
und für die frequenzabhängige Amplitude
%
\begin{equation*}
	A_2(\omega) = \frac{K}{\sqrt{(\omega_0^2 - \omega^2)^2 + (2 \gamma \omega)^2}}
\end{equation*}

Für die Resonanzfrequenz $\omega_0$ hat die Amplitude $A_2(\omega_0)$ ein Maximum, dessen Höhe von der Dämpfung $\gamma$ abhängt. Ohne Dämpfung ($\gamma = 0$) würde die Amplitude an diesem Maximum divergieren (Resonanzkatastrophe).

\subsection{Elektrische Oszillatoren}

Ein Schwingkreis besteht aus einem Widerstand (Dämpfung), einem Kondensator (Rückstellkraft) und einer Spule (Träges Moment), die in Reihe geschaltet sind. Die Differentialgleichung dieses Systems lautet \cite[S.~174]{Demtroeder:Experimentalphysik2}
%
\begin{equation*}
	L  \cdot \ddot{I} + R \cdot \dot{I} + \frac{1}{C} \cdot I = \dot{F}_0(t)
\end{equation*}

Ein elektrischer Schwingkreis ist im Allgemeinen zur Rückkopplung fähig, siehe dazu auch Abbildung~\ref{fig:oszi-schema}. Damit eine Oszillation zustande kommen kann muss folgende Bedingung erfüllt sein
%
\begin{equation*}
	V \cdot k = 1
\end{equation*}
%
wobei $V$ die Verstärkung und $k$ die Dämpfung durch die Rückkopplung ist.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[draw,circle,label={below:{Verstärkung $V$}}] (amp) {\tikz\node[op amp,scale=0.5] {};};
		\draw (amp.west) to[short]
		++(-0.5,0) to[short]
		++(0,1.5) to[generic,l={Rückkopplung $k$}]
		++(3,0) to[short,-*]
		++(0,-1.5) to[short]
		(amp.east)
		(amp.east) to[short,-o]
		++(1,0) node[right] {$A$};
	\end{tikzpicture}
	\caption{Schematisches Schaltbild eines Oszillators.}
	\label{fig:oszi-schema}
\end{figure}

\subsection{Filterschaltungen}

\subsubsection{Differenzierer und Integrierer}
\nocite{EP:Skript}

Beschaltet man einen Operationsverstärker nicht nur mit ohmschen Widerständen, sondern auch mit einem Kondensator, so kann man je nach Position der Kapazität \emph{differenzierende} oder \emph{integrierende} Eigenschaften hervorrufen.

Schaltet man den Kondensator vor den invertierenden Eingang des Operationsverstärkers als Eingangswiderstand und einen ohmschen Widerstand als Rückkopplungswiderstand, so erhält man einen \emph{Hochpass}. Dieser hat \emph{differenzierende} Eigenschaften. Für die Übertragungsfunktion gilt
%
\begin{equation*}
	U_A = - R C \left( \frac{\diff}{\diff t} U_E \right)
\end{equation*}
zur Illustration vergleiche mit Abbildung~\ref{fig:Differenzierer}.

Vertauscht man die Position von ohmschem Widerstand und Kondensator, also den ohmschen Widerstand als Eingangswiderstand und den Kondensator als Rückkopplungswiderstand, so erhält man einen \emph{Tiefpass}. Dieser hat \emph{integrierende} Eigenschaften. Für dessen Übertragungsfunktion gilt dabei
%
\begin{equation*}
	U_A = - \frac{1}{R C} \int U_E \diff t
\end{equation*}
%
zur Illustration vergleiche mit Abbildung~\ref{fig:umkehrIntegrierer}.

\subsubsection{Bandpassfilter}

Ein Bandpassfilter oder auch selektiver Filter besitzt für verschiedene Eingangsfrequenzen verschiedene Verstärkungen. Ein beispielhaftes Schaltbild eines Bandpassfilters ist in Abbildung~\ref{fig:bandpass} dargestellt.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp,yscale=-1] (opamp) {}
		(opamp.+) to[short,-*]
		++(-0.5,0) coordinate (R to ground) to[C,l_=$C$,-*]
		++(-1.5,0) node (C to ground){} to[R=$R$,-o]
		++(-1.5,0) node[left] {$E$}
		(R to ground) to[R,l_=$2R$] ++(0,-2) node[ground] {}
		(C to ground) to[C,l_=$C$] ++(0,-2) node[ground] {}
		(opamp.out) to[short,*-o] ++(1,0) node[right] {$A$}
		(opamp.out) to[R=$(V-1)R_1$,*-*] ++(0,-1.5) coordinate (h)
		(h) to[R=$R_1$,*-] ++(0,-1.5) node[ground] {}
		let \p1 = (h), \p2 = (opamp.-) in
			(\p2) to[short] (\x2,\y1) to[short] (\p1)
		let \p1 = (opamp.out), \p2 = (C to ground) in
			(\p1) to[short]
			(\x1,\y1+2cm) to[R=$R$]
			(\x2,\y1+2cm) to[short]
			(\p2)
		node[above left of=C to ground,draw,circle] (one) {$1$}
		(one) edge[out=-20,in=110,-to] (C to ground);
	\end{tikzpicture}
	\caption{Schaltbild eines Bandpassfilters.}
	\label{fig:bandpass}
\end{figure}

Verändert man den Betrag der komplexen Verstärkung, so wirkt sich dies auf die Amplitude des Ausgangssignal aus, während eine Veränderung der Phase eine Phasenverschiebung zur Folge hat.

Die frequenzabhängige Übertragungsfunktion des Bandpasses lautet
%
\begin{equation*}
	\begin{split}
		U_A
		&= U_E \cdot \frac{\mathrm{j} \omega R C V}{1 + (3 - V) \mathrm{j} \omega R C - \omega^2 (R C)^2} \\
		&= U_E \cdot \frac{\mathrm{j} \omega \tau V}{1 + (3 - V) \mathrm{j} \omega \tau - \omega^2 \tau^2}
	\end{split}
\end{equation*}
%
mit $\tau = R C = \omega_0^{-1}$. Für die reelle Verstärkung gilt demnach
%
\begin{equation}
	\begin{split}
		V_{SF} = \left|\frac{U_A}{U_E}\right|(\omega)
		&= \frac{\omega \tau V}{\sqrt{(1 - \omega^2 \tau^2)^2 + ((3 - V) \omega \tau)^2}}
	\end{split}
	\label{eq:uebertrag}
\end{equation}

Ist $\omega$ klein, insbesondere $\omega \ll \omega_0$, so gilt die Proportionalitätsbedingung $V \propto \omega$, für $\omega \gg \omega_0$ gilt hingegen $V \propto \omega^{-1}$.
Die Phasenverschiebung bewegt sich zwischen $\pi/2$ für $\omega \ll \omega_0$ und $-\pi/2$ für $\omega \gg \omega_0$.
Resonanz tritt auf bei $\omega = \omega_0 = 1/\tau$.

\subsubsection{Kenngrößen elektrischer Oszillatoren}
Bei der Resonanzfrequenz erfährt das Eingangssignal die \emph{maximale Verstärkung}
%
\begin{equation*}
	V_\mathrm{max} = V_{SF}(\omega_0) = \frac{V}{3 - V}
\end{equation*}
%
dabei entspricht $V = 3$ einer verschwindenden Dämpfung und führt zur Resonanzkatastrophe.

Die \emph{Bandbreite} $\Delta \omega$ des Oszillators ist definiert als das FWHM der Übertragungsfunktion~\eqref{eq:uebertrag}. Eine kurze Rechnung offenbart
%
\begin{equation*}
	\Delta \omega = \frac{3 - V}{\tau} = \omega_0 \cdot (3 - V)
\end{equation*}

Die \emph{Güte} eines Resonators gibt die Frequenzselektivität an oder einfacher gesagt das Frequenzauflösungsvermögen. Resonatoren mit hoher Güte können also einzelne Frequenzen besser auflösen. Die Güte schreibt sich in Formeln
%
\begin{equation*}
	Q = \frac{\omega}{\omega_0} = \frac{1}{3 - V}
\end{equation*}
%
Das heißt, je geringer die Bandbreite, desto höher die Güte.

\subsection{Fouriertransformation}

Ist $f(t) \in L^1(\mathbb{R}^n)$, dann gilt für die zeitkontinuierliche Fouriertransformierte
%
\begin{align*}
	\mathcal{F}[f](\omega) = \frac{1}{\sqrt{(2 \pi)^n}} \int_{\mathbb{R}^n} f(t) \, \mathrm{e}^{-\mathrm{j} \omega t} \diff t
\end{align*}

Ist nun auch $\mathcal{F}[f](\omega) \in L^1(\mathbb{R}^n)$, so existiert die zeitkontinuierliche Fourierrücktransformierte
%
\begin{align*}
	f(t) = \mathcal{F}^{-1}\left[ \mathcal{F}[f] \right](t) = \frac{1}{\sqrt{(2 \pi)^n}} \int_{\mathbb{R}^n} \mathcal{F}[f](\omega) \, \mathrm{e}^{-\mathrm{j} \omega t} \diff \omega
\end{align*}

Bei Messreihen handelt es sich jedoch meist um zeitdiskrete Signale, die nicht so transformiert werden können. Deshalb bedient man sich der diskreten Fouriertransformation (DFT). Sei dazu $f_k = f(t_k) \in \mathbb{C}$ an den diskreten Zeitpunkten $t_k$ \cite[S.~39]{PhaC:Skript}.
%
\begin{align*}
	\mathrm{DFT}(f_k)_n &= g_n = \sum_{k=0}^{N-1} f_k \mathrm{e}^{-\mathrm{j} \frac{2\pi}{N} n k} \\
	\mathrm{iDFT}(g_n)_k &= f(t_k) = \sum_{n=0}^{N-1} \frac{g_n}{N} \mathrm{e}^{\mathrm{j} \frac{2\pi}{N} n k}
\end{align*}
%
Dieses Verfahren ist von der Ordnung $\mathcal{O}(N^2)$, was für viele Datenpunkte von Nachteil ist.

Deshalb bedient man sich meist der \emph{Fast Fourier Transform} (FFT) nach Cooley und Tukey, welche für eine Anzahl von Datenpunkten $N = 2 M$ das Verfahren auf die Ordnung $\mathcal{O}(N \log N)$ reduzieren kann.

\section{Versuchsaufbau und -durchführung}
Alle Versuchsteile werden mit dem Operationsverstärker OP27G durchgeführt.

\subsection{Umkehr-Differenzierer}
Die Schaltung wird nach ABB.~\ref{fig:Differenzierer} aufgebaut.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-0.5) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E$] (lu) to[C=1<\nano\farad>,o-*] (opamp.-)
		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.out)+(0,1.5) to[R,l_=10<\kilo\ohm>] (rn)
		(opamp.out)+(0,1.5) to[short] (opamp.out)
		(opamp.out) to[short,*-o] (2.5,0) coordinate[label=right:$A$] (ru);
	\end{tikzpicture}
	\caption{Schaltbild des Umkehr-Differenzierers.}
	\label{fig:Differenzierer}
\end{figure}

Am Frequenzgenerator wird ein Sinussignal mit $U=\SI{100}{\milli\V_{pp}}$ und einer Frequenz von \SI{500}{\Hz} eingestellt. Die Frequenz wird manuell hochgedreht und am Oszilloskop wird das Ein- und Ausgangssignal beobachtet. Dann wird die Auswirkung eines reduzierten Eingangspegels, sowie eines erhöhten Innenwiderstands der Spannungsquelle untersucht. 

Ebenso verfährt man mit einer Rechtecks- und Dreiecksspannung, mit einer Symmetrie von 50\%.

\subsection{Umkehr-Integrator}
Man erhält den Umkehr-Integrator, indem man Widerstand und Kondensator vertauscht und außerdem einen zweiten Widerstand parallel zum Kondensator schaltet.
Das Eingangssignal soll nun eine Spannung von $U=\SI{5}{\V_{pp}}$ besitzen.
Es werden die selben Einflüsse wie im vorherigen Versuchsteil untersucht.
Außerdem wird die Funktion des parallelgeschalteten Widerstands analysiert.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[>=triangle 45]
		\draw (0,0) node[op amp] (opamp) {}
		(opamp.+)+(0,-0.5) node[ground] (ground) {} to[short] (opamp.+)
		(opamp.-)+(-2,0) coordinate[label=left:$E$] (lu) to[R=10<\kilo\ohm>,o-*] (opamp.-)
		(opamp.-)+(0,1) coordinate (rn) to[short] (opamp.-)
		(opamp.-)+(0,2) coordinate (rh) to[short,-*] (rn)
		(opamp.out)+(0,1.5) to[R=1<\mega\ohm>] (rn)
		(opamp.out)+(0,2.5) to[C,l_=1<\nano\farad>] (rh)
		(opamp.out)+(0,2.5) to[short,-*]
		($(opamp.out)+(0,1.5)$) to[short] (opamp.out)
		(opamp.out) to[short,*-o] (2.5,0) coordinate[label=right:$A$] (ru);
	\end{tikzpicture}
	\caption{Schaltbild des Umkehr-Integrierers.}
	\label{fig:umkehrIntegrierer}
\end{figure}

\subsection{Selektiver Filter}
\subsubsection{Bandbreite eines aktiven Filters}
Der selektive Filter lässt sich mit der Schaltung in
ABB.~\ref{fig:Selektiv} realisieren. Dabei sind die Widerstände auf \SI{100}{\kilo \ohm} und Kondensatoren alle auf \SI{1}{\nano\farad} dimensioniert.
Mit Hilfe des Messprogrammes LabVIEW wird die Frequenzabhängigkeit der Verstärkung und der Phasenverschiebung zwischen Ein- und Ausgang aufgezeichnet. Aus den Daten soll die Resonanzfrequenz, sowie die Dämpfung und Halbwertsbreite bestimmt werden.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[scale=0.95,>=triangle 45]
		\draw (0,0) node[op amp,yscale=-1] (opamp) {}
		(opamp.-) to[short] ++(0,-0.7) -| (opamp.out)
		(opamp.+) to[short,-*] ++(-2,0) to[C=1<\nano\farad>,-*] ++(-2,0) to[R=100<\kilo\ohm>,-o] ++(-2,0)
		(opamp.+)++(-2,0) to[R=200<\kilo\ohm>] ++(0,-2) node[ground] {}
		(opamp.+)++(-4,0) to[C,l_=1<\nano\farad>] ++(0,-2) node[ground] {}
		let \p1=($(opamp.+)+(4,1)$), \p2=(opamp.out) in
		(opamp.+)++(-4,0) to[short] ++(0,1) to[R=100<\kilo\ohm>] (\x2,\y1) to[short,-*] (\p2)
		(opamp.out) to[short,-o] ++(0.5,0);
	\end{tikzpicture}
	\caption{Schaltbild des selektiven Filters.}
	\label{fig:Selektiv}
\end{figure}

Nun wird der Filter nach ABB.~\ref{fig:Entdaempft} entdämpft. Für $R_1$ wird eine Widerstandsdekade eingebaut und $R_2 = \SI{10}{\kilo\ohm}$. Es wird eine kleine Signalamplitude von $U = \SI{25}{\milli\V_{pp}}$ eingestellt.

\begin{figure}[tpbh]
	\centering
	\begin{tikzpicture}[scale=0.95,>=triangle 45]
		\draw (0,0) node[op amp,yscale=-1] (opamp) {}
		(opamp.+) to[short,-*] ++(-2,0) to[C=1<\nano\farad>,-*] ++(-2,0) to[R=100<\kilo\ohm>,-o] ++(-2,0)
		(opamp.+)++(-2,0) to[R=200<\kilo\ohm>] ++(0,-2) node[ground] {}
		(opamp.+)++(-4,0) to[C,l_=1<\nano\farad>] ++(0,-2) node[ground] {}
		let \p1=($(opamp.+)+(4,1)$), \p2=(opamp.out) in
		(opamp.+)++(-4,0) to[short] ++(0,1) to[R=100<\kilo\ohm>] (\x2,\y1) to[short,-*] (\p2)
		(opamp.out) to[short,-o] ++(0.5,0);
		\draw let \p1 = (opamp.-), \p2 = ($(opamp.out)+(0,-1.5)$) in
		(\p1) to[short] (\x1,\y2) to[short,-*] (\x2,\y2)
		(opamp.out) to[R=$R_2$] (\p2) to[R=$R_1$] ++(0,-1.5) node[ground] {};
	\end{tikzpicture}
	\caption{Schaltbild der Entdämpfung des selektiven Filters.}
	\label{fig:Entdaempft}
\end{figure}

Der Widerstand $R_1$ wird so weit herunter gedreht, bis die Schaltung zu schwingen beginnt. 
Anschließend wird der Widerstand etwas höher gewählt, sodass die Schaltung noch nicht zu schwingen beginnt, und mit LabVIEW der Amplituden- und Phasengang gemesssen.

\subsubsection{Untersuchung eines Signals mit dem selektiven Filter}
Es soll die spektrale Zusammensetzung eines Sinus-, Dreieck- und Rechtecksignals bestimmt werden. Dazu wird der im vorherigen Versuchsteil aufgebaute Frequenzfilter verwendet, indem die Frequenz des abzutastenden Signals variiert wird. Man beginnt bei der Grundfrequenz und reduziert die Frequenz dann, bis das nächste Maximum der Ausgangsspannung auf dem Oszilloskop erscheint. So werden die Frequenzen der ersten drei Oberschwingungen bestimmt. 
Abschließend wird mit LabVIEW der Amplituden- und Phasengang der Rechteckspannung aufgenommen.

\section{Messwerte}

\subsection{Untersuchung eines Signals mit dem selektiven Filter}
Die manuell ermittelten Oberschwingungen


\section{Formeln}
Die folgenden Zeichen und Einheiten wurden im Weiteren verwendet
%
\begin{itemize}
	\item $V$ ohne Einheit: Verstärkung des selektiven Filters
	\item $V^*$ ohne Einheit: Tatsächliche (frequenzabhängige) Verstärkung
	\item $\hat{V}^*$ ohne Einheit: Das globale Maximum von $V^*$
	\item $U_{A,E}$ in Volt [\si{\V}]: Ein- bzw. Ausgangsspannung
	\item $\omega$ in 1 pro Sekunde [\si{\per \s}]: Die Kreisfrequenz des Eingangssignals
	\item $\omega_0$ in 1 pro Sekunde [\si{\per \s}]: Resonanzkreisfrequenz des Filters
	\item $\tau$ in Sekunden [\si{\s}]: Inverse Resonanzkreisfrequenz des Filters
	\item $\Delta \omega$ in 1 pro Sekunde [\si{\per \s}]: Bandbreite des Filters
	\item $Q$ ohne Einheit: Güte des Filters
		
	\item $\nu$ in Hertz [\si{\Hz}]: Frequenz des Eingangssignals
	\item $\nu_0$ in Hertz [\si{\Hz}]: Resonanzfrequenz des Filters
\end{itemize}

\begin{description}
	\item[Übertragungsfunktion] Die komplexe Übertragungsfunktion des Bandpasses lautet
		\begin{equation}
			U_A = U_E \frac{\mathrm{j} \omega \tau V}{1 + (3 - V) \mathrm{j} \omega \tau - \omega^2 \tau^2}
			\label{eq:Uebertrag-komplex}
		\end{equation}
		%
		Daraus ergibt sich die reelle Verstärkung als
		%
		\begin{equation}
			V^* = \left| \frac{U_A}{U_E} \right| = \frac{\omega \tau V}{\sqrt{(1 - \omega^2 \tau^2)^2 + ((3 - V) \omega \tau)^2}}
			\label{eq:Uebertrag-reell}
		\end{equation}

	\item[Maximale Verstärkung] Der Realteil der maximalen Verstärkung ergibt sich zu
		\begin{equation}
			\hat{V}^* = V^* = \frac{V}{3 - V}
			\label{eq:Vmax}
		\end{equation}

	\item[Bandbreite] Die Bandbreite des selektives Filters ist gegeben durch
		\begin{equation}
			\Delta \omega = \frac{3 - V}{\tau} = (3 - V) \omega_0
			\label{eq:Bandbreite}
		\end{equation}
		Diese ist auch durch die Halbwertsbreite der Resonanzkurve gegeben.

	\item[Güte] Die Güte ist das Maß für die Dämpfung eines Resonators
		\begin{equation}
			Q = \frac{\omega_0}{\Delta \omega} = \frac{1}{3 - V}
			\label{eq:Guete}
		\end{equation}
\end{description}

\section{Auswertung}
\subsection{Umkehr-Differenzierer}
Bei einer Eingangsamplitude von $U= \SI{100}{\milli\V_{pp}}$ erfüllt die Schaltung ihre Funktion als Differenzierer bis zu einer Frequenz von ungefähr \SI{21}{\kilo\Hz}, an dieser Frequenz wird die Betriebsspannung des OpAmps erreicht, somit geht auch die differenzierende Eigenschaft in den Bereichen der Sättigung verloren, zu sehen ist dieser Effekt in ABB.~\ref{fig:Sine} an den Plateaus der Ausgangsspannung. Unter einem Eingangspegel von \SI{74}{\milli\V_{pp}} wird die Betriebsspannung nicht mehr erreicht. Die Ausgangsspannung sinkt dann jedoch ab einer Frequenz von ungefähr \SI{20}{\kilo\Hz}.

Der Innenwiderstand der Quelle führt zu einer Phasenverschiebung zwischen Eingangs- und Ausgangssignal, wodurch die differenzierende Eigenschaft verloren geht.

Die Eigenschaften des Umkehr-Differenzierer konnten auch mit Rechteck- und Dreiecksspannung bestätigt werden, siehe ABB.~\ref{fig:Square} und \ref{fig:Ramp}.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.1.1/}{Sine.tex}
	\caption{Wirkung des Umkehr-Differenzierers auf ein Sinussignal mit $U_\textnormal{pp} = \SI{100}{\milli\V_{pp}}$ und $\nu \approx \SI{21}{\kilo\Hz}$}
	\label{fig:Sine}
\end{figure}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.1.1/}{Square.tex}
	\caption{Wirkung des Umkehr-Differenzierers auf ein Rechtecksignal mit $U_\textnormal{pp} = \SI{100}{\milli\V_{pp}}$ und $\nu < \SI{21}{\kilo\Hz}$}
	\label{fig:Square}
\end{figure}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.1.1/}{Ramp.tex}
	\caption{Wirkung des Umkehr-Differenzierers auf ein Dreiecksignal mit $U_\textnormal{pp} = \SI{100}{\milli\V_{pp}}$ und $\nu < \SI{21}{\kilo\Hz}$}
	\label{fig:Ramp}
\end{figure}

\subsection{Umkehr-Integrierer}
In ABB.~\ref{fig:umkehrSine}, \ref{fig:umkehrSquare} und \ref{fig:umkehrRamp} ist das Ausgangssignal des Integrierers für verschiedene Eingangssignalformen zu sehen. Im Gegensatz zum Differenzierer wird der Integrierer bei niedrigen Frequenzen durch die Betriebsspannung limitiert. Bei einer größeren Wellenlänge, steigt die Fläche unter den Kurven und somit das Ausgangssignal. Bei großen Frequenzen wird somit das Ausgangssignal immer kleiner, sodass es irgendwann im Rauschen untergeht.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.1.2/}{Sine.tex}
	\caption{Wirkung des Umkehr-Integrierers auf ein Sinussignal mit $U_\textnormal{pp} = \SI{5}{\V_{pp}}$}
	\label{fig:umkehrSine}
\end{figure}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.1.2/}{Square.tex}
	\caption{Wirkung des Umkehr-Integrierers auf ein Rechtecksignal mit $U_\textnormal{pp} = \SI{5}{\V_{pp}}$}
	\label{fig:umkehrSquare}
\end{figure}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.1.2/}{Ramp.tex}
	\caption{Wirkung des Umkehr-Integrierers auf ein Dreiecksignal mit $U_\textnormal{pp} = \SI{5}{\V_{pp}}$}
	\label{fig:umkehrRamp}
\end{figure}

Der zum Kondensator parallel geschaltete Widerstand, dient dazu den Kondensator zu entladen. Wird der Widerstand entfernt und der Eingang der Schaltung auf Masse gelegt, wird erwartet den Offsetstrom zu messen. Dieser konnte jedoch nicht nachgewiesen werden, da der verwendete Operationsverstärker zu gut optimiert ist und keinen Offsetstrom aufweist.

\subsection{Selektiver Filter}
\subsubsection{Bandbreite eines aktiven Filters}
Der Frequenzabhängigkeit der Verstärkung und der Phase ist für den Frequenzbereich von \SI{100}{\Hz} bis \SI{100}{\kilo\Hz} in ABB.~\ref{fig:sin verstärkung} und \ref{fig:sin phase} zu sehen.
%
\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.2.1/}{Amplitude.tex}
	\caption{Frequenzabhängigkeit der Verstärkung des selektiven Filters}
	\label{fig:sin verstärkung}
\end{figure}
%
\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.2.1/}{Phase.tex}
	\caption{Frequenzabhängigkeit der Phasenverschiebung des selektiven Filters}
	\label{fig:sin phase}
\end{figure}
%
Die Übertragungsfunktion des Bandpasses für die reelle Verstärkung ist gegeben durch Gleichung~\ref{eq:Uebertrag-reell}, somit wurde eine Fitfunktion der Form
\[
	f(x) = \frac{a b \omega}{\sqrt{(1-a^2 \omega^2)^2 + ((3-b) a \omega)^2}}
\]
an die Messwerte in ABB.~\ref{fig:sin verstärkung} angepasst. Vergleicht man den Fit mir Gleichung~\ref{eq:Uebertrag-reell}, so führen die Fitparameter auf 
%
\begin{itemize}
	\item $V_0 = \num{0.9942}$
	\item $\omega_0 = \SI{10108}{\per\s}$ 
	\item $\nu_0 = \SI{1608}{\Hz}$
\end{itemize}
%
Die Halbwertbreite ergibt sich nach Gleichung~\ref{eq:Bandbreite} zu
\[
	\Delta \omega = (3-V)\omega_0 = \SI{20276}{\per\s}
\]
Nach Gleichung~\ref{eq:Guete} ergibt sich für die Güte somit $Q = \num{0.499}$.

\subsubsection{Entdämpfung des Filters}
Durch Variation der Widerstandsdekade wurde ein Widerstand von $R_1 = \SI{4.89}{\kilo\ohm}$ ermittelt. Dies entspricht einer Verstärkung von
\[ V=1+\frac{R_2}{R_1} = 1+ \frac{\SI{10}{\kilo\ohm}}{\SI{4.89}{\kilo\ohm}} \approx 3.03 \]
ab welcher die Schaltung zu schwingen beginnt.
Bei einer etwas geringeren Verstärkung, also einem etwas größeren Widerstand $R_1$ ergibt sich der in ABB.~\ref{fig:entdämpft verstärkung} und \ref{fig:entdämpft phase} sichtbare Verstärkungs- und Phasengang.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.2.2/}{Amplitude.tex}
	\caption{Frequenzabhängigkeit der Verstärkung des ungedämpften selektiven Filters}
	\label{fig:entdämpft verstärkung}
\end{figure}

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.2.2/}{Phase.tex}
	\caption{Frequenzabhängigkeit der Phasenverschiebung des ungedämpften selektiven Filters}
	\label{fig:entdämpft phase}
\end{figure}

Vergleicht man die Verstärkungskurven des entgedämpften und gedämpften Filters, so fällt auf, dass der entdämpfte Filter eine deutliche größere Verstärkung $V = \num{2.9812}$ bei der Resonanzfrequenz $\nu = \SI{1616}{\Hz}$ von aufweist. Durch die fehlende Dämpfung kommt es zu einer Resonanzkatastrophe, wobei die maximale Ausgangsspannung jedoch von der Betriebsspannung begrenzt ist.
	
\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messwerte/8.2.2/}{Vergleich.tex}
	\caption{Vergleich der Übertragungskurven für den gedämpften und entdämpften Filter.}
	\label{fig:vergleich}
\end{figure}

Auch der Phasengang unterscheidet sich. Beim entdämpften Filter kommt es bei der Resonanzfrequenz zu einem Phasensprung von \SI{90}{\degree} zu \SI{-90}{\degree}, während diese Phasenverschiebung beim gedämpften Filter kontinuierlich auftritt.

\subsection{Untersuchung eines Signals mit dem selektiven Filter}

Mit Hilfe des selektiven Filters kann eine Fourieranalyse eines angelegten Signals durchgeführt werden. Dazu wird die Frequenz der Eingangssignals durchgefahren und das Ausgangssignal auf Resonanz beobachtet. Resonanz tritt genau dann auf, wenn die Frequenz des Eingangssignals genau der Durchlassfrequenz entspricht oder die Durchlassfrequenz eine höhere harmonische der Eingangsfrequenz ist. Man erhält also durch die Amplitude die Fourierkoeffizienten des Eingangssignals.

%Für ein Rechtecksignal treten Resonanzen auf bei \SI{1620}{\kilo\Hz}, \SI{540}{\kilo\Hz} und \SI{323}{\kilo\Hz}.
%Für ein Dreiecksignal treten Resonanzen auf bei \SI{1620}{\kilo\Hz}, \SI{807}{\kilo\Hz} und \SI{540}{\kilo\Hz}.

Für die Fourierkoeffizienten gelten die folgenden Zusammenhänge aus den jeweiligen Fourierreihen.
%
\begin{align*}
	f_\textnormal{Rechteck}(t) &= \frac{4h}{\pi}\left[\sin \omega t+\frac{1}{3}\sin3 \omega t+\frac {1}{5}\sin5 \omega t+ \cdots\right] \\
	f_\textnormal{Dreieck}(t) &= -\frac{2h}{\pi}\left[ {\sin {\omega t} - \frac{1}{2}\sin{2 \omega t} + \frac{1}{3}\sin {3 \omega t} \mp \cdots}\right]
\end{align*}
%
Demnach gilt für das Rechtecksignal $\omega_1 = \frac{1}{3} \omega_0$, $\omega_2 = \frac{1}{5} \omega_0$ und für das Dreiecksignal $\omega_1 = \frac{1}{2} \omega_0$, $\omega_2 = \frac{1}{3} \omega_0$.

Die gemessenen Resonanzen und die theoretische vorausgesagten sind für beide Signale in Tabelle~\ref{tab:resonanz} dargestellt.

\begin{table}
	\centering
	\begin{tabular}{SS}
		\toprule
		\multicolumn{2}{c}{\bfseries Rechtecksignal} \\
		{gemessen $[\si{\kilo\Hz}]$} & {theoretisch $[\si{\kilo\Hz}]$} \\
		\midrule
		1620 & {--} \\
		540 & 540 \\
		323 & 324 \\
		\toprule
		\multicolumn{2}{c}{\bfseries Dreiecksignal} \\
		{gemessen $[\si{\kilo\Hz}]$} & {theoretisch $[\si{\kilo\Hz}]$} \\
		\midrule
		1620 & {--} \\
		807 & 810 \\
		540 & 540 \\
		\bottomrule
	\end{tabular}
	\caption{Vergleich der gemessenen und der theoretische vorausgesagten Resonanzen.}
	\label{tab:resonanz}
\end{table}

Ein Vergleich zu den theoretischen Werten kann nicht vorgenommen werden, da keine Amplituden erfasst wurden.

\section{Fehlerbetrachtung}

Es werden die relativen Abweichungen der gemessenen Werte von den theoretischen bestimmt.

\subsection{Bandpass}

\begin{description}
	\item[Resonanzfrequenz] Die Resonanzfrequenz wurde gemessen mit $\nu_0 = \SI{1608}{\Hz}$. Der theoretische Werte beträgt $\hat{\nu}_0 = (2 \pi R C)^{-1} = \SI{1592}{\Hz}$. Damit ist die relative Abweichung
		\[ \mathcal{Q}[\nu_0,\hat{\nu}_0] = \frac{\SI{1592}{\Hz} - \SI{1608}{\Hz}}{\SI{1592}{\Hz}} = \SI{1.0}{\percent} \]

	\item[Verstärkung] Die Verstärkung wurde als $V = \num{0.9942}$ ermittelt. Die theoretische Verstärkung beträgt $\hat{V} = \num{1.0}$. Es ergibt sich also eine relative Abweichung von
		\[ \mathcal{Q}[V,\hat{V}] = \frac{\num{0.9942} - \num{1.0}}{\num{0.9942}} = \SI{0.6}{\percent} \]

	\item[Bandbreite] Als Bandbreite ergab sich aus der Messung $\Delta\omega = \SI{20276}{\per\s}$. Theoretisch sind erwartet $\Delta\hat{\omega} = (3-\hat{V}) (RC)^{-1} = \SI{20000}{\per\s}$. Also gilt für die relative Abweichung
		\[ \mathcal{Q}[\Delta\omega,\Delta\hat{\omega}] = \frac{\SI{20276}{\per\s} - \SI{20000}{\per\s}}{\SI{20276}{\per\s}} = \SI{1.3}{\percent} \]

	\item[Güte] Für die Güte konnte berechnet werden $Q = \num{0.499}$. Theoretisch vorausgesagt ist $\hat{Q} = \num{0.5}$.
		\[ \mathcal{Q}[Q,\hat{Q}] = \frac{\num{0.499} - \num{0.5}}{\num{0.499}} = \SI{0.2}{\percent} \]
\end{description}

\subsection{Entdämpfter Bandpass}

\begin{description}
	\item[Resonanzfrequenz] Die Resonanzfrequenz wurde gemessen mit $\nu_0 = \SI{1616}{\Hz}$. Der theoretische Wert beträgt $\hat{\nu}_0 = (2 \pi R C)^{-1} = \SI{1592}{\Hz}$. Damit ist die relative Abweichung
		\[ \mathcal{Q}[\nu_0,\hat{\nu}_0] = \frac{\SI{1592}{\Hz} - \SI{1616}{\Hz}}{\SI{1592}{\Hz}} = \SI{1.5}{\percent} \]

	\item[Verstärkung] Die Verstärkung wurde als $V = \num{2.9812}$ ermittelt. Die theoretische Verstärkung beträgt $\hat{V} = \num{3.0}$. Es ergibt sich also eine relative Abweichung von
		\[ \mathcal{Q}[V,\hat{V}] = \frac{\num{2.9812} - \num{3.0}}{\num{2.9812}} = \SI{0.6}{\percent} \]
\end{description}

\section{Zusammenfassung}

In dem Versuch wurden integrierende und differenzierende Schaltungen, die mit Hilfe von einem Operationsverstärker realisiert wurden, untersucht.

\paragraph{Umkehr-Differenzierer} Im ersten Versuchsteil wurde das Verhalten des Umkehr-Differenzierers betrachtet. Die differenzierende Eigenschaft bleibt erfüllt, bis die Amplitude des Ausgangssignals die Sättigung erreicht. Die differenzierende Eigenschaft ist auch abhängig vom Innenwiderstand der Quelle, welcher zu einer Phasenverschiebung führt und die differenzierende Eigenschaft aufhebt.

\paragraph{Umkehr-Integrierer} Im zweiten Versuchsteil wurde das Verhalten des Umkehr-Integrierers betrachtet. Die integrierende Eigenschaft bleibt erfüllt, bis die Amplitude des Ausgangssignals im Rauschen verschwindet. Durch Entfernen des Parellelwiderstandes sollte ein Ansteigen der Ausgangsspannung als Folge der Aufintegration des Offsetstromes zu erkennen sein. Der verwendete Operationsverstärker war jedoch zu gut optimiert und wies keinen Offsetstrom auf.

\paragraph{Bandpass} Für den Bandpass wurden charakteristische Größen ermittelt.
%
\begin{itemize}
	\item Verstärkung $V = \num{0.9942}$
	\item Resonanzfrequenz $\nu_0 = \SI{1608}{\Hz}$
	\item Bandbreite $\Delta\omega = \SI{20276}{\per\s}$
	\item Güte $Q = \num{0.499}$
\end{itemize}

\paragraph{Entdämpfter Bandpass} Bei $R_1 = \SI{4.98}{\ohm}$ wurde der Bandpass entdämpft. Es ergaben sich die neuen Parameter
%
\begin{itemize}
	\item Verstärkung $V = \num{2.9812}$
	\item Resonanzfrequenz $\nu_0 = \SI{1616}{\Hz}$
\end{itemize}

\paragraph{Fourieranalyse} Mit dem entdämpften Bandpass wurden ein Dreieck- und ein Rechtecksignal in seine Fourierkomponenten zerlegt.

\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
