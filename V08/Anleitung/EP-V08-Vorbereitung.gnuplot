#!/usr/bin/env gnuplot

reset
set terminal pdfcairo size 4.0,3.5 enhanced font 'TeX Gyre Pagella'
set pointsize 0.5
set output 'EP-V08-Vorbereitung.pdf'

# color definitions
set border linewidth 1.5

set key below

# Axes
set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.75
# Grid
set style line 12 lc rgb'#808080' lt 0 lw 1
set grid back ls 12

set mxtics 5
set mytics 5
set samples 10000

set title 'Versuch M30: Erzwungene Schwingung'
set xlabel 'Frequenz ω [s^{-1}]'
set ylabel 'Amplitude x(ω) [a.u.]' offset -1,0

f(a,x0,g,x) = a/sqrt((x0**2 - x**2)**2 + (2*g*x)**2)
load 'EP-V08-Vorbereitung.params'
fit f(a1,x1,g1,x) "resonanzkurve-0.2.dat" using 1:2 via a1,x1,g1
fit f(a2,x2,g2,x) "resonanzkurve-0.4.dat" using 1:2 via a2,x2,g2
update 'EP-V08-Vorbereitung.params'

set label sprintf("Rote Kurve\nω_0 = %.3f\nγ = %.3f", abs(x1),abs(g1)) at 0.58,65
set label sprintf("Blaue Kurve\nω_0 = %.3f\nγ = %.3f", abs(x2),abs(g2)) at 0.58,40

plot \
"resonanzkurve-0.2.dat" using 1:2 with points linecolor 1 pointtype 6 title 'Bremsspannung U = 0.2 A', \
f(a1,x1,g1,x) with lines linecolor 1 title 'Fit F_{a_1,x_{0,1},γ_1}(ν)' , \
"resonanzkurve-0.4.dat" using 1:2 with points linecolor 3 pointtype 6 title 'Bremsspannung U = 0.4 A', \
f(a2,x2,g2,x) with lines linecolor 3 title 'Fit F_{a_2,x_{0,2},γ_2}(ν)'
