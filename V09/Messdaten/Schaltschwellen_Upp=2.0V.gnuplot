#!/usr/bin/env gnuplot

reset
set terminal cairolatex pdf color dashed size 3.4,2.3
set pointsize 0.5
set output 'Schaltschwellen_Upp=2.0V.tex'

set xlabel 'Zeit $t$ $[\si{\milli\s}]$'
set ylabel 'Spannung $U[\mathrm{CH1}]$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set key below maxrows 2 spacing 1.25 width 7
set grid linetype 3 linecolor 0
set fit errorvariables

plot \
'schaltschwellen_Upp=2.0V.dat' using ($1*1e3):4 with lines linetype 1 linecolor 1 title '$U[\mathrm{CH1}]$', \
'schaltschwellen_Upp=2.0V.dat' using ($1*1e3):5 with lines linetype 1 linecolor 3 title '$U[\mathrm{CH2}]$'
