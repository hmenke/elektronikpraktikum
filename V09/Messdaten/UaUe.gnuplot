#!/usr/bin/env gnuplot

set terminal cairolatex pdf solid dashed size 3.4,2.3
set pointsize 0.5

set output "UaUe.tex"	

set key below width -3
set xlabel 'Eingangsspannung $U_e$ $[\si{\volt}]$'
set ylabel 'Ausgangsspannung $U_a$ $[\si{\volt}]$'
set format '\num[detect-all]{%g}'

set arrow nohead from first 0.8,graph 0 to first 0.8,graph 1
set arrow nohead from first 1.4,graph 0 to first 1.4,graph 1
set object rectangle from first 0.8,graph 0 to first 1.4,graph 1 behind fc rgb "#EEEEEE" fillstyle solid noborder

set arrow nohead from graph 0,first 0.4 to graph 1,first 0.4
set arrow nohead from graph 0,first 2.8 to graph 1,first 2.8
set object rectangle from graph 1,graph 0 to first 1.4,first 0.4 behind fillcolor rgb "#BBBBBB" fillstyle solid noborder
set object rectangle from graph 0,graph 1 to first 0.8,first 2.8 behind fillcolor rgb "#BBBBBB" fillstyle solid noborder

plot 'schaltschwellen_Upp=3.5V.dat' using 5:4 with points pointtype 0 linecolor 3 title '$U_a(U_e)$', \
NaN with boxes linetype -1 linecolor rgb "#EEEEEE" fillstyle solid title "undefiniert", \
NaN with boxes linetype -1 linecolor rgb "#BBBBBB" fillstyle solid title "definiert"
