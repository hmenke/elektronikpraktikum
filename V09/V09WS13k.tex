\documentclass[
	aps,pra,
	reprint,a4paper,
	amsmath,amssymb,amsfonts
]{revtex4-1}
%\linespread{1}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[ngerman]{babel}
\usepackage[expansion,protrusion]{microtype}
\usepackage{siunitx,textcomp}
\sisetup{
	mode=math,
	expproduct=cdot, %exponent-product=\cdot,
	allowzeroexp %retain-zero-exponent
}
\usepackage{tikz}
	\usetikzlibrary{calc,arrows}
\usepackage[
	siunitx,
	european,
	americaninductor %americaninductors
]{circuitikz}
\usepackage[caption=false]{subfig}
\usepackage{grffile,import}
\usepackage{booktabs}
\usepackage{bm}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\makeatletter
\AtBeginDocument{
	\def\tocname{Inhalt}
	\def\andname{und}
	\def\Dated@name{Datum: }
	\def\figurename{ABB.}
	\def\tablename{TAB.}
}
\makeatother
\begin{document}

\title{Versuch V09: Logische Gatter}

\author{Henri Menke}
\email{henrimenke@gmail.com}
\author{Jan Trautwein}
\email{jan.manuel.trautwein@web.de}
\affiliation{Gruppe 1--11 --- Platz k}

\collaboration{Betreuer: Boris Bonev}\noaffiliation
\date{16.\ Dezember 2013}

\begin{abstract}
	In diesem Versuch sollen Spannungspegel, Eingangsströme und andere elementare Eigenschaften von logischen Gattern (NAND-Gatter nach TTL-Bauart) betrachtet werden.
	Zunächst werden die Schaltschwellen der Gatter untersucht. Im darauf folgenden Teil werden verschiedene grundlegende logische Verknüpfungen auf Basis der NAND-Gatter wie Multiplexer und Dekoder umgesetzt.
\end{abstract}

\maketitle

\tableofcontents

\section{Grundlagen}

Die \emph{Schaltalgebra} besitzt wie auch die boolsche Algebra zwei Elemente, die mit den Operationen $\land$, $\lor$ und $\neg$ verknüpft werden können. In der Schaltalgebra ist eine etwas andere Schreibweise gebräuchlich; hier heißen die Verknüpfungen \texttt{AND}, \texttt{OR} und \texttt{NOT}.

Aus den drei elementaren Verknüpfungen lassen sich weitere Operatoren ableiten, z.\,B.\ die \texttt{XOR}-Verknüpfung
%
\begin{equation*}
	\begin{split}
		A \oplus B &= (A \lor B) \land \neg (A \land B) \\
	\intertext{bzw.}
		A \texttt{ XOR } B &= (A \texttt{ OR } B) \texttt{ AND} \texttt{ NOT } (A \texttt{ AND } B)	
	\end{split}
\end{equation*}

Sämtliche beschriebenen Logikfunktionen lassen sich auch durch die Sheffer-Operatoren beschreiben. Diese sind der \emph{Schefferstrich} $\uparrow$ (\texttt{NAND}) und der Pierce-Operator $\downarrow$ (\texttt{NOR}).

In Tabelle~\ref{tab:junktoren} sind die wichtigsten Junktoren abgebildet.

\begin{table*}
	\centering
	\begin{tabular}{*{6}{>{\centering}m{0.15\linewidth}}}
		\toprule
		\textbf{Bezeichnung} &
		\textbf{Junktor} &
		\textbf{Symbol in der Elektronik} &
		\textbf{Symbol in der Logik} &
		\textbf{Schaltsymbol} \cr
		\midrule
		Konjuktion & \texttt{AND} & $A \cdot B$ & $A \land B$ & \tikz{\node[and port] {};} \cr
		Disjunktion & \texttt{OR} & $A + B$ & $A \lor B$ & \tikz{\node[or port] {};} \cr
		Negation & \texttt{NOT} & $\bar{A}$ & $\neg A$ & \tikz{\node[not port] {};} \cr
		Sheffer-Funktion & \texttt{NAND} & $\overline{A \cdot B}$ & $A \uparrow B$ & \tikz{\node[nand port] {};} \cr
		Peirce-Funktion & \texttt{NOR} & $\overline{A + B}$ & $A \downarrow B$ & \tikz{\node[nor port] {};} \cr
		Kontravalenz & \texttt{XOR} & $A \oplus B$ & $A \not\equiv B$ & \tikz{\node[xor port] {};} \cr
		\bottomrule
	\end{tabular}
	\caption{Die gängigen Junktoren der Schaltalgebra.}
	\label{tab:junktoren}
\end{table*}

\subsection{Rechenregeln}

Für die Schaltalgebra gelten dieselben Rechenregeln, wie für die boolsche Algebra. Hier nochmals kurz zusammengefasst:
%
{\raggedright
\begin{itemize}
	\item Assoziativität:
		$(A \land B) \land C \equiv A \land (B \land C)$, bzw.\
		$(A \lor B) \lor C \equiv A \lor (B \lor C)$.
	\item Distributivität:
		$A \land (B \lor C) = (A \land B) \lor (A \land C)$, bzw.\
		$A \lor (B \land C) = (A \lor B) \land (A \lor C)$.
	\item Kommutativität:
		$A \land B \equiv B \land A$, bzw.\
		$A \lor B \equiv B \lor A$.
	\item Adsorptionsgesetz:
		$A \lor (A \land B) = A$, bzw.\
		$A \land (A \lor B) = A$.
	\item Komplementarität:
		$A \land \neg A = 0$, bzw.\
		$A \lor \neg A = 1$.
	\item Neutralität:
		$A \land 1 = 1$, bzw.\
		$A \lor 1 = 1$.
	\item Idempotenz:
		$A \land A = A$, bzw.\
		$A \lor A = A$.
	\item Extremalität:
		$A \land 0 = 0$, bzw.\
		$A \lor 1 = 1$.
	\item Doppelnegation:
		$\neg (\neg A) = A$.
	\item Dualität:
		$\neg 0 = 1$, bzw.\
		$\neg 1 = 0$.
	\item De Morgansche Gesetze:
		$\neg (A \lor B) = (\neg A) \land (\neg B)$, bzw.\
		$\neg (A \land B) = (\neg A) \lor (\neg B)$.
\end{itemize}}

\subsection{Logikschaltungen}

Um die logischen Funktionen mit Schaltkreisen zu realisieren muss man die beiden Zustände wahr und falsch verschiedenen Spannungspegeln zuordnen. So wird einem Potential, dass höher als ein gewisser Wert ist der Wert $\mathcal{H}$ (high) zugeordnet, während einem Potential kleiner als dieser gewisse Wert den Wert $\mathcal{L}$ (low) erhält.

Es gibt dann zwei Möglichkeiten die Logik zu definieren:
%
\begin{enumerate}
	\item Positive Logik: $\mathcal{H} \to 1$, $\mathcal{L} \to 0$.
	\item Negative Logik: $\mathcal{H} \to 0$, $\mathcal{L} \to 1$.
\end{enumerate}

Diese beiden Formen lassen sich verschieden interpretieren. Ein \texttt{AND} in positiver Logik geht bei negativer Logik in ein \texttt{OR} über.

Solche logischen Schaltungen werden als Logikgatter ausgeliefert. Am weitesten verbreitet sind die TTL-Gatter, wobei TTL für \emph{Transistor-Transistor-Logik} steht. Die TTL-Gatter sind aus Bipolartransistoren (BJT) aufgebaut. Im Versuch werden TTL-NAND-Gatter verwendet.

\subsection{Zahlensysteme}

Statt des herkömmlichen Dezimalsystems bietet es sich in der Logik an das Binärsystem zu verwenden, da dieses genau zwei Elemente besitzt ($\mathcal{B}_2 = \{0,1\}$, $b = 2$), die die Wahrheitszustände repräsentieren können. Soll nun eine Dezimalzahl $x$ mit $M$ Stellen und $N$ Nachkommastellen ins Binärsystem konvertiert werden, so geschieht das über
%
\begin{equation*}
	a = \sum\limits_{i=-N}^{M} x_i \cdot b^i
\end{equation*}

So wird aus $11_{10} = 1011_{2}$. Wegen $2 = 2^1$, $8 = 2^3$ und $16 = 2^4$ werden die Zahlensysteme zu den aufgeführten Basen, also Binär-, Oktal- und Hexadezimalsystem, oft in der Informationstechnik verwendet, da sich diese durch die beschriebenen Relationen leicht ineinander überführen lassen.



\section{Versuchsaufbau und -durchführung}
\subsection{Eigenschaften von TTL-Logikbausteinen}
Es soll die Funktion eines TTL-Gatters \texttt{DM7400} überprüft werden. Dazu wird eine Schaltung nach ABB.~\ref{fig: TTL-Gatter} aufgebaut. Die Eingänge des Gatters werden mit Tastern verbunden, eine rote LED leuchtet auf wenn eine hohe Spannung anliegt. Der Ausgang des TTL-Bausteins wird an eine zweifarbige LED angeschlossen, welche bei einer niedrigen Spannung grün leuchtet und bei einer hohen Spannung rot. 

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,0) node[nand port] (and) {}
		(and)+(-0.8,0.45) coordinate (and-up)
		(and)+(-0.8,-0.45) coordinate (and-down)
		node[ocirc,label={above:$V_\textnormal{CC}$},label={below:$+\SI{5.0}{\V}$}] (VCC) at (-3,1.5) {}
		node[ocirc,label={above:\texttt{GND}}] (GND) at (-3,-1.5) {}
		node[draw,thick,rectangle,minimum height=2cm] (LED) at (1.5,0) {LED}
		(VCC) to[short,-*] (VCC -| and-up) coordinate(inter-up) to[short] (and-up)
		(GND) to[short,-*] (GND -| and-down) coordinate(inter-down) to[short] (and-down)
		(inter-up) to[short,-*] (inter-up -| LED.north) to[short] (LED.north)
		(inter-down) to[short,-*] (inter-down -| LED.south) to[short] (LED.south)
		(and.out) -- (LED.west)
		(and.out) to[short,*-o] ++(0,-1) node[right] {$A$}
		(and.in 1) to[short,-o] ++(-1,0) node[left] {$E_1$}
		(and.in 2) to[short,-o] ++(-1,0) node[left] {$E_2$}
		;
	\end{tikzpicture}
	\caption{Schaltplan zur Funktionüberprüfung eines TTL-Gatters.}
	\label{fig: TTL-Gatter}
\end{figure}


Im zweiten Versuchsteil wird die Schaltung nach ABB.\ref{fig: TTL-Gatter mit Oszi} aufgebaut. Ein Eingang wird auf $V_\textnormal{CC}$ gelegt. Am Signalgenerator wird eine Dreiecksspannung mit 50\%-Symmetrie und $U_{pp}=\SI{5}{\V}$ eingestellt. Die Eingangsspannung und die Ausgangsspannung werden auf dem Oszilloskop dargestellt. Mit dem Programm LabVIEW wird die Anzeige des Oszilloskops gespeichert.
\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\begin{scope}[xshift=2cm]
			\draw[thick] (0,0) circle (0.5);
			\draw[|-] (0:0.3) -- +(0.4,0) coordinate (R);
			\draw[|-] (60:0.3) -- +(0,0.4) coordinate[label={right:II}] (CH2);
			\draw[|-] (120:0.3) -- +(0,0.4) coordinate[label={left:I}] (CH1);
			\draw[|-] (180:0.3) -- +(-0.4,0) coordinate (L);
			\draw[|-] (270:0.3) -- +(0,-0.4) coordinate (GND);
		\end{scope}
		\draw
		(0,0) node[nand port] (and) {}
		(and)+(-0.8,0.45) coordinate (and-up)
		(and)+(-0.8,-0.45) coordinate (and-down)
		node[ocirc,label={above:$V_\textnormal{CC}$},label={below:$+\SI{5.0}{\V}$}] (VCC) at (-3,2) {}
		(VCC) -| (and-up)
		(VCC -| and.in 1) to[short,*-] (and.in 1)
		(and.in 2) to[short,*-*] +(-0.5,0) coordinate (inter) to[R=100<\ohm>] +(-2,0) to[sV] +(0,-2) coordinate (sV)
		(inter) to[Do,-*] (inter |- VCC)
		(sV) -| (GND)
		(sV -| inter) to[Do,*-] (inter)
		(and-down) to[short,-*] (sV -| and-down)
		(and.in 2) |- ++(4.7,-0.5) -- ++(0,2) coordinate(upper) -| (CH2)
		(and.out) -| ++(upper -| 0.7,0) -| (CH1)
		;
	\end{tikzpicture}
	\caption{Schaltplan zur Ausmessung der Schaltschwellen mit Hilfe des Oszilloskops.}
	\label{fig: TTL-Gatter mit Oszi}
\end{figure}

\subsection{NAND als elementares Gitter}
Nach ABB.~\ref{fig: OR-Funktion} wird eine OR-Funktion aus NAND-Gattern aufgebaut. Um die Zustände an den Punkten $A_1$ und $A_2$ zu bestimmen, werden diese, ebenso wie der Ausgang $A_3$, auch an zweifarbige LEDs angeschlossen.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,0) node[nand port] (and3) {}
		(and3.in 1)+(-0.5,0.5) node[nand port] (and1) {}
		(and3.in 2)+(-0.5,-0.5) node[nand port] (and2) {}
		(and1.out) node[above] {$A_1$} -| (and3.in 1)
		(and2.out) node[above] {$A_2$} -| (and3.in 2)
		(and3.out) to[short,-o] ++(0.5,0) node[above] {$A_3$}
		(and1) ++(-2.5,0) node[left] {$E_1$} to[short,o-] (and1.in 1 |- and1)
		(and1.in 1) -- (and1.in 2)
		(and2) ++(-2.5,0) node[left] {$E_2$} to[short,o-] (and2.in 1 |- and2)
		(and2.in 1) -- (and2.in 2)
		;
	\end{tikzpicture}
	\caption{Schaltplan einer OR-Funktion aus NAND-Gatter.}
	\label{fig: OR-Funktion}
\end{figure}


Anschließend wird eine XOR-Funktion aus vier NAND-Gattern aufgebaut. Der Schaltungsplan ist in ABB.~\ref{fig: XOR-Funktion} zu sehen. Diese soll zu einem Halbaddierer erweitert werden (siehe Auswertung).

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,0) node[nand port] (and3) {}
		(and3.in 1)+(-0.5,1) node[nand port] (and1) {}
		(and3.in 2)+(-0.5,-1) node[nand port] (and2) {}
		(and2.in 1 |- and3)+(-0.5,0) node[nand port] (and4) {}
		(and1.out) node[above] {$A_2$} -| (and3.in 1)
		(and2.out) node[above] {$A_3$} -| (and3.in 2)
		(and3.out) to[short,-o] ++(0.5,0) node[above] {$A_4$}
		(and1.in 1) ++(-3,0) coordinate[label={left:$E_1$}] (E1) to[short,o-] (and1.in 1)
		(and2.in 2) ++(-3,0) coordinate[label={left:$E_2$}] (E2) to[short,o-] (and2.in 2)
		(and4.out) node[above] {$A_1$} to[short,-*] (and4.out -| and1.in 2)
		(and4.out -| and1.in 2) -- (and1.in 2)
		(and4.out -| and1.in 2) -- (and2.in 1)
		(and4.in 1) to[short,-*] (and4.in 1 |- E1)
		(and4.in 2) to[short,-*] (and4.in 2 |- E2)
		;
	\end{tikzpicture}
	\caption{Schaltplan einer XOR-Funktion in positiver Logik.}
	\label{fig: XOR-Funktion}
\end{figure}

\subsection{Multiplexer}
Die Schaltung für einen Multiplexer, welcher nur aus NANDs besteht, ist in ABB.~\ref{fig: Multiplexer} zu sehen. Über die LED-Anzeige wird die Funktionstabelle der Schaltung aufgenommen und der Steuereingang identifiziert.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,0) node[or port] (and3) {}
		(and3.in 1)+(-0.5,1) node[and port] (and1) {}
		(and3.in 2)+(-0.5,-1) node[and port] (and2) {}
		(and1.in 2) node[not port] (not) {}
		(and1.out) -| (and3.in 1)
		(and2.out) -| (and3.in 2)
		(and3.out) to[short,-o] ++(0.5,0) node[above] {$A_4$}
		(and1.in 1) ++(-2.5,0) coordinate[label={left:$E_1$}] (E1) to[short,o-] (and1.in 1)
		(and2.in 1) ++(-2.5,0) coordinate[label={left:$E_2$}] (E2) to[short,o-] (and2.in 1)
		(and2.in 2) ++(-2.5,0) coordinate[label={left:$E_3$}] (E3) to[short,o-] (and2.in 2)
		(not.out -| and1.in 2) -- (and1.in 2)
		(not.in 2) to[short,-*] (not.in 2 |- E3)
		;
	\end{tikzpicture}
	\caption{Schaltplan eines Multiplexer.}
	\label{fig: Multiplexer}
\end{figure}

\subsection{Dekodierschaltung}
Die Dekodierschaltung wird nach ABB.~\ref{fig: Dekodierschaltung} aufgebaut. Die Ausgänge ``0'', ``1'', ``2'', ``3'' werden mit je einer LED verbunden. Es soll bestimmt werden, für welche Logikart die Bezeichnung der Ausgänge richtig ist und welcher Ausgangszustand der aktive ist.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[every path/.style={}]
		\matrix[column sep=0.3cm,row sep=0.3cm]{
			\node[not port] (not1) {}; & \node[not port] (not2) {}; & & \\
			& & \node[nand port] (and3) {}; & \node[ocirc,label={right:``3''}] (n3) {}; \\
			& & \node[nand port] (and2) {}; & \node[ocirc,label={right:``2''}] (n2) {}; \\
			& & \node[nand port] (and1) {}; & \node[ocirc,label={right:``1''}] (n1) {}; \\
			& & \node[nand port] (and0) {}; & \node[ocirc,label={right:``0''}] (n0) {}; \\
		};
		\draw (and3.out) -- (n3) (and2.out) -- (n2) (and1.out) -- (n1) (and0.out) -- (n0);
		\draw (not1.in) to[short] (not1.in |- and0.in 2);
		\draw (not1.out) to[short] (not1.out |- and0.in 2);
		\draw (not2.in) to[short] (not2.in |- and0.in 2);
		\draw (not2.out) to[short] (not2.out |- and0.in 2);
		\draw (not1.in) to[short,*-o] ++(0,0.5) node[above] {$E_2$};
		\draw (not2.in) to[short,*-o] ++(0,0.5) node[above] {$E_1$};
		\draw (and3.in 1) to[short,-*] (and3.in 1 -| not1.in);
		\draw (and3.in 2) to[short,-*] (and3.in 2 -| not2.in);
		\draw (and2.in 1) to[short,-*] (and2.in 1 -| not1.in);
		\draw (and2.in 2) to[short,-*] (and2.in 2 -| not2.out);
		\draw (and1.in 1) to[short,-*] (and1.in 1 -| not1.out);
		\draw (and1.in 2) to[short,-*] (and1.in 2 -| not2.in);
		\draw (and0.in 1) to[short,-*] (and0.in 1 -| not1.out);
		\draw (and0.in 2) to[short,-*] (and0.in 2 -| not2.out);
	\end{tikzpicture}
	\caption{Schaltplan des 2-Bit-Dekodierers.}
	\label{fig: Dekodierschaltung}
\end{figure}

\section{Auswertung}
\subsection{Eigenschaften von TTL-Logikbausteinen}
An einem TTL-Gatter \texttt{DM7400} befinden sich vier NANDs. Nacheinander wurden alle überprüft. Für jeden NAND ergeben sich die in Tabelle~\ref{tab: 9.1a} sichtbaren Messergebnisse. Es sind also alle funktionstüchtig. Leuchtet die LED rot, so misst man am Ausgang eine Spannung von $U_A = \SI{0.15}{\V}$ an, leuchtet sie grün, so misst man eine Spannung von $U_A = \SI{3.92}{\V}$.
Der Gatterineingangsstrom beträgt für das niedrige Eingangspotential $I_L = \SI{0.219}{\milli\A}$ und für das hohe Eingangspotential $I_H \approx \SI{0.}{\milli\A}$.

\begin{table}[htpb]
	\begin{tabular}{cc@{\quad}cc}
		\toprule 
		$E_1$ & $E_2$ & A & Farbe der LED\\ 
		\midrule 
		0 & 0 & 0 & rot\\ 
		1 & 0 & 0 & rot\\ 
		0 & 1 & 0 & rot\\ 
		1 & 1 & 1 & grün\\ 
		\bottomrule
	\end{tabular} 
	\caption{Wahrheitstabelle zur Überprüfung des TTL-Gatters.}
	\label{tab: 9.1a}
\end{table}

% Auswertung 9.1 b) fehlt, das check ich nich so ganz.
%Messprotokoll: Bei einer Eingangsspannung von $U_E > \SI{2.3}{\V}$ hat man garantiert ein High am Ausgang. Unterhalb von $U_E = \SI{1.6}{\V} bekommt man garantiert ein Low am Ausgang.

\subsection{Schaltschwellen des TTL-Gatters}

Die Bezeichnungen in diesem Abschnitt wurden aus den Abbildungen~\ref{fig: TTL-Gatter} und~\ref{fig: TTL-Gatter mit Oszi} übernommen.

Der Eingang $E_2$ wird auf High gesetzt und auf $E_1$ ein Dreiecksignal angelegt. In Abbildung~\ref{fig:schaltschwelle} ist der zeitliche Verlauf der beiden Spannungen dargestellt. Die Sprünge des Signals an $\textnormal{CH1}$ entsprechen genau den Schaltschwellen.

Das Datenblatt sieht vor, dass ein Pegel $U_E > \SI{2.0}{\V}$ als High und ein Pegel $U_E < \SI{0.8}{\V}$ als Low erkannt wird. Mit dem Multimeter kann nachgewiesen werden, dass ein High ab $U_E \geq \SI{2.3}{\V}$ und ein Low ab $U_E \leq \SI{1.6}{\V}$ erkannt wird.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{Schaltschwellen_Upp=3.5V.tex}
	\caption{Schaltschwellen des TTL-NAND-Gatters bei $U_\textnormal{pp} = \SI{3.5}{\volt}$.}
	\label{fig:schaltschwelle}
\end{figure}

Um ein besseres Verständnis dieser Tatsache zu bekommen ist in Abbildung~\ref{fig:UaUe} die Ausgangsspannung über der Eingangsspannung aufgetragen. Man kann erkennen, dass im Bereich $\SI{0.8}{\V} < U_E < \SI{1.4}{\V}$ der Zustand der Ausgangsspannung undefiniert ist. Zugelassen sind nur die Zustände, die in den dunkelgrauen Kästen liegen.

\begin{figure}[htpb]
	\centering\sffamily
	\subimport{Messdaten/}{UaUe.tex}
	\caption{Ausgangsspannung des TTL-Bausteins über der Eingangsspannung.}
	\label{fig:UaUe}
\end{figure}

\subsection{NAND als elementares Gatter}
Die Messwerte (in positiver Logik), zu sehen in Tabelle~\ref{tab: 9.2}, entsprechen genau der Theorie.

\begin{table}[htpb]
	\begin{tabular}{cc@{\quad}ccc}
		\toprule 
		$E_1$ & $E_2$ & $A_1$ & $A_2$ & $A_3$ \\ 
		\midrule 
		0 & 0 & 1 & 1 & 0 \\ 
		1 & 0 & 0 & 1 & 1 \\ 
		0 & 1 & 1 & 0 & 1 \\ 
		1 & 1 & 1 & 0 & 1 \\ 
		\bottomrule
	\end{tabular} 
	\caption{Wahrheitstabelle der OR-Funktion für positive Logik.}
	\label{tab: 9.2}
\end{table}

Für die XOR-Funktion ergeben sich die Messwerte in Tabelle~\ref{tab: 9.2c}. Auch diese Werte stimmen mit der Theorie überein.


\begin{table}[htpb]
	\begin{tabular}{cc@{\quad}cccc}
		\toprule 
		$E_1$ & $E_2$ & $A_1$ & $A_2$ & $A_3$ & $A_4$ \\ 
		\midrule 
		0 & 0 & 1 & 1 & 1 & 0 \\ 
		1 & 0 & 1 & 0 & 1 & 1 \\ 
		0 & 1 & 1 & 1 & 0 & 1 \\ 
		1 & 1 & 0 & 0 & 1 & 0 \\ 
		\bottomrule
	\end{tabular} 
	\caption{Wahrheitstabelle der XOR-Funktion für positive Logik.}
	\label{tab: 9.2c}
\end{table}

Um die XOR-Funktion zu einem Halbaddierer zu erweitern, wird nach ABB.~\ref{fig: Halbaddierer} ein AND-Gatter parallel geschalten. Der Ausgang $C_i$ gibt dabei den Überlauf an.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,0.7) node[xor port] (xor) {}
		(0,-0.7) node[and port] (and) {}
		(xor.in 2) to[short,-*] (and.in 1)
		(xor.in 1)+(-0.5,0) node[circ] {} |- (and.in 2)
		(xor.in 1) to[short,-o] ++(-1,0) node[left] {$A_i$}
		(and.in 1) to[short,-o] ++(-1,0) node[left] {$B_i$}
		(xor.out) node[ocirc,label={right:$S_i$}] {}
		(and.out) node[ocirc,label={right:$C_i$}] {}
		;
	\end{tikzpicture}
	\caption{Schaltplan eines Halbaddierers}
	\label{fig: Halbaddierer}
\end{figure}

\subsection{Multiplexer}

Für den nach Schaltung~\ref{fig:Multiplexer-NAND} aufgebauten Multiplexer ergibt sich Wertetabelle~\ref{tab: 9.3}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw
		(0,0) node[nand port] (and3) {}
		(and3.in 1)+(-0.3,1) node[nand port] (and1) {}
		(and3.in 2)+(-0.3,-1) node[nand port] (and2) {}
		(and1.in 2 |- and3)+(-0.3,0) node[nand port] (and4) {}
		(and1.out) -| (and3.in 1)
		(and2.out) -| (and3.in 2)
		(and3.out) to[short,-o] ++(0.5,0) node[above] {$A_4$}
		(and1.in 1) ++(-2.5,0) coordinate[label={left:$E_1$}] (E1) to[short,o-] (and1.in 1)
		(and2.in 1) ++(-2.5,0) coordinate[label={left:$E_2$}] (E2) to[short,o-] (and2.in 1)
		(and2.in 2) ++(-2.5,0) coordinate[label={left:$E_3$}] (E3) to[short,o-] (and2.in 2)
		(and4.out) -| (and1.in 2)
		(and4.in 2) to[short,*-*] (and4.in 2 |- E3)
		(and4.in 1) to[short] (and4.in 2)
		;
	\end{tikzpicture}
	\caption{Schaltplan eines 2-zu-1-Multiplexers reduziert auf NAND-Gatter.}
	\label{fig:Multiplexer-NAND}
\end{figure}

Anhand der Tabelle kann der Eingang $E_3$ als Schalter identifiziert werden. Liegt an $E_3$ ein hohes Potential an ($E_3 = 1$), so liegt am Ausgang das gleiche Potential wie an $E_1$ an. Liegt an $E_3$ ein niedriges Potential an, so hat man am Ausgang den Zustand des zweiten Eingangs.

\begin{table}[htpb]
	\begin{tabular}{ccc@{\quad}ccc}
		\toprule 
		$E_1$ & $E_2$ & $E_3$ & $A_1$ & $A_2$ & $A_3$  \\ 
		\midrule 
		0 & 0 & 1 & 1 & 1 & 0 \\ 
		1 & 0 & 1 & 1 & 0 & 0 \\ 
		0 & 1 & 1 & 1 & 1 & 1 \\ 
		1 & 1 & 1 & 1 & 1 & 1 \\ 
		0 & 0 & 0 & 1 & 1 & 0 \\ 
		1 & 0 & 0 & 0 & 1 & 1 \\ 
		0 & 1 & 0 & 1 & 1 & 0 \\ 
		1 & 1 & 0 & 0 & 1 & 1 \\ 
		\bottomrule
	\end{tabular} 
	\caption{Wahrheitstabelle des Multiplexers.}
	\label{tab: 9.3}
\end{table}

\subsection{Dekodierschaltung}
Die Messwerte der Dekodierschaltung sind in Tabelle~\ref{tab: 9.4} zu sehen. Es fällt auf, dass der 0-Zustand als aktiver Zustand gewertet werden muss. Außerdem entspricht $E_2$ der ersten Ziffer der Binärzahl und $E_1$ der ersten.

%\begin{table}[htpb]
%	\begin{tabular}{cc@{\quad}cccc}
%		\toprule 
%		$E_1$ & $E_2$ & 0 & 1 & 2 & 3 \\ 
%		\midrule 
%		0 & 0 & 0 & 1 & 1 & 1 \\ 
%		1 & 0 & 1 & 0 & 1 & 1 \\ 
%		0 & 1 & 1 & 1 & 0 & 1 \\ 
%		1 & 1 & 1 & 1 & 1 & 0 \\ 
%		\bottomrule
%	\end{tabular} 
%	\caption{Wahrheitstabelle des 1-aus-4-Dekoders in positiver Logik.}
%	\label{tab: 9.4}
%\end{table}

\begin{table}[htpb]
	\begin{tabular}{c*{8}{>{\ttfamily}c}}
		\toprule 
		Dezimal &
		\multicolumn{1}{c}{Binär} &
		\multicolumn{1}{c}{a} &
		\multicolumn{1}{c}{b} &
		\multicolumn{1}{c}{c} &
		\multicolumn{1}{c}{d} &
		\multicolumn{1}{c}{e} &
		\multicolumn{1}{c}{f} &
		\multicolumn{1}{c}{g} \\ 
		\midrule 
		0 & 0000 & 1 & 1 & 1 & 1 & 1 & 1 & 0 \\
		1 & 0001 & 0 & 1 & 1 & 0 & 0 & 0 & 0 \\
		2 & 0010 & 1 & 1 & 0 & 1 & 1 & 0 & 1 \\
		3 & 0011 & 1 & 1 & 1 & 1 & 0 & 0 & 1 \\
		4 & 0100 & 0 & 1 & 1 & 0 & 0 & 1 & 1 \\
		5 & 0101 & 1 & 0 & 1 & 1 & 0 & 1 & 1 \\
		6 & 0110 & 1 & 0 & 1 & 1 & 1 & 1 & 1 \\
		7 & 0111 & 1 & 1 & 1 & 0 & 0 & 0 & 0 \\
		8 & 1000 & 1 & 1 & 1 & 1 & 1 & 1 & 1 \\
		9 & 1001 & 1 & 1 & 1 & 1 & 0 & 1 & 1 \\ 
		\bottomrule
	\end{tabular} 
	%
	\begin{tikzpicture}[every path/.style = {shorten >=0.1cm,shorten <=0.1cm,thick}]
		\draw (0,0) -- node[below] {d} ++(1,0);
		\draw (0,0) -- node[left] {c} ++(0.2,1);
		\draw (1,0) -- node[right] {e} ++(0.2,1);
		\draw (0,0)++(0.2,1) -- node[above] {g} ++(1,0);
		\draw (0,0)++(0.2,1) -- node[left] {b} ++(0.2,1);
		\draw (1,0)++(0.2,1) -- node[right] {f} ++(0.2,1);
		\draw (0,0)++(0.2,1)++(0.2,1) -- node[above] {a} ++(1,0);
	\end{tikzpicture}
	\caption{Dekodiertabelle der 7-Segment-Anzeige.}
	\label{tab: 9.4}
\end{table}

\section{Fehlerrechnung}
Da der Versuch ausschließlich aus qualitativen Beobachtungen (digitale Messwerte) besteht, ist eine Fehlerrechnung überflüssig.

\section{Zusammenfassung}
In dem Versuch wurden NAND-Gatter in TTL-Bauweise untersucht. Aus NAND-Gattern wurden verschiedene logische Funktionen aufgebaut.

Die Schaltschwellen des NAND-Gatters wurden bestimmt. Ab einer Spannung von $U_E \geq \SI{2.3}{\V}$ befindet man sich im Zustand \textit{High} und ab $U_E \leq \SI{1.6}{\V}$ im Zustand \textit{Low}.

Die theoretisch erwartete Funktionsweise einer OR-, AND, XOR- Funkttion, sowie einer Multiplex- und Dekodierschaltung wurde experimentell überprüft und bestätigt.
Beim Multiplexer wurde der Eingang $E_3$ als Schalter identifiziert.
\nocite{EP:Anleitung}
\raggedright
\bibliography{../bibliography}

\end{document}
